class FloatType extends NumericType
{
	public FloatType()
	{
		super("float");
	}

	public boolean	isFloat ()	{ return true; }

	public String toString () 	{ return "float";}

	public boolean isAssignable(Type t) {
		return t instanceof FloatType;
	}
}
