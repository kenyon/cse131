abstract class BasicType extends Type
{
	public BasicType(String name)
	{
		super(name, 4);
	}

	public boolean	isBasic ()	{ return true; }
}
