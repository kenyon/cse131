abstract class NumericType extends BasicType
{
	public NumericType(String name)
	{
		super(name);
	}

	public boolean	isNumeric ()	{ return true; }
}
