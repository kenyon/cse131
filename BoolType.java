class BoolType extends BasicType
{
	public BoolType()
	{
		super("bool");
	}

	public boolean	isBool ()	{ return true; }

	public String toString () 	{ return "bool";}

	public boolean isAssignable(Type t) {
		return t instanceof BoolType;
	}
}
