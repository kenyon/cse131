class ArrayType extends CompositeType
{
	public ArrayType()
	{
		super("array");
	}

	public ArrayType(Type elementType)
	{
		super("array");
		this.setElementType(elementType);
	}

	public ArrayType(Type elementType, int dimensionSize)
	{
		this(elementType);
		this.setDimensionSize(dimensionSize);
	}

	public boolean	isArray ()	{ return true; }

	public String toString () 	{ return "array";}

	private void setElementType(Type t)
	{
		this.m_elementType = t;
	}

	public Type getElementType()
	{
		return m_elementType;
	}

	public void setDimensionSize(int i)
	{
		this.m_dimensionSize = i;
	}

	public int getDimensionSize()
	{
		return m_dimensionSize;
	}

	public boolean isAssignable(Type t) {
		//System.out.println("ArrayType.isAssignable(): " + t);

		if (t.isPointer()) {
			return true;
		}

		return t instanceof ArrayType;
	}
	
	private Type m_elementType;
	private int m_dimensionSize;
}
