import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Stack;
import java.util.Vector;

public class AssemblyCodeGenerator {
    private String oneTimeGlobalExprInitString = "";

    public static boolean infunc = true;

    private int indent_level = 0;

    /**
     * An incrementing temp label number.
     */
    public static int tempnum = 0;

    public static int secretflagnum = 0;

    public static int andlabel = 0;
    public static int orlabel = 0;
    public static int afterelselabel = 0;
    public static int beforewhilelabel = 0;
    public static int afterwhilelabel = 0;

    public static Stack<Integer> andlabelstack = new Stack<Integer>();
    public static Stack<Integer> orlabelstack = new Stack<Integer>();
    public static Stack<Integer> afterelselabelstack = new Stack<Integer>();
    public static Stack<Integer> afterwhilelabelstack = new Stack<Integer>();
    public static Stack<Integer> beforewhilelabelstack = new Stack<Integer>();

    public static Stack<STO> funcexprstack = new Stack<STO>();

    /**
     * An incrementing temp label number.
     */
    public static int endifnum = 0;

    /**
     * An incrementing float label number.
     */
    public static int floatnum = 0;

    /**
     * An incrementing string label number.
     */
    private static int stringnum = 0;

    /**
     * An incrementing bool printing false label number for printing boolean
     * vars.
     */
    private static int boolprintnum = 0;

    /**
     * An incrementing end of false label number for printing boolean vars.
     */
    private static int endoffalsenum = 0;

    /**
     * Secret float label used in temporary computations.
     */
    private static final String SECRET_FLOAT = "s3cr3t.fl04t";
    private static final String SECRET_FLOAT2 = "s3cr3t.fl04t2";
    private static final String SECRET_FLAG = "s3cr3t.fl4g";

    private static final String ERROR_IO_CLOSE = "Unable to close fileWriter";
    private static final String ERROR_IO_CONSTRUCT = "Unable to construct FileWriter for file %s";
    private static final String ERROR_IO_WRITE = "Unable to write to fileWriter";

    private FileWriter fileWriter;

    private static final String FILE_HEADER = "! Generated %s by the kralph and rhejja CSE 131 compiler.\n\n"
	+ "\t.section \".rodata\"\n"
	+ "\t.align 4\n"
	+ "endl:\t.asciz \"\\n\"\n"
	+ "intFmt:\t.asciz \"%%d\"\n"
	+ "boolT:\t.asciz \"true\"\n" + "boolF:\t.asciz \"false\"\n"
	+ SECRET_FLOAT + ":\t.single 0r1.0\n"
	+ SECRET_FLOAT2 + ":\t.single 0r0.0\n"
	+ SECRET_FLAG + ":\t.single 0r0.0\n";

    /**
     * Special register we'll use to return float values from functions.
     */
    private static final String FLOAT_RETURN_REG = "%f5";

    private static final String FILE_FOOTER = "! End of file.";

    private static final String DATASIZE = "4";
    private static final String SEPARATOR = "\t";

    private static final String SET_OP = "set";

    private static final String THREE_PARAM = "%s" + SEPARATOR + "%s, %s, %s";
    private static final String TWO_PARAM = "%s" + SEPARATOR + "%s, %s";
    private static final String ONE_PARAM = "%s" + SEPARATOR + "%s";

    private static final String SAVE_ENDOFFUNC = "SAVE.%s = -(92 + %s) & -8";
    private static final String GLOBAL = ".global %s";
    private static final String ALIGN = ".align %s";
    private static final String SECTION = ".section \".%s\"";
    private static final String LABEL = "%s:";

    public AssemblyCodeGenerator(String fileToWrite) {
	try {

	    fileWriter = new FileWriter(fileToWrite);

	    writeAssembly(FILE_HEADER, (new Date()).toString());

	} catch (IOException e) {
	    System.err.printf(ERROR_IO_CONSTRUCT, fileToWrite);
	    e.printStackTrace();
	    System.exit(1);
	}
    }

    public void decreaseIndent() {
	indent_level--;
    }

    public void dispose() {
	try {
	    writeAssembly(FILE_FOOTER);

	    fileWriter.close();
	} catch (IOException e) {
	    System.err.println(ERROR_IO_CLOSE);
	    e.printStackTrace();
	    System.exit(1);
	}
    }

    public void increaseIndent() {
	indent_level++;
    }


    public void writeOneTimeGlobalExprInits()
    {
	//FIXME: make conditional here and make boolean also. s3cr3t_._b00l34n
	/*
	this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLAG, "%o0");
	this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%o0");
	this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
	this.writeAssembly("bne endif." + endifnum);
	this.writeAssembly("nop");*/
	
	this.writeAssembly("%s", oneTimeGlobalExprInitString);
	
	/*this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o0");
	this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLAG, "%o1");
	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	this.writeAssembly("endif." + endifnum++ + ":");*/
    }

    public void writeFuncPrologue(STO func) {
	//System.out.println("writing function prologue for " + funcid);

	this.writeAssembly("");

	if (func.getName().equals("main")) {
	    this.writeAssembly(LABEL, "globals");
	    this.writeAssembly(GLOBAL, func.getName() + "\t\t\t! start " + func.getName() + " prologue");
	} else {
	    this.writeAssembly("\t\t\t\t\t! start " + func.getName() + " prologue");
	}

	this.increaseIndent();
	this.writeAssembly(SECTION, "text");
	this.writeAssembly(ALIGN, DATASIZE);
	this.decreaseIndent();
	this.writeAssembly("");

	this.writeAssembly(LABEL, func.getName());

	this.increaseIndent();

	if (func.getName().equals("main")) {
	    this.writeAssembly(TWO_PARAM, SET_OP, "globals", "%l7");
	}

	this.writeAssembly(TWO_PARAM, SET_OP, "SAVE." + func.getName(), "%g1");
	this.writeAssembly(THREE_PARAM, "save", "%sp", "%g1", "%sp");

	if (!func.getName().equals("main"))
	{
	    for(int i = 0; i < 6; i++)
	    {
		this.writeAssembly(TWO_PARAM, "st", "%i" + i, "[%fp-" + (-68 - i*4) + "]");
	    }
	}

	this.writeAssembly("\t\t\t\t! end " + func.getName() + " prologue");
    }

    public void writeFormalParams(FuncSTO func, Vector<VarSTO> params)
    {
	this.writeAssembly("\t\t\t! Start of write formal params (call by ref) for " + func.getName());
	
	for (int i = 0; i < 6 && i < params.size(); i++) {
	    MyParser.incOffset();
	    if (func.getParamList() != null && i < func.getParamList().size() && ((STO)func.getParamList().elementAt(i)).getIsRef()) {
		this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + (-68 - i*4) + "]", "%o0");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
		this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%o0");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + (-68 - i*4) + "]");
	    }
	}
	
	this.writeAssembly("\t\t\t! End of write formal params (call by ref) for " + func.getName());
    }

    public void writeFuncEpilogue(FuncSTO fsto, int BytesOfLocalVarsAndTempStackSpaceNeeded) {
	String funcid = fsto.getName();

	this.writeAssembly("\t\t\t\t! start " + fsto.getName() + " epilogue");


	Vector<VarSTO> params = fsto.getParamList();

	// need to do this here in case a void func does not have a return
	// statement, so writeRetStmt would not be called. this stuff could be
	// duplicted, but that is ok, since the duplicated code would be
	// unreachable.
	if (fsto.getReturnType() == null) { // func returns void
	    if (params != null) {
		int currentOffset = 0;

		for (int i = 0; i < 6 && i < params.size(); i++) {
		    currentOffset += 4;

		    if (params.elementAt(i).getIsRef()) {
			this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + currentOffset + "]", "%o1");
			this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + (-68 - i*4) + "]", "%o0");
			this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
		    }

		}
	    }
	}


	this.writeAssembly("ret");
	this.writeAssembly("restore");
	this.writeAssembly("\t\t\t\t! end " + fsto/*.getName()*/ + " epilogue");
	this.decreaseIndent();

	this.writeAssembly(SAVE_ENDOFFUNC, funcid, BytesOfLocalVarsAndTempStackSpaceNeeded + "");

	this.increaseIndent();
	this.writeAssembly("");
	this.writeAssembly(SECTION, "data");
	this.writeAssembly(ALIGN, DATASIZE);
	this.writeAssembly("");
	this.decreaseIndent();
    }

    public void writeGlobalHeader() {
	this.writeAssembly("! start writeGlobalHeader");

	this.increaseIndent();

	this.writeAssembly(SECTION, "data");
	this.writeAssembly(ALIGN, DATASIZE);

	this.decreaseIndent();

	this.writeAssembly("! end writeGlobalHeader");
    }

    public void writeVarDecl(String name, String val) {
	this.writeAssembly(ONE_PARAM, name + ":", ".word " + val
		+ "\t! writeVarDecl 2 param");
    }

    public void writeVarDecl(String name, STO expr, Type vartype) {
	if (vartype.isFloat())
	{
	    this.writeAssembly(ONE_PARAM, name + ":", ".single 0r0.0\t! expr initialization of float in main");
	    this.writeAssembly(ONE_PARAM, SECRET_FLAG + secretflagnum, ":\t.word 0\n");
	    
	    this.infunc = false;

	    this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLAG + secretflagnum, "%o0");
	    this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%o0");
	    this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
	    this.writeAssembly("bne endif." + endifnum);
	    this.writeAssembly("nop");
	    
	    this.writeAssembly(TWO_PARAM, SET_OP, name, "%l1");
	    this.writeAssembly(expr.getIntoRegAssembly("%%f0", indent_level));

	    //if(!expr.getType().isFloat())
	    //{
	    //this.writeAssembly(TWO_PARAM, "fitos", "%f0", "%f0");
	    //}

	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%l1]\t! end expr initialization of float in main");
	    
	    this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o0");
	    this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLAG + secretflagnum++, "%o1");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	    this.writeAssembly("endif." + endifnum++ + ":");
	    
	    this.infunc = true;
	} else {
	    this.writeAssembly(ONE_PARAM, name + ":", ".word 0\t! expr initialization to follow");
	    this.writeAssembly(ONE_PARAM, SECRET_FLAG + secretflagnum, ":\t.word 0\n");

	    this.infunc = false;

	    this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLAG + secretflagnum, "%o0");
	    this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%o0");
	    this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
	    this.writeAssembly("bne endif." + endifnum);
	    this.writeAssembly("nop");
	    
	    this.writeAssembly(TWO_PARAM, SET_OP, name, "%l1");
	    this.writeAssembly(expr.getIntoRegAssembly("%%l0", indent_level));
	    this.writeAssembly(TWO_PARAM, "st", "%l0", "[%l1]\t! end expr initialization to follow");

	    this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o0");
	    this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLAG + secretflagnum++, "%o1");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	    this.writeAssembly("endif." + endifnum++ + ":");
	    
	    this.infunc = true;
	}
    }

    public void writeFloatDecl(String name, String val) {
	this.writeAssembly(ONE_PARAM, name + ":", ".single 0r" + val
		+ "\t! writeFloatDecl 2 param");
    }

    public void writeLocalVarDecl(int offset, String id) {
	this.writeAssembly(TWO_PARAM, SET_OP, 1234 + "", "%o0\t! start local uninitialized var decl for " + id);
	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + offset + "]\t! end local uninitialized var decl for " + id);
    }

    public void writeLocalVarDecl(int offset, String val, String id) {
	this.writeAssembly(TWO_PARAM, SET_OP, val + "", "%o0\t\t! start local initialized var decl for " + id);
	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + offset + "]\t! end local initialized var decl for " + id);
    }

    public void writeLocalFloatDecl(int offset, String id) {
	this.writeAssembly(SECTION, "data\"\t! start local float decl 1 param for " + id);
	this.writeAssembly(ALIGN, DATASIZE);

	this.decreaseIndent();
	this.writeAssembly("float." + floatnum + ":\t.single\t0r" + 1.23456789);
	this.increaseIndent();

	this.writeAssembly(SECTION, "text");
	this.writeAssembly(ALIGN, DATASIZE);
	this.writeAssembly(TWO_PARAM, SET_OP, "float." + floatnum, "%l0");
	this.writeAssembly(TWO_PARAM, "ld", "[%l0]", "%f0");

	this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + offset + "]\t! end local float decl 1 param for " + id);

	floatnum += 1;
    }

    public void writeLocalFloatDecl(int offset, String val, String id) {
	this.writeAssembly(SECTION, "data\"\t! start local float decl 2 param for " + id);
	this.writeAssembly(ALIGN, DATASIZE);

	this.decreaseIndent();
	this.writeAssembly("float." + floatnum + ":\t.single\t0r" + val);
	this.increaseIndent();

	this.writeAssembly(SECTION, "text");
	this.writeAssembly(ALIGN, DATASIZE);
	this.writeAssembly(TWO_PARAM, SET_OP, "float." + floatnum, "%l0");
	this.writeAssembly(TWO_PARAM, "ld", "[%l0]", "%f0");

	this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + offset + "]\t! end local float decl 2 param for " + id);

	floatnum += 1;
    }

    public void writeLocalGenDecl(STO left, STO right)
    {
	if (left.getType().isFloat()) {

	    this.writeAssembly(right.getIntoRegAssembly("%%f0", indent_level)
		    + "\t! start writeLocalGenDecl, condition left isFloat");

	    if (left.getLocalOffset() == 0) {
		this.writeAssembly(TWO_PARAM, SET_OP, left.getName(), "%o0");
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%o0]");
	    } else {
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + left.getLocalOffset() + "]\t! end writeLocalGenDecl, condition left isFloat");
	    }

	} else {

	    this.writeAssembly(right.getIntoRegAssembly("%%o0", indent_level)
		    + "\t! start writeLocalGenDecl, condition left !isFloat");

	    if (left.getLocalOffset() == 0) {
		this.writeAssembly(TWO_PARAM, SET_OP, left.getName(), "%o1");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	    } else {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + left.getLocalOffset() + "]\t! end writeLocalGenDecl, condition left !isFloat");
	    }

	}
    }

    public void writePrintf(BoolType t, String id, int offset) {
	if (offset == 0) {
	    this.writeAssembly(TWO_PARAM, SET_OP, id, "%l0\t\t! start variable bool printing");
	    this.writeAssembly(TWO_PARAM, "ld", "[%l0]", "%l1");
	} else {
	    this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + offset + "]", "%l1\t\t! start variable bool printing");
	}

	this.writeAssembly(TWO_PARAM, "cmp", "%l1", "%g0");
	this.writeAssembly(ONE_PARAM, "be", "boolprintfalse." + boolprintnum);
	this.writeAssembly("nop");

	this.writeAssembly(TWO_PARAM, SET_OP, "boolT", "%o0");
	this.writeAssembly(ONE_PARAM, "bne", "endoffalse." + endoffalsenum);
	this.writeAssembly("nop");

	this.decreaseIndent();
	this.writeAssembly(LABEL, "boolprintfalse." + boolprintnum);
	this.increaseIndent();

	this.writeAssembly(TWO_PARAM, SET_OP, "boolF", "%o0");

	this.decreaseIndent();
	this.writeAssembly(LABEL, "endoffalse." + endoffalsenum);
	this.increaseIndent();

	this.writeAssembly(ONE_PARAM, "call", "printf");
	this.writeAssembly("nop\t\t\t! end variable bool printing");

	endoffalsenum += 1;
	boolprintnum += 1;
    }

    public void writePrintf(boolean b, int offset) {
	if (offset == 0) {
	    this.writeAssembly(TWO_PARAM, SET_OP, b ? "boolT" : "boolF", "%o0\t! start writePrintf(boolean b, int offset)");
	} else {
	    this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + offset + "]", "%l1\t! start writePrintf(boolean b, int offset)");
	    this.writeAssembly(TWO_PARAM, "cmp", "%l1", "%g0");
	    this.writeAssembly(ONE_PARAM, "be", "boolprintfalse." + boolprintnum);
	    this.writeAssembly("nop");

	    this.writeAssembly(TWO_PARAM, SET_OP, "boolT", "%o0");
	    this.writeAssembly(ONE_PARAM, "bne", "endoffalse." + endoffalsenum);
	    this.writeAssembly("nop");

	    this.decreaseIndent();
	    this.writeAssembly(LABEL, "boolprintfalse." + boolprintnum);
	    this.increaseIndent();

	    this.writeAssembly(TWO_PARAM, SET_OP, "boolF", "%o0");

	    this.decreaseIndent();
	    this.writeAssembly(LABEL, "endoffalse." + endoffalsenum);
	    this.increaseIndent();
	}

	this.writeAssembly(ONE_PARAM, "call", "printf");
	this.writeAssembly("nop\t! end writePrintf(boolean b, int offset)");

	endoffalsenum += 1;
	boolprintnum += 1;
    }

    public void writeBoolPrintf(STO sto)
    {
	this.writeAssembly(sto.getIntoRegAssembly("%%l1", indent_level));

	this.writeAssembly(TWO_PARAM, "cmp", "%l1", "%g0");
	this.writeAssembly(ONE_PARAM, "be", "boolprintfalse." + boolprintnum);
	this.writeAssembly("nop");

	this.writeAssembly(TWO_PARAM, SET_OP, "boolT", "%o0");
	this.writeAssembly(ONE_PARAM, "bne", "endoffalse." + endoffalsenum);
	this.writeAssembly("nop");

	this.decreaseIndent();
	this.writeAssembly(LABEL, "boolprintfalse." + boolprintnum);
	this.increaseIndent();

	this.writeAssembly(TWO_PARAM, SET_OP, "boolF", "%o0");

	this.decreaseIndent();
	this.writeAssembly(LABEL, "endoffalse." + endoffalsenum);
	this.increaseIndent();

	this.writeAssembly(ONE_PARAM, "call", "printf");
	this.writeAssembly("nop\t\t\t! end variable bool printing");

	endoffalsenum += 1;
	boolprintnum += 1;
    }
    
    public void writePrintf(String s) {
	this.writeAssembly(SECTION, "data\"\t! start string printing");
	this.writeAssembly(ALIGN, DATASIZE);

	this.decreaseIndent();
	this.writeAssembly("string." + stringnum + ":\t.asciz\t\"" + s + "\"");
	this.increaseIndent();

	this.writeAssembly(SECTION, "text");
	this.writeAssembly(ALIGN, DATASIZE);
	this.writeAssembly(TWO_PARAM, SET_OP, "string." + stringnum, "%o0");
	this.writeAssembly(ONE_PARAM, "call", "printf");
	this.writeAssembly("nop\t\t\t! end string printing");

	stringnum += 1;
    }

    public void writePrintf(STO sto) {
	this.writeAssembly("\t\t\t! start writePrintf on " + sto.getName() + " of type " + sto.getType());

	if(sto.getType().isFloat())
	{
	    this.writeAssembly(sto.getIntoRegAssembly("%%f0", indent_level));
	    this.writeAssembly(ONE_PARAM, "call", "printFloat");
	}
	else
	{
	    this.writeAssembly(TWO_PARAM, SET_OP, "intFmt", "%o0");
	    this.writeAssembly(sto.getIntoRegAssembly("%%o1", indent_level));
	    this.writeAssembly(ONE_PARAM, "call", "printf");
	}

	this.writeAssembly("nop");
	this.writeAssembly("\t\t\t! end writePrintf on " + sto.getName() + " of type " + sto.getType());
    }

    public void writePrintReturn(STO sto)
    {
	if (((FuncPtrType)sto.getType()).getReturnType().isFloat()) {

	    MyParser.incOffset();
	    this.writeAssembly(TWO_PARAM, "st", FLOAT_RETURN_REG, "[%fp-" + (MyParser.getOffset()) + "]\t! start writePrintReturn isFloat condition");
	    VarSTO sto2 = new VarSTO(sto.getName(), new FloatType());
	    sto2.setLocalOffset(MyParser.getOffset());
	    this.writePrintf(sto2);
	    this.writeAssembly("\t! end writePrintReturn isFloat condition");

	} else if (((FuncPtrType)sto.getType()).getReturnType().isBool()) {

	    this.writeAssembly("\t! start writePrintReturn isBool condition");
	    MyParser.incOffset();
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + (MyParser.getOffset()) + "]");
	    VarSTO sto2 = new VarSTO(sto.getName(), new BoolType());
	    sto2.setLocalOffset(MyParser.getOffset());
	    this.writeBoolPrintf(sto2);
	    //this.writePrintf(new BoolType(), sto.getName(), (MyParser.getOffset()));
	    this.writeAssembly("\t! end writePrintReturn isBool condition");

	} else {

	    MyParser.incOffset();
	    this.writeAssembly("\t! start writePrintReturn else condition");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + (MyParser.getOffset()) + "]");
	    VarSTO sto2 = new VarSTO(sto.getName(), new IntType());
	    sto2.setLocalOffset(MyParser.getOffset());
	    this.writePrintf(sto2);
	    this.writeAssembly("\t! end writePrintReturn else condition");

	}
    }

    public void writeRetStmt(FuncSTO func, STO retval) {
	this.writeAssembly("\t\t\t! start writeRetStmt, func=" + func/*.getName()*/ + "; retval=" + ((retval != null) ? retval/*.getName()*/ : null));

	Vector<VarSTO> params = func.getParamList();

	if (params != null) {
	    int currentOffset = 0;

	    for (int i = 0; i < 6 && i < params.size(); i++) {
		currentOffset += 4;

		if (params.elementAt(i).getIsRef()) {
		    this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + currentOffset + "]", "%o1");
		    this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + (-68 - i*4) + "]", "%o0");
		    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
		}

	    }
	}

	if (retval == null) {
	    this.writeAssembly("ret\t\t\t! From Void Return");
	    this.writeAssembly("restore\t\t\t! End writeRetStmt retval == null condition");
	} else if (retval.getType().isFloat()) {
	    this.writeAssembly(retval.getIntoRegAssembly("%" + FLOAT_RETURN_REG, indent_level) + "\t\t\t! writeRetStmt isFloat condition");
	    this.writeAssembly("ret");
	    this.writeAssembly("restore\t\t\t! End writeRetStmt retval.getType().isFloat() condition");
	} else {
	    this.writeAssembly(retval.getIntoRegAssembly("%%i0", indent_level) + "\t! start writeRetStmt else condition");
	    this.writeAssembly("ret");
	    this.writeAssembly("restore\t\t\t! end writeRetStmt else condition");
	}
    }

    public void writeAssignStmt(STO left, STO right) {
	this.writeAssembly("\t\t\t! Start of assignment of " + right.getName() + " of type " + right.getType() + " to " + left.getName() + " of type " + left.getType());
	if(left.getType().isFloat())
	{
	    this.writeAssembly(right.getIntoRegAssembly("%%f0", indent_level));
	    this.writeAssembly(left.getLocIntoRegAssembly("%%o1", indent_level));
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%o1]");
	}
	else
	{
	    this.writeAssembly(right.getIntoRegAssembly("%%o0", indent_level));	
	    this.writeAssembly(left.getLocIntoRegAssembly("%%o1", indent_level));
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	}
	this.writeAssembly("\t\t\t! End of assignment");
    }

    public void writeAddSubOp(STO left, String oper, STO right) {
	this.writeAssembly("\t\t\t! start of " + oper + " oper on " + left.getName() + " of type " + left.getType() + " and " + right.getName() + " of type " + right.getType() + " (writeAddSubOp)");

	if (!left.getType().isFloat() && !right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%o0", this.indent_level) + "\t! writeAddSubOp condition 1");
	    this.writeAssembly(right.getIntoRegAssembly("%%o1", this.indent_level));
	    this.writeAssembly(THREE_PARAM, oper.equals("+") ? "add" : "sub", "%o0",  "%o1", "%g1");
	    this.writeAssembly(TWO_PARAM, "st", "%g1", "[%fp-" + MyParser.getOffset() + "]\t! end writeAddSubOp condition 1");
	} else if (!left.getType().isFloat() && right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! writeAddSubOp condition 2");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    //this.writeAssembly(TWO_PARAM, "fitos", "%f0", "%f0");
	    this.writeAssembly(THREE_PARAM, oper.equals("+") ? "fadds" : "fsubs", "%f0",  "%f1", "%f2");
	    this.writeAssembly(TWO_PARAM, "st", "%f2", "[%fp-" + MyParser.getOffset() + "]\t! end writeAddSubOp condition 2");
	} else if (left.getType().isFloat() && !right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! writeAddSubOp condition 3");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    //this.writeAssembly(TWO_PARAM, "fitos", "%f1", "%f1");
	    this.writeAssembly(THREE_PARAM, oper.equals("+") ? "fadds" : "fsubs", "%f0",  "%f1", "%f2");
	    this.writeAssembly(TWO_PARAM, "st", "%f2", "[%fp-" + MyParser.getOffset() + "]\t! end writeAddSubOp condition 3");
	} else if (left.getType().isFloat() && right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! writeAddSubOp condition 4");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    this.writeAssembly(THREE_PARAM, oper.equals("+") ? "fadds" : "fsubs", "%f0",  "%f1", "%f2");
	    this.writeAssembly(TWO_PARAM, "st", "%f2", "[%fp-" + MyParser.getOffset() + "]\t! end writeAddSubOp condition 4");
	} else {
	    this.writeAssembly("Error: unhandled condition in writeAddSubOp!");
	}

	this.writeAssembly("\t\t\t! end of add/sub oper (writeAddSubOp)");
    }

    public void writeMulDivOp(STO left, String oper, STO right) {
	this.writeAssembly("\t\t\t! start of " + oper + " oper on " + left.getName() + " of type " + left.getType() + " and " + right.getName() + " of type " + right.getType() + "(writeMulDivOp)");

	if (!left.getType().isFloat() && !right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%o0", this.indent_level) + "\t! start writeMulDivOp condition 1");
	    this.writeAssembly(right.getIntoRegAssembly("%%o1", this.indent_level));

	    if (oper.equals("*")) {
		this.writeAssembly(ONE_PARAM, "call", ".mul");
	    } else if (oper.equals("/")) {
		this.writeAssembly(ONE_PARAM, "call", ".div");
	    } else {
		this.writeAssembly("Error: unhandled condition in writeMulDivOp condition 1!");
	    }

	    this.writeAssembly("nop");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]" + "\t! end writeMulDivOp condition 1");
	} else if (!left.getType().isFloat() && right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! start writeMulDivOp condition 2");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    //this.writeAssembly(TWO_PARAM, "fitos", "%f0", "%f0");
	    this.writeAssembly(THREE_PARAM, oper.equals("*") ? "fmuls" : "fdivs", "%f0",  "%f1", "%f2");
	    this.writeAssembly(TWO_PARAM, "st", "%f2", "[%fp-" + MyParser.getOffset() + "]" + "\t! end writeMulDivOp condition 2");
	} else if (left.getType().isFloat() && !right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! start writeMulDivOp condition 3");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    //this.writeAssembly(TWO_PARAM, "fitos", "%f1", "%f1");
	    this.writeAssembly(THREE_PARAM, oper.equals("*") ? "fmuls" : "fdivs", "%f0",  "%f1", "%f2");
	    this.writeAssembly(TWO_PARAM, "st", "%f2", "[%fp-" + MyParser.getOffset() + "]" + "\t! end writeMulDivOp condition 3");
	} else if (left.getType().isFloat() && right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! start writeMulDivOp condition 4");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    this.writeAssembly(THREE_PARAM, oper.equals("*") ? "fmuls" : "fdivs", "%f0",  "%f1", "%f2");
	    this.writeAssembly(TWO_PARAM, "st", "%f2", "[%fp-" + MyParser.getOffset() + "]" + "\t! end writeMulDivOp condition 4");
	} else {
	    this.writeAssembly("Error: unhandled condition in writeMulDivOp!");
	}

	this.writeAssembly("\t\t\t! end of writeMulDivOp");
    }

    public void writeModOp(STO left, String oper, STO right) {
	this.writeAssembly("\t\t\t! start of %% oper on " + left.getName() + " of type " + left.getType() + " and " + right.getName() + " of type " + right.getType());

	this.writeAssembly(left.getIntoRegAssembly("%%o0", this.indent_level));
	this.writeAssembly(right.getIntoRegAssembly("%%o1", this.indent_level));
	this.writeAssembly("call .rem");
	this.writeAssembly("nop");
	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");

	this.writeAssembly("\t\t\t! end of writeModOp");
    }

    public void writeBitOp(STO left, String oper, STO right) {
	this.writeAssembly("\t\t\t! start of " + oper + " oper on " + left.getName() + " of type " + left.getType() + " and " + right.getName() + " of type " + right.getType() + "(writeBitOp)");

	this.writeAssembly(left.getIntoRegAssembly("%%o0", this.indent_level));
	this.writeAssembly(right.getIntoRegAssembly("%%o1", this.indent_level));

	if (oper.equals("|")) {
	    this.writeAssembly(THREE_PARAM, "or", "%o0",  "%o1", "%g1");
	} else if (oper.equals("&")) {
	    this.writeAssembly(THREE_PARAM, "and", "%o0",  "%o1", "%g1");
	} else if (oper.equals("^")) {
	    this.writeAssembly(THREE_PARAM, "xor", "%o0",  "%o1", "%g1");
	} else {
	    this.writeAssembly("Error: unhandled condition in writeBitOp!");
	}

	this.writeAssembly(TWO_PARAM, "st", "%g1", "[%fp-" + MyParser.getOffset() + "]");

	this.writeAssembly("\t\t\t! end of writeBitOp");
    }

    public void writeUnary(String sign, STO sto) {
	if (!MyParser.getInFunc()) {
	    this.infunc = false;
	}

	this.writeAssembly("\t\t\t! start unary " + sign + " op for " + sto.getName());

	if (!sto.getType().isFloat()) {
	    this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level) + "\t! sto is not float");
	    this.writeAssembly(TWO_PARAM, sign.equals("-") ? "neg" : "nop\t!", "%o0", "%o0");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]" + "\t! end sto is not float");
	} else if (sto.getType().isFloat()) {
	    this.writeAssembly(sto.getIntoRegAssembly("%%f0", indent_level) + "\t! sto is float");
	    this.writeAssembly(TWO_PARAM, sign.equals("-") ? "fnegs" : "nop\t!", "%f0", "%f0");
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]" + "\t! end sto is float");
	} else {
	    this.writeAssembly("Error: unhandled condition in writeUnary!");
	}

	this.writeAssembly("\t\t\t! end unary " + sign + " op for " + sto.getName());

	this.infunc = true;
    }

    public void writePreIncDecOp(String oper, STO sto) {
	this.writeAssembly("\t\t\t! start pre " + oper + " op for " + sto.getName());
	
	if (sto.getType().isPointer())
	{
	    this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level) + "\t! sto is not float");
	    this.writeAssembly(TWO_PARAM, SET_OP, "4", "%o1");
	    this.writeAssembly(THREE_PARAM, oper.equals("--") ? "sub" : "add", "%o0", "%o1", "%o0");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
	    
	    if (sto.getLocalOffset() != 0) {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" +sto.getLocalOffset() + "]" + "\t! sto is local");
	    } else {
		this.writeAssembly(TWO_PARAM, SET_OP, sto.getName(), "%o1" + "\t! sto is global");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]" + "\t! end sto is global");
	    }
	}
	else if (!sto.getType().isFloat()) {
	    this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level) + "\t! sto is not float");
	    this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o1");
	    this.writeAssembly(THREE_PARAM, oper.equals("--") ? "sub" : "add", "%o0", "%o1", "%o0");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");

	    if (sto.getLocalOffset() != 0) {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" +sto.getLocalOffset() + "]" + "\t! sto is local");
	    } else {
		this.writeAssembly(TWO_PARAM, SET_OP, sto.getName(), "%o1" + "\t! sto is global");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]" + "\t! end sto is global");
	    }
	} else if (sto.getType().isFloat()) {
	    this.writeAssembly(sto.getIntoRegAssembly("%%f0", indent_level) + "\t! sto is float");
	    this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLOAT, "%o1");
	    this.writeAssembly(TWO_PARAM, "ld", "[%o1]", "%f1");
	    this.writeAssembly(THREE_PARAM, oper.equals("--") ? "fsubs" : "fadds", "%f0", "%f1", "%f0");
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]");

	    if (sto.getLocalOffset() != 0) {
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" +sto.getLocalOffset() + "]" + "\t! sto is local");
	    } else {
		this.writeAssembly(TWO_PARAM, SET_OP, sto.getName(), "%o0" + "\t! sto is global");
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%o0]" + "\t! end sto is global");
	    }
	} else {
	    this.writeAssembly("Error: unhandled condition in writePreIncDecOp!");
	}

	this.writeAssembly("\t\t\t! end post " + oper + " op for " + sto.getName());
    }

    public void writePostIncDecOp(String oper, STO sto) {
	this.writeAssembly("\t\t\t! start post " + oper + " op for " + sto.getName());
	
	if (sto.getType().isPointer())
	{
	    this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level) + "\t! sto is not float");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]"); // puts x's non inc'd/dec'd val into result of exp
	    this.writeAssembly(TWO_PARAM, SET_OP, "4", "%o1");
	    this.writeAssembly(THREE_PARAM, oper.equals("--") ? "sub" : "add", "%o0", "%o1", "%o0");
	    
	    if (sto.getLocalOffset() != 0) {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + sto.getLocalOffset() + "]" + "\t! sto is local");
	    } else {
		this.writeAssembly(TWO_PARAM, SET_OP, sto.getName(), "%o1");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	    }
	}
	else if (!sto.getType().isFloat()) {
	    this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level) + "\t! sto is not float");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]"); // puts x's non inc'd/dec'd val into result of exp
	    this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o1");
	    this.writeAssembly(THREE_PARAM, oper.equals("--") ? "sub" : "add", "%o0", "%o1", "%o0");

	    if (sto.getLocalOffset() != 0) {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + sto.getLocalOffset() + "]" + "\t! sto is local");
	    } else {
		this.writeAssembly(TWO_PARAM, SET_OP, sto.getName(), "%o1");
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]");
	    }
	} else if (sto.getType().isFloat()) {
	    this.writeAssembly(sto.getIntoRegAssembly("%%f0", indent_level));
	    //this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%f0");
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]");
	    this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLOAT, "%o1");
	    this.writeAssembly(TWO_PARAM, "ld", "[%o1]", "%f1");
	    this.writeAssembly(THREE_PARAM, oper.equals("--") ? "fsubs" : "fadds", "%f0", "%f1", "%f0");

	    if (sto.getLocalOffset() != 0) {
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + sto.getLocalOffset() + "]");
	    } else {
		this.writeAssembly(TWO_PARAM, SET_OP, sto.getName(), "%o0");
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%o0]");
	    }
	} else {
	    this.writeAssembly("Error: unhandled condition in writePostIncDecOp!");
	}

	this.writeAssembly("\t\t\t! end post " + oper + " op for " + sto.getName());
    }

    public void writeLogicalOp(STO left, String oper, STO right)
    {
	this.writeAssembly("\t\t\t! Start of logical " + oper + " on " + left.getName() + " and " + right.getName());

	this.writeAssembly(left.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(right.getIntoRegAssembly("%%o1", indent_level));

	if (oper.equals("||")) {
	    this.writeAssembly(THREE_PARAM, "or", "%o0", "%o1", "%o0");
	} else if (oper.equals("&&")) {
	    this.writeAssembly(THREE_PARAM, "and", "%o0", "%o1", "%o0");
	} else {
	    this.writeAssembly("Error: unhandled condition in writeLogicalOp!");
	}

	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");

	this.writeAssembly("\t\t\t! end of logical");
    }

    /**
     * Write logical operation for unary not (!) operator.
     */
    public void writeLogicalOp(String oper, STO right)
    {
	this.writeAssembly("\t\t\t! Start of logical " + oper + " on " + right.getName());
	this.writeAssembly(right.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(THREE_PARAM, "xor", "%o0", "1", "%o0");
	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
	this.writeAssembly("\t\t\t! End of logical " + oper + " on " + right.getName());
    }

    public void writeShortCircuitAnd(STO left)
    {
	this.writeAssembly("\t\t\t! Start of writeShortCircuitAnd on " + left.getName());
	this.writeAssembly(left.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, "cmp", "%g0", "%o0");
	this.writeAssembly("be ANDlabel." + andlabel);
	this.writeAssembly("nop");
	andlabelstack.push(andlabel);
	andlabel++;
	this.writeAssembly("\t\t\t! End of writeShortCircuitAnd on " + left.getName());
    }

    public void writeShortCircuitAndLabel()
    {
	this.writeAssembly("\t\t\t! Start of writeShortCircuitAndLabel");
	this.decreaseIndent();
	this.writeAssembly(LABEL, "ANDlabel." + andlabelstack.pop());
	this.increaseIndent();
	this.writeAssembly("\t\t\t! End of writeShortCircuitAndLabel");
    }

    public void writeShortCircuitOr(STO left)
    {
	this.writeAssembly("\t\t\t! Start of writeShortCircuitOr on " + left.getName());
	this.writeAssembly(left.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, "cmp", "%g0", "%o0");
	this.writeAssembly("bne ORlabel." + orlabel);
	this.writeAssembly("nop");
	orlabelstack.push(orlabel);
	orlabel++;
	this.writeAssembly("\t\t\t! End of writeShortCircuitOr on " + left.getName());
    }

    public void writeJumpOverElse()
    {
	this.writeAssembly("\t\t\t! Start of writeJumpOverElse");
	this.writeAssembly("ba afterelselabel." + afterelselabel);
	this.writeAssembly("nop");
	afterelselabelstack.push(afterelselabel);
	afterelselabel++;
	this.writeAssembly("\t\t\t! End of writeJumpOverElse");
    }

    public void writeAfterElseLabel()
    {
	this.writeAssembly("\t\t\t! Start of writeAfterElseLabel");
	this.decreaseIndent();
	this.writeAssembly(LABEL, "afterelselabel." + afterelselabelstack.pop());
	this.increaseIndent();
	this.writeAssembly("\t\t\t! End of writeAfterElseLabel");
    }

    public void writeShortCircuitOrLabel()
    {
	this.writeAssembly("\t\t\t! Start of writeShortCircuitOrLabel");
	this.decreaseIndent();
	this.writeAssembly(LABEL, "ORlabel." + orlabelstack.pop());
	this.increaseIndent();
	this.writeAssembly("\t\t\t! End of writeShortCircuitOrLabel");
    }

    public void writeBeforeWhileLabel()
    {
	this.writeAssembly("\t\t\t! Start of writeBeforeWhileLabel");
	this.decreaseIndent();
	this.writeAssembly(LABEL, "beforewhilelabel." + beforewhilelabel);
	this.increaseIndent();
	beforewhilelabelstack.push(beforewhilelabel);
	beforewhilelabel++;
	//sync with afterwhileba
	this.writeAssembly("\t\t\t! End of writeBeforeWhileLabel");
    }

    public void writeWhile(STO expr)
    {
	this.writeAssembly("\t\t\t! Start of writeWhile on " + expr);
	//sync with afterwhilelabel
	this.writeAssembly(expr.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, "cmp", "%g0", "%o0");
	this.writeAssembly("be afterwhilelabel." + afterwhilelabel);
	this.writeAssembly("nop");
	afterwhilelabelstack.push(afterwhilelabel);
	afterwhilelabel++;
	this.writeAssembly("\t\t\t! End of writeWhile on " + expr);
    }

    public void writeAfterWhileBA()
    {
	this.writeAssembly("\t\t\t! Start of writeAfterWhileBA");
	this.writeAssembly("ba beforewhilelabel." + beforewhilelabelstack.pop());
	this.writeAssembly("nop");
	//sync with beforewhilelabel
	this.writeAssembly("\t\t\t! End of writeAfterWhileBA");
    }

    public void writeAfterWhileLabel()
    {
	this.writeAssembly("\t\t\t! Start of writeAfterWhileLabel");
	this.decreaseIndent();
	this.writeAssembly(LABEL, "afterwhilelabel." + afterwhilelabelstack.pop());
	this.increaseIndent();
	//sync with writewhile
	this.writeAssembly("\t\t\t! End of writeAfterWhileLabel");
    }

    public void writeBreak()
    {
	this.writeAssembly("\t\t\t! Start of writeBreak");
	this.writeAssembly("ba afterwhilelabel." + afterwhilelabelstack.peek());
	this.writeAssembly("nop");
	this.writeAssembly("\t\t\t! End of writeBreak");
    }

    public void writeContinue()
    {
	this.writeAssembly("\t\t\t! Start of writeContinue");
	this.writeAssembly("ba beforewhilelabel." + beforewhilelabelstack.peek());
	this.writeAssembly("nop");
	this.writeAssembly("\t\t\t! End of writeContinue");
    }

    public void writeRelationOp(STO left, String oper, STO right) {
	this.writeAssembly("\t\t\t! start of " + oper + " oper on " + left.getName() + " of type " + left.getType() + " and " + right.getName() + " of type " + right.getType() + "(writeRelationOp)");
	//System.out.println("start of " + oper + " oper on " + left.getName() + " of type " + left.getType() + " and " + right.getName() + " of type " + right.getType() + "(writeRelationOp)");
	// !FLOAT AND !FLOAT
	if (!left.getType().isFloat() && !right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%o0", this.indent_level) + "\t! !FLOAT AND !FLOAT");
	    this.writeAssembly(right.getIntoRegAssembly("%%o1", this.indent_level));
	    this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%o1");

	    if (oper.equals(">")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "bg", "tempz." + tempnum);

	    } else if (oper.equals("<")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "bl", "tempz." + tempnum);

	    } else if (oper.equals("<=")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "ble", "tempz." + tempnum);

	    } else if (oper.equals(">=")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "bge", "tempz." + tempnum);

	    } else if (oper.equals("==")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "be", "tempz." + tempnum);

	    } else if (oper.equals("!=")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "bne", "tempz." + tempnum);

	    } else {

		//tempnum += 1;
		this.writeAssembly("What is this operator? 1");

	    }

	    this.writeAssembly("nop");
	    this.writeAssembly(TWO_PARAM, "st", "%g0", "[%fp-" + MyParser.getOffset() + "]");

	    tempnum += 1;

	    this.writeAssembly(ONE_PARAM, "ba", "tempz." + tempnum);
	    this.writeAssembly("nop");
	    this.decreaseIndent();
	    this.writeAssembly(LABEL, "tempz." + (tempnum - 1));
	    this.increaseIndent();
	    this.writeAssembly(TWO_PARAM, SET_OP, "1", "%g1");
	    this.writeAssembly(TWO_PARAM, "st", "%g1", "[%fp-" + MyParser.getOffset() + "]");
	    this.decreaseIndent();
	    this.writeAssembly(LABEL, "tempz." + tempnum);
	    this.increaseIndent();

	    // FLOAT OR FLOAT
	} else if (left.getType().isFloat() || right.getType().isFloat()) {
	    this.writeAssembly(left.getIntoRegAssembly("%%f0", this.indent_level) + "\t! FLOAT OR FLOAT");
	    this.writeAssembly(right.getIntoRegAssembly("%%f1", this.indent_level));
	    this.writeAssembly(TWO_PARAM, "fcmps", "%f0", "%f1");

	    if (oper.equals(">")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "fbg", "tempz." + tempnum);

	    } else if (oper.equals("<")) {
		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "fbl", "tempz." + tempnum);

	    } else if (oper.equals("<=")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "fble", "tempz." + tempnum);

	    } else if (oper.equals(">=")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "fbge", "tempz." + tempnum);

	    } else if (oper.equals("==")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "fbe", "tempz." + tempnum);

	    } else if (oper.equals("!=")) {

		tempnum += 1;
		this.writeAssembly(ONE_PARAM, "fbne", "tempz." + tempnum);

	    } else {

		//tempnum += 1;
		this.writeAssembly("What is this operator? 2");

	    }

	    this.writeAssembly("nop");
	    this.writeAssembly(TWO_PARAM, "st", "%g0", "[%fp-" + MyParser.getOffset() + "]");

	    tempnum += 1;

	    this.writeAssembly(ONE_PARAM, "ba", "tempz." + tempnum);
	    this.writeAssembly("nop");
	    this.decreaseIndent();
	    this.writeAssembly(LABEL, "tempz." + (tempnum - 1));
	    this.increaseIndent();
	    this.writeAssembly(TWO_PARAM, SET_OP, "1", "%g1");
	    this.writeAssembly(TWO_PARAM, "st", "%g1", "[%fp-" + MyParser.getOffset() + "]");
	    this.decreaseIndent();
	    this.writeAssembly(LABEL, "tempz." + tempnum);
	    this.increaseIndent();

	} else {
	    this.writeAssembly("LOL WHAT IS THIS? left type=" + left.getType() + ", right type=" + right.getType());
	}

	this.writeAssembly("\t\t\t! end of oper " + oper + " (writeRelationOp)");
    }


    public void writeIfStmt(STO expr)
    {
	this.writeAssembly("\t\t\t! Start writeIfStmt with expr " + expr.getName());

	this.writeAssembly(expr.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
	this.writeAssembly("be endif." + endifnum);
	this.writeAssembly("nop");

	this.writeAssembly("\t\t\t! End writeIfStmt ");
    }

    public STO writeFuncCall(FuncSTO fsto, Vector<STO> exprs)
    {
	this.writeAssembly("\t\t\t! start writeFuncCall, fsto=" + fsto + "; return type=" + fsto.getReturnType());

	//System.out.println("fsto in writefunccall = " + fsto + " with name " + fsto.getName() + " with params " + fsto.getParamList());
	//System.out.println("writeFuncCall: fsto=" + fsto + fsto.hashCode());
	
	Vector<VarSTO> paramlist = fsto.getParamList();
	
	if (!fsto.getName().equals("main") && exprs != null && exprs.size() != 0) {
	
	    //System.out.println("exprs.size() = " + exprs.size());
	
	    for (int i = 0; i < exprs.size() && i < 6; ++i) {
		//System.out.println("writefunccall " +((STO)exprs.elementAt(i)) + " with name " + ((STO)exprs.elementAt(i)).getName());

		if (paramlist.elementAt(i).getIsRef()) {
		    if (exprs.elementAt(i).getLocalOffset() == 0) {
			this.writeAssembly(TWO_PARAM, "set", exprs.elementAt(i).getName(), "%o" + i + "\t! ref set for globals");
		    } else {
			this.writeAssembly(TWO_PARAM, "set", exprs.elementAt(i).getLocalOffset() + "", "%o" + i + "\t\t! ref set for locals");
			this.writeAssembly(THREE_PARAM, "sub", "%fp", "%o" + i, "%o" + i + "\t! ref sub for locals");
		    }
		} else {
		    this.writeAssembly(exprs.elementAt(i).getIntoRegAssembly("%%o" + i, indent_level) + "\t! not ref else branch");
		}
	    }
	}
	
	this.writeAssembly(ONE_PARAM, "call", fsto.getName());
	this.writeAssembly("nop");

	FuncSTO newsto = (FuncSTO)fsto.clone();

	//newsto.setType(sto.getType());

	//System.out.println("ACG: fsto: " + fsto + ", type: " + fsto.getType() + ", return type: " + fsto.getReturnType() + fsto.hashCode());
	//System.out.println("ACG: newsto: " + newsto + ", type: " + newsto.getType() + ", return type: " + newsto.getReturnType() + newsto.hashCode());
	
	MyParser.incOffset();
	newsto.setLocalOffset(MyParser.getOffset());

	if (fsto.getReturnType() != null) { // nonvoid return
	    if (fsto.getReturnType().isFloat()) { // FIXME: Why is the return type not correct for tests/PA2Tests-graded/tI3j.rc ???
		this.writeAssembly(TWO_PARAM, "st", FLOAT_RETURN_REG, "[%fp-" + newsto.getLocalOffset() + "]\t! end writeFuncCall (float return type)");
	    } else { // nonfloat return type
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + newsto.getLocalOffset() + "]\t! end writeFuncCall (nonfloat return type)");
	    }
	} else { // void return
	    this.writeAssembly("\t\t\t! end writeFuncCall (void return)");
	}

	return newsto;
	
    }

    public void writeExit(STO exitsto)
    {
	this.writeAssembly(exitsto.getIntoRegAssembly("%%o0", indent_level) + "\t! start writeExit");
	this.writeAssembly("call exit");
	this.writeAssembly("nop\t! end writeExit");
    }

    public void writeReadStmt(STO designator)
    {
	this.writeAssembly("\t\t\t! start writeReadStmt");

	if(!designator.getType().isFloat()) {
	    this.writeAssembly("call inputInt");
	} else {
	    this.writeAssembly("call inputFloat");
	}

	this.writeAssembly("nop");

	if (designator.getLocalOffset() != 0) {
	    if (designator.getType().isFloat()) {
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + designator.getLocalOffset() + "]\t! local float designator");
	    } else {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + designator.getLocalOffset() + "]\t! local nonfloat designator");
	    }
	} else {
	    this.writeAssembly(designator.getLocIntoRegAssembly("%%o1", indent_level) + "\t! global designator");

	    if (designator.getType().isFloat()) {
		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%o1]\t! float");
	    } else {
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%o1]\t! nonfloat");
	    }
	}

	this.writeAssembly("\t\t\t! end writeReadStmt");
    }

    public void writeArrayDef(STO sto)
    {
	if (sto.getLocalOffset() == 0)
	{
	    this.writeAssembly("\n!Start of global array " + sto.getName());

	    for (int i = 0; i < ((ArrayType) sto.getType()).getDimensionSize(); i++)
	    {
		if (((ArrayType)sto.getType()).getElementType().isFloat())
		{
		    this.writeAssembly(sto.getName().substring(0, sto.getName().indexOf("_0_")) + "_" + ((((ArrayType) sto.getType()).getDimensionSize() - 1) - i) + "_" + ":\t" + ".single 0r0.0");
		}
		else
		{
		    this.writeAssembly(sto.getName().substring(0, sto.getName().indexOf("_0_")) + "_" + ((((ArrayType) sto.getType()).getDimensionSize() - 1) - i) + "_" + ":\t" + ".word 0");
		}
	    }

	    this.writeAssembly("!End of global array " + sto.getName() + "\n");
	}
	else
	{
	    this.writeAssembly("!Start of local array " + sto.getName());

	    for (int i = 0; i < ((ArrayType) sto.getType()).getDimensionSize(); i++)
	    {
		this.writeAssembly("!LOCAL ARRAY: " + sto.getName().substring(0, sto.getName().indexOf("_0_")) + "[" + i + "] is at [%%fp-" + (MyParser.getOffset()-((((ArrayType) sto.getType()).getDimensionSize()-1)*4-i*4)) + "]");
	    }

	    this.writeAssembly("!End of local array " + sto.getName().substring(0, sto.getName().indexOf("_0_")) + "\n");
	}
    }


    public void writeStructDef(STO sto)
    {
	StructType st = (StructType)sto.getType();
	//System.out.println("num of fields=" + st.numFields());
	Vector<STO> fields = st.getFields();
	//System.out.println("fields=" + fields);

	if (sto.getLocalOffset() == 0) {
	    this.writeAssembly("! Start of global structdef " + sto.getName() + "st=" + st);
	    this.writeAssembly(SECTION, "data");
	    this.writeAssembly(ALIGN, DATASIZE);

	    for (int i = 0; i < st.numFields(); ++i) {
		this.writeAssembly(LABEL, sto.getName() + "." + fields.elementAt(i).getName());
		this.increaseIndent();

		if (fields.elementAt(i).getType().isInt() || fields.elementAt(i).getType().isBool()) {
		    this.writeAssembly(".word 0");
		} else if (fields.elementAt(i).getType().isFloat()) {
		    this.writeAssembly(".single 0r0.0");
		}

		this.decreaseIndent();
	    }

	} else {
	    this.writeAssembly("! Start of local structdef " + sto.getName());
	}

	this.writeAssembly("! End of structdef " + sto.getName());
    }


    public void writeDereference(STO sto) {
	this.writeAssembly("\t\t\t! Start dereference of " + sto.getName() + " of pointed type " + ((PointerType) sto.getType()).getPointedType());
	
	this.writeAssembly("\t\t\t! Start run time null check on pointer " + sto.getName());
	
	this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%g0");
	this.writeAssembly(ONE_PARAM, "bne", "tempnum_" + tempnum);
	this.writePrintf("Attempt to dereference NULL pointer.");
	this.writePrintf("\\n");
	this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o0");
	this.writeAssembly(ONE_PARAM, "call", "exit");
	this.writeAssembly("nop");
	this.writeAssembly(LABEL, "tempnum_" + tempnum);
	
	this.writeAssembly("\t\t\t! End run time null check on pointer " + sto.getName());
	
	tempnum++;
	
	this.writeAssembly(sto.getIntoRegAssembly("%%o0", indent_level));
	
	if (((PointerType) sto.getType()).getPointedType().isFloat())
	{
	    this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%f0");
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]");
	}
	else
	{
	    this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%o0");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
	}
	
	this.writeAssembly("\t\t\t! End dereference of " + sto.getName());
    }

    public void writeAddressOf(STO sto)
    {
	this.writeAssembly("\t\t\t! Start address of " + sto.getName());
	
	this.writeAssembly(sto.getLocIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
	
	this.writeAssembly("\t\t\t! Start address of " + sto.getName());
    }

    public void writeRunTimeArrayBoundsCheck(STO array, STO expr) {
	this.writeAssembly("\t\t\t! Start run time array bounds check on array " + array + " with index " + expr);
	
	this.writeAssembly(expr.getIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, SET_OP, ((ArrayType) array.getType()).getDimensionSize() + "", "%o1");
	this.writeAssembly(TWO_PARAM, "cmp", "%o0", "%o1");
	this.writeAssembly(ONE_PARAM, "bl", "tempnum_" + tempnum);
	this.writePrintf("Index value of ");
	this.writePrintf(expr);
	this.writePrintf(" is outside legal range [0,");
	this.writePrintf(new ConstSTO(array.getName() + "error", new IntType(), (double)(((ArrayType) array.getType()).getDimensionSize())));
	this.writePrintf(").\\n");
	this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o0");
	this.writeAssembly(ONE_PARAM, "call", "exit");
	this.writeAssembly("nop");
	this.writeAssembly(LABEL, "tempnum_" + tempnum);
	
	this.writeAssembly("\t\t\t! End run time array bounds check on array " + array + " with index " + expr);
	
	tempnum++;
    }

    public void writeNew(STO sto) {
	this.writeAssembly(sto.getLocIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, SET_OP, "0", "%o1");
	this.writeAssembly(TWO_PARAM, "st", "%o1", "[%o0]");
    }

    public void writeDelete(STO sto) {
	this.writeAssembly(sto.getLocIntoRegAssembly("%%o0", indent_level));
	this.writeAssembly(TWO_PARAM, SET_OP, "0", "%o1");
	this.writeAssembly(TWO_PARAM, "st", "%o1", "[%o0]");
    }

    public void writeTypecast(STO sto, Type t) {
	if (sto.getType().isInt() || sto.getType().isPtrGrp() && t.isFloat()) //cast int/ptr to float
	{
	    this.writeAssembly(sto.getIntoRegAssembly("%%f0", indent_level));
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]");
	}
	else if (sto.getType().isFloat() && t.isInt() || t.isPtrGrp() || t.isBool()) //cast float to int/ptr/bool
	{
	    this.writeAssembly(sto.getIntoRegAssembly("%%f0", indent_level));
	    this.writeAssembly(TWO_PARAM, "fstoi", "%f0", "%f0");
	    this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]");
	    this.writeAssembly(TWO_PARAM, "ld", "[%fp-" + MyParser.getOffset() + "]", "%o0");
	    this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
	}
	else if (sto.getType().isBool() && t.isInt() || t.isFloat() || t.isPtrGrp()) //cast bool to int/float/ptr
	{
	    this.writeAssembly(sto.getIntoRegAssembly("%%l1", indent_level));

	    if (t.isFloat())
	    {
		this.writeAssembly(TWO_PARAM, "cmp", "%l1", "%g0");
		this.writeAssembly(ONE_PARAM, "be", "boolfalse." + boolprintnum);
		this.writeAssembly("nop");
		
		this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLOAT, "%o0");
		this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%f0");
		this.writeAssembly(ONE_PARAM, "bne", "endoffalse." + endoffalsenum);
		this.writeAssembly("nop");

		this.decreaseIndent();
		this.writeAssembly(LABEL, "boolfalse." + boolprintnum);
		this.increaseIndent(); 
		
		this.writeAssembly(TWO_PARAM, SET_OP, SECRET_FLOAT2, "%o0");
		this.writeAssembly(TWO_PARAM, "ld", "[%o0]", "%f0");

		this.decreaseIndent();
		this.writeAssembly(LABEL, "endoffalse." + endoffalsenum);
		this.increaseIndent();

		this.writeAssembly(TWO_PARAM, "st", "%f0", "[%fp-" + MyParser.getOffset() + "]");
	    }
	    else
	    {
		this.writeAssembly(TWO_PARAM, "cmp", "%l1", "%g0");
		this.writeAssembly(ONE_PARAM, "be", "boolfalse." + boolprintnum);
		this.writeAssembly("nop");

		this.writeAssembly(TWO_PARAM, SET_OP, "1", "%o0");
		this.writeAssembly(ONE_PARAM, "bne", "endoffalse." + endoffalsenum);
		this.writeAssembly("nop");

		this.decreaseIndent();
		this.writeAssembly(LABEL, "boolfalse." + boolprintnum);
		this.increaseIndent();

		this.writeAssembly(TWO_PARAM, SET_OP, "0", "%o0");

		this.decreaseIndent();
		this.writeAssembly(LABEL, "endoffalse." + endoffalsenum);
		this.increaseIndent();

		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + MyParser.getOffset() + "]");
	    }

	    endoffalsenum += 1;
	    boolprintnum += 1;
	}
    }

    public void writeAssembly(String template, String... params) {
	if (infunc)
	{
	    StringBuilder asStmt = new StringBuilder();

	    for (int i = 0; i < indent_level; i++) {
		asStmt.append(SEPARATOR);
	    }

	    asStmt.append(String.format(template, (Object[]) params));
	    asStmt.append("\n");

	    /* // lol raptors
	    if (!System.getenv("USER").equals("rhejja") && !System.getenv("USER").equals("kralph")) 
	    {
		asStmt.append("\n");
		asStmt.append(this.writeRaptor());
		asStmt.append("\n");
	    }
	    */

	    try {
		fileWriter.write(asStmt.toString());
	    } catch (IOException e) {
		System.err.println(ERROR_IO_WRITE);
		e.printStackTrace();
	    }
	}
	else
	{
	    StringBuilder asStmt = new StringBuilder();

	    for (int i = 0; i < indent_level; i++) {
		asStmt.append(SEPARATOR);
	    }

	    asStmt.append(String.format(template, (Object[]) params));
	    asStmt.append("\n");

	    oneTimeGlobalExprInitString += asStmt.toString();
	}
    }

    /*public STO writeFuncPtrCall(STO sto, Vector exprs) {
	this.writeAssembly("\t\t\t! start writeFuncCall, fsto=" + fsto + "; return type=" + fsto.getReturnType());

	//System.out.println("fsto in writefunccall = " + fsto + " with name " + fsto.getName() + " with params " + fsto.getParamList());
	//System.out.println("writeFuncCall: fsto=" + fsto + fsto.hashCode());
	
	Vector<VarSTO> paramlist = fsto.getParamList();
	
	if (!fsto.getName().equals("main") && exprs != null && exprs.size() != 0) {
	
	    //System.out.println("exprs.size() = " + exprs.size());
	
	    for (int i = 0; i < exprs.size() && i < 6; ++i) {
		//System.out.println("writefunccall " +((STO)exprs.elementAt(i)) + " with name " + ((STO)exprs.elementAt(i)).getName());

		if (paramlist.elementAt(i).getIsRef()) {
		    if (exprs.elementAt(i).getLocalOffset() == 0) {
			this.writeAssembly(TWO_PARAM, "set", exprs.elementAt(i).getName(), "%o" + i + "\t! ref set for globals");
		    } else {
			this.writeAssembly(TWO_PARAM, "set", exprs.elementAt(i).getLocalOffset() + "", "%o" + i + "\t\t! ref set for locals");
			this.writeAssembly(THREE_PARAM, "sub", "%fp", "%o" + i, "%o" + i + "\t! ref sub for locals");
		    }
		} else {
		    this.writeAssembly(exprs.elementAt(i).getIntoRegAssembly("%%o" + i, indent_level) + "\t! not ref else branch");
		}
	    }
	}
	
	this.writeAssembly(ONE_PARAM, "call", fsto.getName());
	this.writeAssembly("nop");

	FuncSTO newsto = (FuncSTO)fsto.clone();

	//newsto.setType(sto.getType());

	//System.out.println("ACG: fsto: " + fsto + ", type: " + fsto.getType() + ", return type: " + fsto.getReturnType() + fsto.hashCode());
	//System.out.println("ACG: newsto: " + newsto + ", type: " + newsto.getType() + ", return type: " + newsto.getReturnType() + newsto.hashCode());
	
	MyParser.incOffset();
	newsto.setLocalOffset(MyParser.getOffset());

	if (fsto.getReturnType() != null) { // nonvoid return
	    if (fsto.getReturnType().isFloat()) {
		this.writeAssembly(TWO_PARAM, "st", FLOAT_RETURN_REG, "[%fp-" + newsto.getLocalOffset() + "]\t! end writeFuncCall (float return type)");
	    } else { // nonfloat return type
		this.writeAssembly(TWO_PARAM, "st", "%o0", "[%fp-" + newsto.getLocalOffset() + "]\t! end writeFuncCall (nonfloat return type)");
	    }
	} else { // void return
	    this.writeAssembly("\t\t\t! end writeFuncCall (void return)");
	}

	return newsto;
    }*/

    public String writeRaptor()
    {
	return "!~~~~~~~~~~~~~~~PET RAPTOR===========~~~~~~~\n"
		+ "!~~~~~~~~~~~~LOVES ASSEMBLY========~~~~~~\n"
		+ "!~~~~~~~~~~~~~~~~~===================~~~~\n"
		+ "!~~~~~~~~~~~~~~~~~~===================~~~\n"
		+ "!~~~~~~~~~~~~~~~~~~~===================~~\n"
		+ "!~~~~~~~~IZ8D~~~~~~~~~=========N========~\n"
		+ "!~~~~~787$?ZOD~~~~~~~~=====?DI==========~\n"
		+ "!~~~OZ$O?N$DNN~~~~~~~~~~N88+=============\n"
		+ "!~~$$?OIZT8NM~~~?Z8OONDDDI===============\n"
		+ "!~~ZZ$ZONMAOONO8$88OOZ8D~================\n"
		+ "!~~:~~~~~:ILODD$8888I88:~++==============\n"
		+ "!~~~~~~~~~~ZH8Z$NO88Z88??================\n"
		+ "!~~~~~~~~~~88ADDZMNODD$~~~===============\n"
		+ "!~~~~~~~~:Z8:~8DNIDDD$+~~~===============\n"
		+ "!~~~~~~~O~~~~~$MM7NDO?~~~~~==============\n"
		+ "!~~~~~~+~~~~O~~~O~MNN?~~~~~~=============\n"
		+ "!~~~~~:N~~~+~~~~D~7~D~~~~~~~=============\n"
		+ "!~~~~~~~~~D~~~~8~~~8~~~~~~~~~============\n"
		+ "!:~~~~~~~~~~~D8:~~~O~~~~~~~~~============\n"
		+ "!:~~~~~~~~~~~~~~~DN$~~~~~~~~~============\n"
		+ "!:~~~~~~~~~~~~~~~~~~~~~~~~~~~============\n"
		+ "!:~~~~~~~~~~~~~~~~~~~~~~~~~~~============\n"
		+ "!:~~~~~~~~~~~~~~~~~~~~~~~~~~~============\n"
		+ "!::~~~~~~~~~HE EATS IT UP~~~~============\n"
		+ "!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~=~=========\n";
    }
}

// vim: set sw=4:
