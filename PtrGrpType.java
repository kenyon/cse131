abstract class PtrGrpType extends CompositeType implements Cloneable
{
	public PtrGrpType(String name)
	{
		super(name);
	}

	public boolean	isPtrGrp ()	{ return true; }

	public String toString () 	{ return "ptrgrp";}

	public Object clone()
	{
	    return super.clone();
	}
}
