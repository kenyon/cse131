import java.util.Vector;

//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class FuncSTO extends STO implements Cloneable
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public
	FuncSTO(String strName)
	{
		super(strName);
		setReturnType(null);
		this.setIsAddressable(true);
	//System.out.println("FuncSTO: Using the 1 arg ctor: (" + strName + ")");
	}

	/**
	 * Create a FuncSTO with a given name and return type.
	 */
	public
	FuncSTO(String name, Type retType)
	{
		super(name, new FuncPtrType());
		this.m_type = new FuncPtrType(retType);
		this.setIsAddressable(true);
	//System.out.println("FuncSTO: Using the 2 arg ctor: (" + name + ", " + retType + ")");
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	isFunc ()
	{
		return true;
	}


	//----------------------------------------------------------------
	// This is the return type of the function. This is different from 
	// the function's type (for function pointers).
	//----------------------------------------------------------------
	public void
	setReturnType(Type typ)
	{
		this.m_type.setReturnType(typ);
	}

	public FuncPtrType
	getType ()
	{
		return	this.m_type;
	}

	public Type
	getReturnType ()
	{
		return this.m_type.getReturnType();
	}

	public int
	getNumParams()
	{
		return m_numParams;
	}

	public void
	setNumParams(int n)
	{
		this.m_numParams = n;
	}

	public void
	setParamList(Vector<VarSTO> params)
	{
		this.m_paramList = params;
	}
	
	public Vector<VarSTO>
	getParamList()
	{
		return m_paramList;
	}
	
	public boolean
	hasReturnedYet()
	{
		return hasReturned;
	}
	
	public void
	hasReturnedYes()
	{
		hasReturned = true;
	}

	public Object clone()
	{
	    FuncSTO copy = (FuncSTO)super.clone();

	    if (m_type != null) {
		copy.m_type = (FuncPtrType)m_type.clone();
	    }

	    if (m_paramList != null) {
		copy.m_paramList = (Vector<VarSTO>)m_paramList.clone();
	    }

	    return copy;
	}

//----------------------------------------------------------------
//	Instance variables.
//----------------------------------------------------------------
	private FuncPtrType 	m_type;
	private Vector<VarSTO>	m_paramList;
	private int		m_numParams;
	private boolean hasReturned;
	public static int m_localsOffset;
}
