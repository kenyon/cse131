
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java_cup.runtime.*;
import java.util.Stack;
import java.util.Vector;



class MyParser extends parser
{

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	MyParser (Lexer lexer, ErrorPrinter errors)
	{
		m_lexer = lexer;
		m_symtab = new SymbolTable ();
		m_errors = errors;
		m_nNumErrors = 0;
		//current_scope = new Stack<String>();
       		//current_scope.push("global");
		m_icStructIds = new Vector<Type>();
		aw = new AssemblyCodeGenerator(OUTPUT_ASSEMBLY_FILE);
	}

	/**
	 * Called when the parser is done, from RC.java.
	 */
	public void
	close()
	{
		aw.dispose();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	Ok ()
	{
		return (m_nNumErrors == 0);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Symbol
	scan ()
	{
		Token		t = m_lexer.GetToken ();

		//	We'll save the last token read for error messages.
		//	Sometimes, the token is lost reading for the next
		//	token which can be null.
		m_strLastLexeme = t.GetLexeme ();

		switch (t.GetCode ())
		{
			case sym.T_ID:
			case sym.T_ID_U:
			case sym.T_STR_LITERAL:
			case sym.T_FLOAT_LITERAL:
			case sym.T_INT_LITERAL:
			case sym.T_CHAR_LITERAL:
				return (new Symbol (t.GetCode (), t.GetLexeme ()));
			default:
				return (new Symbol (t.GetCode ()));
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	syntax_error (Symbol s)
	{
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	report_fatal_error (Symbol s)
	{
		m_nNumErrors++;
		if (m_bSyntaxError)
		{
			m_nNumErrors++;

			//	It is possible that the error was detected
			//	at the end of a line - in which case, s will
			//	be null.  Instead, we saved the last token
			//	read in to give a more meaningful error 
			//	message.
			m_errors.print (Formatter.toString (ErrorMsg.syntax_error, m_strLastLexeme));
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	unrecovered_syntax_error (Symbol s)
	{
		report_fatal_error (s);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	DisableSyntaxError ()
	{
		m_bSyntaxError = false;
	}

	public void
	EnableSyntaxError ()
	{
		m_bSyntaxError = true;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String 
	GetFile ()
	{
		return (m_lexer.getEPFilename ());
	}

	public int
	GetLineNum ()
	{
		return (m_lexer.getLineNumber ());
	}

	public void
	SaveLineNum ()
	{
		m_nSavedLineNum = m_lexer.getLineNumber ();
	}

	public int
	GetSavedLineNum ()
	{
		return (m_nSavedLineNum);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoProgramStart()
	{
		// Opens the global scope.
		m_symtab.openScope ();
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoProgramEnd()
	{
		m_symtab.closeScope ();
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoVarDecl (Vector lstIDs, Type t, STO STATIC)
	{
	    if (t == null) {
		//System.out.println("Error: WTF, type is null!?!");
	    }

	   //System.out.println("DoVarDecl(" + lstIDs + ", " + t + ")");

	    STO fullsto = null;
	    Type t_given;

	    boolean isPointerType = false;
	    boolean isStructType = false;
	    boolean isArrayType = false;
	    boolean initialized = true;

	    //System.out.println("DoVarDecl: lstIDs=" + lstIDs);
		    //+ " ...type: " + t + " ...pointed type: " + ((PointerType)t).getPointedType());

	    for (int i = 0; i < lstIDs.size (); i++) {
		String id = (String)lstIDs.elementAt(i);

		//System.out.println("DoVarDecl: current id is " + id);

		if (i < lstIDs.size() - 1 && i % 2 == 0) {
		    fullsto = (STO)lstIDs.elementAt(i+1);

		    if (fullsto != null) {
			//System.out.println("DoVarDecl: fullsto=" + fullsto);
			//fullsto.setName(id);
			t_given = (Type)fullsto.getType();
		    } else {
			initialized = false;
			t_given = null;
		    }

		    //System.out.println("DoVarDecl: t_given class=" + t_given.getClass() + ", OptModifierList=" + t_given.getOptModifierList());
		    //System.out.println("DoVarDecl: t = " + t + ", OptModifierList=" + t.getOptModifierList());

		    if (t.getOptModifierList() != null) {
			//System.out.println("optmodlist : " + t.getOptModifierList().length());
			isPointerType = true;
		    }

		    if (t.isStruct()) {
			//System.out.println("glockenspuel" + ((StructType)t).getFields());
			isStructType = true;
		    }

		    if (t.isArray())
		    {
			isArrayType = true;
			//id = id + "_0_";
			//System.out.println("lollerskates the ucsd bird! " + id);
			
			//this.incOffset();
			//VarSTO arr = new VarSTO(id, new ArrayType());
			//arr.setArrayLoc(new ConstSTO("1", 1.0));
			//arr.setLocalOffset(this.getOffset());
			
			//m_symtab.insert(arr);
		    }

		    if (t_given != null && t_given.isFuncPtr()) {
			//System.out.println("DoVarDecl: t_given is a FuncPtrType; t_given's return type: " + ((FuncPtrType)t_given).getReturnType());
		    }

		    if (t_given != null && !t_given.isAssignable(t)) {
			if (t_given != null) {
			    if (t_given.isFuncPtr()) {
				if (!(((FuncPtrType)t_given).getReturnType()).isAssignable(t)) {
				    m_nNumErrors++;
				    m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, t_given, t));
				}
			    }
			}
		    }

		    i++;
		   //System.out.println("i = " + i);
		}

		//System.out.println("DoVarDecl(): t: " + t + " id: " + id);
		//System.out.println("symtam.accessLocal(" + id + ") = " + m_symtab.accessLocal(id));
		if (m_symtab.accessLocal(id) != null) {
		    m_nNumErrors++;
		    m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
		}

		//System.out.println("DoVarDecl: creating (id=" + id + ", t=" + t + ")");

		VarSTO 		sto = null;

		if (isPointerType) {
		    PointerType tptr = new PointerType(t);
		    tptr.setPointerLevel(t.getOptModifierList().length());

		    //System.out.println("getptdtype: " + ((PointerType)t).getPointedType());

		    if (isStructType) {
			//System.out.println("i made a pointer to a struct with fields: " + ((StructType)t).getFields());
			TypedefSTO structsto = new TypedefSTO(id + "*", t, false);
			if (structsto == null)
			{
			    //System.out.println("INSERTING NULL STO INTO SYMTAB IN DOVARDECL STRUCT");
			}
			m_symtab.insert(structsto);
			m_symtab.markStructComplete(id + "*");
			m_symtab.insertStructFields(id + "*", ((StructType)t).getFields());
			
			//StructType strct = new StructType();
		    }
		    //System.out.println("DoVarDecl: making pointer");

		    sto = new VarSTO (id, tptr);
		} else if (t.isPointer()) {
		    //System.out.println("i made a pointer to a struct with fields: " + id);;
		    TypedefSTO structsto = new TypedefSTO(id + "*", new StructType(), false);
		    //System.out.println(structsto + " OMG LOL MAN");
		    if (structsto == null)
			{
			    //System.out.println("INSERTING NULL STO INTO SYMTAB IN DOVARDECL STRUCT");
			}
		    m_symtab.insert(structsto);
		    m_symtab.markStructComplete(id + "*");
		    //System.out.println("newbs! " + t.getAliasType());
		    m_symtab.insertStructFields(id + "*", ((StructType)m_symtab.access(t.getAliasType() + "*").getType()).getFields());
		    sto = new VarSTO(id, t);
		} else if (isStructType) {
		    // not sure why we have the below lines, but commenting them seems to make structs better for the moment.
		    //this.DoStructdefDecl_1(id + "*", (StructType)t);
		    //this.DoStructdefDecl(id + "*", ((StructType)t).getFields());

		    sto = new VarSTO(id, t);
		    //System.out.println("isStructType, sto=" + sto);
		    //System.out.println("  fields=" + ((StructType)sto.getType()).getFields());
		    aw.writeStructDef(sto);
		} else {
		    sto = new VarSTO (id, t);
		}

		//if (initialized && fullsto.isConst() || !initialized)
		//{
		    //this.incOffset();
		//}
		//else
		//{
		    //sto.setLocalOffset(m_localsOffset);
		    //aw.writeAssembly("! " + sto.getName() + "'s offset has been set to [%%fp-" + m_localsOffset +   "]");
		//}

		// handling global variable declarations
		if (m_symtab.getFunc () == null) { // we're in global namespace
		    //System.out.println("DoVarDecl: fullsto=" + fullsto);

		    if (firstGlobal) {
			firstGlobal = false;
			aw.writeGlobalHeader();
		    }

		    if (isPointerType)
		    {
			aw.writeVarDecl(id, 0 + "");
		    }
		    else if (initialized) {
			Type stoType = fullsto.getType();
			//System.out.println("DoVarDecl: stoType=" + stoType);
			//System.out.println("sto is const: " + fullsto.isConst());
			if (t.isArray())
			{
			    //System.out.println("OMGWTFBBQ");
			    sto.setName(sto.getName() + "_0_");
			    //System.out.println("sto name " + sto.getName());
			    aw.writeArrayDef(sto);
			}
			else if (t.isInt()) {
			    if (fullsto.isConst()) {
				aw.writeVarDecl(id, ((ConstSTO)fullsto).getIntValue()+"");
			    } else {
				aw.writeVarDecl(id, fullsto, t);
				sto.setLocalOffset(0);
			    }
			} else if (t.isFloat()) {
			    if (fullsto.isConst()) {
				aw.writeFloatDecl(id, ((ConstSTO)fullsto).getFloatValue()+"");
			    } else {
				aw.writeVarDecl(id, fullsto, t);
				sto.setLocalOffset(0);
			    }
			} else if (t.isBool()) {
			    if (fullsto.isConst()) {
				int boolval = ((ConstSTO)fullsto).getBoolValue() ? 1 : 0;
				//System.out.println("DoVarDecl: " + boolval);

				aw.writeVarDecl(id, boolval+"");
			    } else {
				aw.writeVarDecl(id, fullsto, t);
				sto.setLocalOffset(0);
			    }
			}
		    } else { // uninitialized, initialize it to 0 per spec I.1
			if (t.isArray())
			{
			    //System.out.println("OMGWTFBBQ");
			    sto.setName(sto.getName() + "_0_");
			    aw.writeArrayDef(sto);
			}
			else if (t.isFloat()) {
			    aw.writeFloatDecl(id, 0+"");
			} else {
			    aw.writeVarDecl(id, 0+"");
			}
		    }
		} else { // we're in a function namespace
		    //aw.writeAssembly("!omg it works not, the reg in st above here needs to be changed to %%f5 for a float return value being initialized to a float local var");
		    this.incOffset();
		    sto.setLocalOffset(m_localsOffset);
		    if (initialized) {
			Type stoType = fullsto.getType();
			//System.out.println("DoVarDecl: stoType=" + stoType + ", id=" + id);
			if (t.isArray()) {
			    this.incOffset(((ArrayType) sto.getType()).getDimensionSize()-1);
			    sto.setName(sto.getName() + "_0_");
			    aw.writeArrayDef(sto);
			} else if (t.isInt()) {
			    if (fullsto.isConst()) {
				aw.writeLocalVarDecl(m_localsOffset, ((ConstSTO)fullsto).getIntValue()+"", id);
			    } else {
				//System.out.println("WTF");
				aw.writeLocalGenDecl(sto, fullsto);
			    }
			} else if (t.isFloat()) {
			    if (fullsto.isConst()) {
				aw.writeLocalFloatDecl(m_localsOffset, ((ConstSTO)fullsto).getFloatValue()+"", id);
			    } else {
				aw.writeLocalGenDecl(sto, fullsto);
			    }
			} else if (t.isBool()) {
			    if (fullsto.isConst()) {
				int boolval = ((ConstSTO)fullsto).getBoolValue() ? 1 : 0;
				//System.out.println("DoVarDecl: " + boolval);

				aw.writeLocalVarDecl(m_localsOffset, boolval+"", id);
			    } else {
				aw.writeLocalGenDecl(sto, fullsto);
			    }
			}
		    } else { // uninitialized, do not initialize per spec I.1
			if (t.isArray()) {
			    this.incOffset(((ArrayType) sto.getType()).getDimensionSize()-1);
			    sto.setName(sto.getName() + "_0_");
			    aw.writeArrayDef(sto);
			} else if (t.isFloat()) {
			    aw.writeLocalFloatDecl(m_localsOffset, id);
			} else {
			    aw.writeLocalVarDecl(m_localsOffset, id);
			}
		    }
		}
		//System.out.println("symtab insert: " + sto.getName());
		if (sto == null)
		{
		    //System.out.println("NULL STO BEING INSERTED INTO SYMBOL TABLE IN DOVARDECL!");
		}

		m_symtab.insert (sto);
	    }
	}

	STO
	DoParamDecl(Type t, String ref, String id)
	{
		//System.out.println("DoParamDecl: t.getOptModifierList: " + t.getOptModifierList());

		boolean isPointerType = false;
		boolean isRef = false;

		VarSTO 		sto = null;

		if (m_symtab.accessLocal (id) != null) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
		}

		if (ref != null && ref.equals("&"))
		{
		    isRef = true;
		   //System.out.println("here");
		}
		
		if (t.getOptModifierList() != null) {
			isPointerType = true;
		}

		if (isPointerType) {
			PointerType tptr = new PointerType(t);
			sto = new VarSTO (id, tptr);
		} else {
			sto = new VarSTO (id, t);
			sto.setIsRef(isRef);
		}

		return sto;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoConstDecl (Type t, Vector lstIDs)
	{
		this.incOffset();
		
		for (int i = 0; i < lstIDs.size(); i+=2)
		{
			Type t_given = ((STO)lstIDs.elementAt(i+1)).getType();

			String id = (String) lstIDs.elementAt (i);

			//System.out.println("DoConstDecl: t_given=" + t_given);

			/*
			if (t_given == null) {
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error8_CompileTime, id));
			} else if (!t_given.isAssignable(t) && (t_given != null && t_given.isFuncPtr() && !(((FuncPtrType)t_given).getReturnType()).isAssignable(t))) {
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error8_Assign, t_given, t));
			}
			*/

			//System.out.println("DoConstDecl(): t: " + t + " id: " + id);

			if (m_symtab.accessLocal (id) != null) {
				m_nNumErrors++;
				m_errors.print (Formatter.toString (ErrorMsg.redeclared_id, id));
			}

			//System.out.println("isConst(): " + lstIDs.elementAt(i+1));

			Double val = null;

			if (((STO)lstIDs.elementAt(i+1)).isConst() && !((STO)lstIDs.elementAt(i+1)).isError()) {
				//System.out.println("IST IT IN HEREREREE!!!!????????");

				val = ((ConstSTO)lstIDs.elementAt(i+1)).getValue();
			}

			//System.out.println("val=" + val);

			/*
			if (val == null) {
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error8_CompileTime, id));
			}
			*/

			//System.out.println("DoConstDecl: creating (" + id + ", " + t + ", " + val + ")");
		
			ConstSTO 	sto = new ConstSTO (id, t, val);
			if (sto == null)
			{
			    //System.out.println("INSERTING NULL STO INTO SYMTAB IN CONSTDECL");
			}
			m_symtab.insert (sto);

			// handling global const variable declarations
			if (m_symtab.getFunc () == null) {
				Type stoType = sto.getType();
				//System.out.println("DoVarDecl: stoType=" + stoType);

				if (firstGlobal) {
					firstGlobal = false;
					aw.writeGlobalHeader();
				}

				if (stoType.isInt()) {
					aw.writeVarDecl(id, ((ConstSTO)sto).getIntValue()+"");
				} else if (stoType.isFloat()) {
					aw.writeFloatDecl(id, ((ConstSTO)sto).getFloatValue()+"");
				} else if (stoType.isBool()) {
					int boolval = ((ConstSTO)sto).getBoolValue() ? 1 : 0;
					//System.out.println("DoVarDecl: " + boolval);

					aw.writeVarDecl(id, boolval+"");
				}
			}
		}
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoTypedefDecl (Vector lstIDs, Type t)
	{
		for (int i = 0; i < lstIDs.size (); i++)
		{
			String id = (String) lstIDs.elementAt (i);

			//System.out.println(i + ": " + id);

			if (m_symtab.accessLocal (id) != null)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
			}
		
			TypedefSTO sto;

			//System.out.println("DoTypedefDecl: " + t);

			boolean pointerflag = false;
			PointerType newptr = null;
			
			boolean structflag = false;
			
			if (t.getOptModifierList() != null)
			{
				//System.out.println("DoTypedefDecl: " + t.getOptModifierList());
				int ii = t.getOptModifierList().length();
				
				newptr = new PointerType(t);
				newptr.setPointerLevel(ii);
				
				pointerflag = true;
			}
			
			if (t.isStruct())
			{
				//System.out.println("STRUUUUUUUUUUUUUUUUCT");
				structflag = true;
			}

			if (t.getAliasType() == null) {
				if (pointerflag)
				{
					if (structflag)
					{
					//System.out.println("i made a pointer to a struct " + id + "* with fields: " + ((StructType)t).getFields());
						//this.DoStructdefDecl_1(id + "*", (StructType)t);
						//this.DoStructdefDecl(id + "*", ((StructType)t).getFields());
						TypedefSTO structsto = new TypedefSTO(id + "*", t, false);
						if (structsto == null)
						{
						    //System.out.println("INSERTING NULL STO INTO SYMTAB IN DOTYPEDEFDECL");
						}
						m_symtab.insert(structsto);
						m_symtab.markStructComplete(id + "*");
						m_symtab.insertStructFields(id + "*", ((StructType)t).getFields());
						
						//StructType strct = new StructType();
					}
					newptr.setParentAliasType(newptr.toString());
					newptr.setAliasType(id);
					sto = new TypedefSTO (id, newptr);
				}
				else
				{
					t.setParentAliasType(t.toString());
					t.setAliasType(id);
					sto = new TypedefSTO (id, t);
				}
			} else {
				if (pointerflag)
				{
					newptr.setParentAliasType(newptr.getAliasType());
					newptr.setAliasType(id);
					sto = new TypedefSTO (id, newptr);
				}
				else
				{
					t.setParentAliasType(t.getAliasType());
					t.setAliasType(id);
					sto = new TypedefSTO (id, t);
				}
			}

			//System.out.println("\nnew TypedefSTO: id=" + id + " name=" + sto.getName() + " type=" + sto.getType() + " aliastype=" + sto.getType().getAliasType() + " parentaliastype=" + sto.getType().getParentAliasType());
			if (sto == null)
			{
			    //System.out.println("INSERTING NULL STO INTO SYMTAB IN DOTYPEDEFDECL");
			}
			m_symtab.insert (sto);
		}
	}

	void
	SetCurStruct (Type type, Vector fields)
	{
	//System.out.println("SetCurStruct(" + type + ", " + fields + ")");

		if (type != null) {
			for (int i = 0; i < fields.size(); i++) {
				Type t = null;
				
				if (type.isArray())
				{
					t = new ArrayType();
				}
				if (type.isBool())
				{
					t = new BoolType();
				}
				if (type.isInt())
				{
					t = new IntType();
				}
				if (type.isVoid())
				{
					//t = new VoidType();
				}
				if (type.isFloat())
				{
					t = new FloatType();
				}
				if (type.isFuncPtr())
				{
					t = new FuncPtrType();
				}
				if (type.isPointer())
				{
					t = new PointerType();
				}
				if (type.isStruct())
				{
					t = new StructType();
				}
				
				t.setName((String)fields.elementAt(i));
				//System.out.println("t type is " + t);
				m_icStructIds.addElement(t);
			}
		}
	}

	void
	AddToCurStruct (String func)
	{
	//System.out.println("AddToCurStruct(" + func + ")");
		Type type = new FuncPtrType();
		type.setName(func);
		m_icStructIds.addElement(type);
	}

	void
	ClearCurStruct ()
	{
	//System.out.println("ClearCurStruct() called.");
		m_icStructIds.clear();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoStructdefDecl (String id, Vector fields)
	{
		//System.out.println("start of DoStructdefDecl: id=" + id + " fields=" + fields);

		TypedefSTO symtabresult = (TypedefSTO)m_symtab.accessLocal(id);

		if (symtabresult != null && symtabresult.isCompleteDef()) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
		}

		//System.out.println("top of DoStructdefDecl: symtabresult=" + symtabresult);
		
		Vector<STO> fieldSTOs = new Vector();

		for (int i = 0; i < fields.size(); i++) {
			//System.out.println("fields(" + i + "): " + fields.elementAt(i) + ", class name: " + fields.elementAt(i).getClass().getName());

			if (fields.elementAt(i).getClass().getName() == "java.util.Vector") {
				//System.out.println("element " + i + " is a vector.");
				Vector elementi = (Vector)fields.elementAt(i);

				//System.out.println("this line's type: " + elementi.elementAt(elementi.size() - 1).getClass().getName());

				Type linetype = (Type)elementi.elementAt(elementi.size() - 1);

				//System.out.println("linetype mod list: " + linetype.getOptModifierList());

				if (linetype.getOptModifierList() != null) {
					PointerType tptr = new PointerType(linetype);
					tptr.setPointerLevel(linetype.getOptModifierList().length());

					linetype = tptr;

					//System.out.println("linetype pointed type: " + ((PointerType)linetype).getPointedType());

					//System.out.println("linetype isPointer: " + linetype.isPointer());
				}

				for (int j = 0; j < ((Vector)fields.elementAt(i)).size(); j++) {


					//System.out.println( i + ", " + j + " class: " + ((Vector)fields.elementAt(i)).elementAt(j).getClass().getName() ); 


					// The elements are all identifiers except the last one.
					if ( j < ((Vector)fields.elementAt(i)).size() - 1 ) {
						String identifier = (String)((Vector)fields.elementAt(i)).elementAt(j);

						//System.out.println("adding VarSTO(" + identifier + ", " + linetype + ")");

						VarSTO fieldSTO = new VarSTO(identifier, linetype);

						fieldSTOs.addElement( fieldSTO );
					}
				}

			} else if (fields.elementAt(i).getClass().getName() == "FuncSTO") {
				//System.out.println("FuncSTO name = " + ((FuncSTO)fields.elementAt(i)).getName());

				fieldSTOs.addElement( (FuncSTO)fields.elementAt(i) );
			}

		}  //.getClass().getName()

		/*
		for (int i = 0; i < ids.size(); i++) {
		//System.out.println("ids[" + i + "]=" + ids.elementAt(i));
		}
		*/

		fieldSTOs = dupefinder(fieldSTOs);
		//System.out.println("fieldSTOs: " + fieldSTOs);


		//System.out.println(fields.getClass().getName());

		//System.out.println("id is " + id);

		// here, instead of just inserting to symbol table, see if
		// identifier already is in there and if it is and is marked
		// not complete, then tell the symbol table to mark it
		// complete.

		//System.out.println("DoStructdefDecl: " + symtabresult);

		boolean isCompleteStructDef = false;

		if ( symtabresult.isStruct() ) {
			isCompleteStructDef = symtabresult.isCompleteDef();

			//System.out.println("symtabresult.isCompleteDef()=" + isCompleteStructDef);
		}

		if (!isCompleteStructDef) {
			//System.out.println("!isCompleteStructDef: symtabresult=" + symtabresult);

			//System.out.println("tell symtab to mark id=" + symtabresult.getName() + " complete");

			m_symtab.markStructComplete(symtabresult.getName());

			isCompleteStructDef = true;

			m_symtab.insertStructFields(symtabresult.getName(), fieldSTOs);

			//System.out.println("symtab: " + m_symtab.accessLocal(symtabresult.getName()));

			//System.out.println("symtab: " + m_symtab.accessLocal("booya"));
		}


		// FIXME: In addition to marking complete, we need to put
		// fieldSTOs in the TypedefSTO that is already in the symtab.
		//
		// Look at why t14i.rc refs to struct member return the type of
		// the member (float or whatever) instead of struct like 15b
		// and 14f.

		//TypedefSTO 	sto = new TypedefSTO (id, new StructType(fieldSTOs));

		//System.out.println("fields into: " + id + " : are : " + fieldSTOs);

		//m_symtab.insert (sto);

		//System.out.println("m_nNumErrors=" + m_nNumErrors);

		//System.out.println("end DoStructdefDecl " + m_nNumErrors);
	}

	/**
	 * Inserts an incomplete struct definition into the symbol table.
	 */
	void DoStructdefDecl_1(String id, StructType t)
	{
		//TypedefSTO structsto = new TypedefSTO(id + "#", t, false);
		TypedefSTO structsto = new TypedefSTO(id, t, false);

		if(structsto == null)
		{
		    //System.out.println("INSERING NULL STO INTO SYMTAB IN DOSTRUCTDEFDECL_1");
		}
		m_symtab.insert (structsto);
	}

	private int old_numErrors;

	private Vector dupefinder(Vector ids)
	{
		for (int i = 1; i < ids.size(); i++) {
			for (int j = i - 1; j >= 0; j--) {
				/*
			//System.out.println("looking at i="
						+ i + "(" + ids.elementAt(i).getClass().getName() + ", name=" + ((STO)ids.elementAt(i)).getName() + "), j="
						+ j + "(" + ids.elementAt(j).getClass().getName() + ", name=" + ((STO)ids.elementAt(j)).getName() + ")");
				*/

				if (((STO)ids.elementAt(i)).equals((STO)ids.elementAt(j))) {
					//System.out.println("found dupes: i=" + i + "=" + ids.elementAt(i) + ", j=" + j + "=" + ids.elementAt(j));

					old_numErrors++;

					m_nNumErrors++;
					m_errors.print(Formatter.toString(ErrorMsg.error13_Struct, ((STO)ids.elementAt(i)).getName()));
					m_nNumErrors--;


					ids.removeElementAt(i);
					//System.out.println("size: " + ids.size());
					return dupefinder(ids);
				}

			}
		}

		m_nNumErrors += old_numErrors;

		return ids;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoFuncDecl_1 (String id, Type t)
	{
		if (m_typedefCount <= 0 && m_symtab.accessLocal (id) != null) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.redeclared_id, id));
		}
	
		//System.out.println("making new FuncSTO(" + id + ", " + t + ")");

		this.resetLocalsOffset();

		FuncSTO sto = new FuncSTO (id, t);
		if (sto == null)
		{
		    //System.out.println("INSERTING NULL STO INTO SYMTAB IN DOFUNCDECL");
		}
		m_symtab.insert (sto);

		m_symtab.openScope ();
		m_symtab.setFunc (sto);

		aw.writeFuncPrologue(sto);
		
		if (id.equals("main"))
		{
			aw.writeOneTimeGlobalExprInits();
		}

		return sto;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoFuncDecl_2 ()
	{
		String funcname = m_symtab.getFunc().getName();

		if (m_symtab.getFunc() != null) {
		    //System.out.println(m_symtab.getFunc().getName() + " is bein tested for funkyness!");

		    /*
		    //System.out.println("DoFuncDecl_2: m_symtab.getFunc().getReturnType(): "
			    + m_symtab.getFunc().getReturnType());
		    */

		    if (!m_symtab.getFunc().hasReturnedYet() && m_symtab.getFunc().getReturnType() != null) {
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error6b_Return_missing);
		    }
		}

		aw.writeFuncEpilogue(m_symtab.getFunc(), m_localsOffset);

		m_symtab.closeScope ();
		m_symtab.setFunc (null);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoFormalParams (Vector<VarSTO> params)
	{
		if (m_symtab.getFunc () == null)
		{
			m_nNumErrors++;
			m_errors.print ("internal: DoFormalParams says no proc!");
		}

		if (params != null) {
			//System.out.println("params list: " + params);

			for (int i = 0; i < params.size(); i++) {
			    	//System.out.println("param " + i + ": " + params.elementAt(i) + " | " + ((STO)params.elementAt(i)).getIsRef() + " | " + ((STO)params.elementAt(i)).getLocalOffset());
				VarSTO param = params.elementAt(i);
				param.setLocalOffset(-68-i*4);

				//System.out.println("param " + i + ": " + param.getType().getOptModifierList());
				if (params.elementAt(i) == null)
				{
				    //System.out.println("INSERTING NULL STO INTO SYMTAB IN DOFORMALPARAMS");
				}
				m_symtab.insert(params.elementAt(i));

				//DoVarDecl((Vector)params.elementAt(i), ((STO)params.elementAt(i)).getType());
			}

			m_symtab.getFunc().setNumParams(params.size());
			m_symtab.getFunc().setParamList(params);
			aw.writeFormalParams(m_symtab.getFunc(), params);
		}
	}



	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoBlockOpen ()
	{
		//System.out.println("FULL OF FAIL OPEN = " + s);
		
		//current_scope.push(s);
		aw.writeAssembly("!{");
		
		aw.increaseIndent();

		m_symtab.openScope ();
		
		//whileFlag = true;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoBlockClose()
	{
		//System.out.println("FULL OF FAIL CLOSE");
		//current_scope.pop();
		aw.decreaseIndent();
		aw.writeAssembly("!}");
		m_symtab.closeScope ();
		//whileFlag = false;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	void
	DoAssignStmt (STO left, STO right)
	{
		//System.out.println("DoAssignStmt(" + left + ", " + right + ")");
		//System.out.println("DoAssignStmt(" + left.getType().isPointer() + ")"); 

		// No erroneous code will be tested, therefore all assignments are legal, do it!
	    	aw.writeAssignStmt(left, right);

	    	if (right.isFunc())
	    	{
	    	    /*String id = left.getName();
	    	    left = (FuncSTO)right.clone();
	    	    left.setIsModifiable(true);
	    	    left.setIsAddressable(true);
	    	    left.setName(id);
	    	    //m_symtab.insert(left);
	    	    //System.out.println("DoAssignStmt(" + left + ", " + right + ")");*/
	    	    //m_symtab.setPointedFunc(left.getName(), right);
	    	    //System.out.println("left side " + left.hashCode());
	    	    left.setPointedFunc((FuncSTO)right);
	    	}

	    	if (!right.isFunc() && right.getType().isFuncPtr())
	    	{
	    	    //System.out.println("RAPTOOOOOOOOOOOR!");
	    	    left = right;
	    	}
		
		if (!left.isModLValue())
		{
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error3a_Assign);
		}

		/*
		if (left.getType().isPointer()) {
		//System.out.println("DoAssignStmt(): left is a pointer, it points to a " + ((PointerType)left.getType()).getPointedType());
		}
		*/

		if (left.getType() != null
		    && right.getType() != null
		    && !right.getType().isAssignable(left.getType())
		    && !(right.getType().isPtrGrp() && left.getType().isPtrGrp()))
		{
			if (right instanceof FuncSTO
			    && ((FuncSTO)right).getReturnType() != null
			    && !((FuncSTO)right).getReturnType().isAssignable(left.getType()))
			{

				//m_nNumErrors++;
				//m_errors.print(Formatter.toString(ErrorMsg.error3b_Assign, right.getType(), left.getType()));
			}

			//System.out.println("DoAssignStmt(out if): " + right.getName() + " (" + right.getType() + ", alias " + right.getType().getAliasType() + "): " + right + ", " + left.getName() + " (" + left.getType() + "): " + left);
			
			//System.out.println("DoAssignStmt(in if): " + right.getType() + " <<<right | left>>>" + left.getType());
			
			//m_nNumErrors++;

			//System.out.println("FUCK THIS.");
			//System.out.println("rettype " + ((FuncSTO)right).getReturnType() + " lefttype " + left.getType());
			
			//m_errors.print(Formatter.toString(ErrorMsg.error3b_Assign,
						//right.getType().getAliasType() != null ? right.getType().getAliasType() : right.getType(),
						//left.getType().getAliasType() != null ? left.getType().getAliasType() : left.getType()));
					//right.isTypedef() ? right.getType() : right.getType().getAliasType() + "lollllllllllllllll",
					//left.isTypedef() ? left.getName() : left.getType().getAliasType() + "rofllllllll"));
		}

		//if (left.getType
	}

	void
	DoIfStmt(STO expr)
	{
		//System.out.println("WHAT THE FOOOK: " + expr.getType());

	    	aw.writeIfStmt(expr);

		/*
		if (expr.getType() != null && !expr.getType().isBool()) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error4_Test, expr.getType()));
		}
		*/
	}

	void
	DoWhileStmt(STO expr)
	{
		//System.out.println(expr);

		//current_scope.pop();
		//current_scope.push("while");

		aw.writeWhile(expr);

		/*
		if (!expr.getType().isBool())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error4_Test, expr.getType()));
		}
		*/
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoFuncCall (STO sto, Vector<STO> exprs)
	{
	    //this.resetLocalsOffset();

	    //aw.writeFuncCall(sto, exprs);

	    if (sto instanceof VarSTO && ((VarSTO)sto).getType().isFuncPtr()) {
		//System.out.println("here");
		return DoFuncPtrCall(sto, exprs);
	    }

	   //System.out.println("DoFuncCall: sto=" + sto);

	    //System.out.println("DoFuncCall: exprs=" + exprs);

	    if (!(sto instanceof FuncSTO)) {
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.not_function, sto.getName()));
		return (new ErrorSTO (sto.getName ()));
	    }

	    //FuncSTO fsto = (FuncSTO)sto;

	   //System.out.println("DoFuncCall: sto " + sto.getName() + "'s type=" + sto.getType() + ", return type=" + ((FuncPtrType)sto.getType()).getReturnType());

	   //System.out.println("DoFuncCall: cloning sto -> fsto.");

	    FuncSTO fsto = (FuncSTO)sto.clone();

	    //FuncPtrType stotype = (FuncPtrType)((FuncSTO)sto).getType().clone();
	    //System.out.println("stotype=" + stotype);

	    //FuncPtrType stotype = new FuncPtrType(((FuncPtrType)sto.getType()).getReturnType(), ((FuncPtrType)sto.getType()).getParamList());
	    //FuncSTO fsto = new FuncSTO(sto.getName(), stotype);

	   //System.out.println("DoFuncCall: fsto " + fsto.getName() + "'s type=" + fsto.getType() + ", return type=" + fsto.getType().getReturnType());

	    if (exprs == null && fsto.getNumParams() != 0) {
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.error5n_Call, "0", (fsto).getNumParams()));
		return (new ErrorSTO (fsto.getName ()));
	    } else if (exprs != null && fsto.getNumParams() != exprs.size()) {
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.error5n_Call, exprs.size(), (fsto).getNumParams()));
		return (new ErrorSTO (fsto.getName ()));
	    }

	    FuncSTO oldsto = m_symtab.getFunc();
	   //System.out.println("DoFuncCall: oldsto " + oldsto.getName() + "'s type=" + oldsto.getType() + ", return type=" + oldsto.getType().getReturnType());
	    m_symtab.openScope();
	    m_symtab.setFunc(fsto);

/*	    if (exprs != null) {
		for (int i = 0; i < exprs.size(); i++) {
		    if (exprs.elementAt(i).getType() != null
			    && m_symtab.getFunc().getParamList().elementAt(i).getType() != null) {
			if (!exprs.elementAt(i).getType().isAssignable(m_symtab.getFunc().getParamList().elementAt(i).getType())) {
			    m_nNumErrors++;
			    m_errors.print (Formatter.toString(ErrorMsg.error5a_Call, ((STO)exprs.elementAt(i)).getType(),
					    m_symtab.getFunc().getParamList().elementAt(i).getName(), m_symtab.getFunc().getParamList().elementAt(i).getType()));

			    m_symtab.closeScope ();
			    m_symtab.setFunc (oldsto);
			    return (new ErrorSTO (fsto.getName ()));
			}
		    }
		}
	    }*/

	    m_symtab.closeScope();
	    m_symtab.setFunc(oldsto);

	   //System.out.println("giving fsto to writeFuncCall; fsto=" + fsto + fsto.hashCode());
	    return aw.writeFuncCall(fsto, exprs);
	}

	STO
	DoFuncPtrCall(STO sto, Vector exprs)
	{
	    // FIXME: this is bullshit
	    //System.out.println("doing a func ptr call " + sto.hashCode() + " pointing to " + sto.getPointedFunc());// + exprs);
	    //return aw.writeFuncPtrCall(sto, exprs);
	    
	    
	    
	    return aw.writeFuncCall(sto.getPointedFunc(), exprs);
	    
	    //return sto;
	}
	
	STO
	DoRetStmt (STO sto)
	{
		m_symtab.getFunc().hasReturnedYes();

		//System.out.println("this func is now funky! " + m_symtab.getFunc().getName());
		
		if (sto == null && m_symtab.getFunc().getReturnType() != null) {
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error6a_Return_expr);
			return (new ErrorSTO ("null"));
		}

		//System.out.println(sto.getType() + " " + m_symtab.getFunc().getReturnType());

		if (sto != null && sto.getType() != null) {
			if (sto != null
				&& !(sto.getType()).isAssignable(m_symtab.getFunc().getReturnType())
				&& (sto.getType().isFuncPtr()
				    && !((FuncPtrType)sto.getType()).getReturnType().isAssignable(m_symtab.getFunc().getReturnType())))
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error6a_Return_type, sto.getType(), m_symtab.getFunc().getReturnType()));
				return (new ErrorSTO (sto.getName ()));
			}

		} else if (sto != null) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, sto.getName()));
			return (new ErrorSTO ("ERROR in DoRetStmt BITCHHHHH"));
		}

		aw.writeRetStmt(m_symtab.getFunc(), sto);

		//System.out.println("return value: " + csto.getIntValue());
		
		

		//System.out.println("making return type of " + m_symtab.getFunc() + ": "+ sto.getType());
		if (sto != null && sto.getType() != null)
		{
		    ((FuncPtrType)m_symtab.getFunc().getType()).setReturnType(sto.getType());
		}
		
		//System.out.println("return type is now: " + m_symtab.getFunc());
		
		return sto;
	}

	STO
	DoExitStmt (STO sto)
	{
	    aw.writeExit(sto);

		if (!sto.getType().isAssignable(new IntType()))
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error7_Exit, sto.getType()));
			return (new ErrorSTO (sto.getName ()));
		}
		return sto;
	}
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoDesignator2_Dot (STO sto, String strID)
	{
		//System.out.println("        Sto is: " + sto.getName() + " --- Sto type is " + sto.getType() + " --- Id is: " + strID);
	    String mangled_name = sto.getName() + "." + strID;


		if (sto.getName().equals("this"))
		{
			boolean exists = false;

			STO sto2 = null;

			for (int i = 0; i < m_icStructIds.size(); i++)
			{
				//System.out.println("trying to find id: " + strID + " --- in struct, found: " + m_icStructIds.elementAt(i).getName());
				if (strID.equals(m_icStructIds.elementAt(i).getName()))
				{
					exists = true;

					//System.out.println("              found a match! type: " + (m_icStructIds.elementAt(i) + " --- name: " + m_icStructIds.elementAt(i).getName()));
					
					if (m_icStructIds.elementAt(i).isArray())
					{
						sto2 = new VarSTO(m_icStructIds.elementAt(i).getName(), new ArrayType());
					}
					if (m_icStructIds.elementAt(i).isBool())
					{
						sto2 = new VarSTO(m_icStructIds.elementAt(i).getName(), new BoolType());
					}
					if (m_icStructIds.elementAt(i).isInt())
					{
						sto2 = new VarSTO(m_icStructIds.elementAt(i).getName(), new IntType());
					}
					if (m_icStructIds.elementAt(i).isVoid())
					{
						//t = new VoidType();
					}
					if (m_icStructIds.elementAt(i).isFloat())
					{
						sto2 = new VarSTO(m_icStructIds.elementAt(i).getName(), new FloatType());
					}
					if (m_icStructIds.elementAt(i).isFuncPtr())
					{
						sto2 = new FuncSTO(m_icStructIds.elementAt(i).getName(), new FuncPtrType());
					}
					if (m_icStructIds.elementAt(i).isPointer())
					{
						//System.out.println("LOL HERE POINTAR!");

						sto2 = new VarSTO(m_icStructIds.elementAt(i).getName(), new PointerType());
					}
					if (m_icStructIds.elementAt(i).isStruct())
					{
						//System.out.println("LOL HERE STRACT!");

						sto2 = new TypedefSTO(m_icStructIds.elementAt(i).getName(), new StructType());
					}
				}
			}

			if (!exists)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error14b_StructExpThis, strID));
				return (new ErrorSTO (sto.getName ()));
			}
			return sto2;
		}

		if (sto.getType() != null && !sto.getName().equals("this") && !sto.getType().isStruct())
		{
			m_nNumErrors++;
			//System.out.println("sto name is: " + sto.getName() + " : and id is : " + strID);
			m_errors.print (Formatter.toString(ErrorMsg.error14t_StructExp, sto.getType()));
			return (new ErrorSTO (sto.getName ()));
		}

		//aw.writeAssembly("! WHY HELLO THAR.");

		if (!sto.getName().equals("this")) {
			//System.out.println( "LOLOLLOULOLUOLEUOEUUPENISAHHA " + ((StructType)sto.getType()).getFields() );
			//System.out.println("cockle: " + sto.getType());

			if (sto.getType() == null || ((StructType)sto.getType()).getFields() == null) {
				//System.out.println("RETURNES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				return sto;
			}

			Vector<STO> penis = ((StructType)sto.getType()).getFields();

			boolean foundFlag = false;
			
			for (int i = 0; i < penis.size(); i++) {
				//System.out.println("strID = " + strID + "  & penis element = " + penis.elementAt(i).getName());
				
				if (penis.elementAt(i).getType() != null && strID.equals(penis.elementAt(i).getName()))
				{
					//System.out.println(penis.elementAt(i));
					foundFlag = true;
					
					if (penis.elementAt(i).getType().isArray())
					{
						sto = new VarSTO(penis.elementAt(i).getName(), new ArrayType());
					}
					if (penis.elementAt(i).getType().isBool())
					{
						sto = new VarSTO(penis.elementAt(i).getName(), new BoolType());
					}
					if (penis.elementAt(i).getType().isInt())
					{
						sto = new VarSTO(penis.elementAt(i).getName(), new IntType());
					}
					if (penis.elementAt(i).getType().isVoid())
					{
						//t = new VoidType();
					}
					if (penis.elementAt(i).getType().isFloat())
					{
						sto = new VarSTO(penis.elementAt(i).getName(), new FloatType());
					}
					if (penis.elementAt(i).getType().isFuncPtr())
					{
						sto = new FuncSTO(penis.elementAt(i).getName(), new FuncPtrType());
					}
					if (penis.elementAt(i).getType().isPointer())
					{
						//System.out.println("LOL HERE POINTAR!");

						sto = new VarSTO(penis.elementAt(i).getName(), new PointerType());
					}
					if (penis.elementAt(i).getType().isStruct())
					{
						//System.out.println("LOL HERE STRACT!");

						sto = new TypedefSTO(penis.elementAt(i).getName(), new StructType());
					}

				}
			}

			if (!foundFlag)
			{
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error14f_StructExp, strID, sto.getName()));
				return (new ErrorSTO (sto.getName ()));
			}
		}
		
		// for assembly writing name mangled global struct members
		sto.setName(mangled_name);

		return sto;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoDesignator2_Array (STO sto, STO exprsto)
	{
	    //System.out.println("sto " + sto + " exprsto " + exprsto);
	    
		if (sto.getType() != null && (!sto.getType().isArray() && !sto.getType().isPointer())) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error11t_ArrExp, sto.getType()));
			return new ErrorSTO (sto.getName ());
		}

		if (!exprsto.getType().isInt()) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error11i_ArrExp, sto.getType()));
			return new ErrorSTO (sto.getName ());
		}

		if (sto.getType() != null && !sto.getType().isPointer() && exprsto.isConst()) {

			//System.out.println("XPERSTO: " + ((ConstSTO)exprsto).getIntValue());

			if (((ConstSTO)exprsto).getIntValue() > ((ArrayType)(sto.getType())).getDimensionSize() - 1) {
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error11b_ArrExp, ((ConstSTO)exprsto).getIntValue(), ((ArrayType)(sto.getType())).getDimensionSize() - 1));
				return new ErrorSTO (sto.getName ());
			}
		}


		if (sto.getType().isPointer()) {
			return new VarSTO(sto.getName(), ((PointerType)sto.getType()).getPointedType());
		}

		VarSTO sto2 = new VarSTO(sto.getName() + "_base_offsetby_" + exprsto.getName() + "_", ((ArrayType)sto.getType()).getElementType());

		sto2.setLocalOffset(sto.getLocalOffset());

		if (exprsto == null)
		{
		    //System.out.println("A null index sto was put into arrayLoc of " + sto2.getName());
		}
		
		//System.out.println("arrayLoc of " + sto2.getName() + " is now " + exprsto.getName());
		
		sto2.setArrayLoc(exprsto);
		
		aw.writeRunTimeArrayBoundsCheck(sto, exprsto);
		//aw.writeAssembly("!here");
		return sto2;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoDesignator3_ID (String strID)
	{
		STO sto = null;

		if ((sto = m_symtab.access (strID)) == null && m_symtab.access(strID + "_0_") == null)
		{
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, strID));	
			sto = new ErrorSTO (strID);
		}
		
		if (sto == null && m_symtab.access(strID + "_0_") != null)
		{
		    sto = m_symtab.access(strID + "_0_");
		}
		return (sto);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	STO
	DoQualIdent (String strID)
	{
		STO sto;

		//System.out.println("DoQualIdent(" + strID + ")");

		if ((sto = m_symtab.access(strID)) == null && m_symtab.access(strID + "_0_") == null) {
			m_nNumErrors++;
		 	//m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID) + " DoQualIdent");
		 	m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
			return new ErrorSTO (strID);
		}

		if (!sto.isTypedef() && !sto.getType().isStruct()) {
			m_nNumErrors++;
			m_errors.print(Formatter.toString(ErrorMsg.not_type, sto.getName ()));
			return new ErrorSTO (sto.getName ());
		}

		//System.out.println("doqual: sto fields is: " + ((StructType)sto.getType()).getFields());
		
		return sto;
	}

	STO
	DoUnary(String sign, STO designator)
	{
	    if (designator.isConst())
	    {
		ConstSTO csto = null;
		if (sign.equals("-"))
		{
		    csto = new ConstSTO(designator.getName(), designator.getType(), -((ConstSTO)designator).getValue());
		}
		else
		{
		    csto = new ConstSTO(designator.getName(), designator.getType(), ((ConstSTO)designator).getValue());
		}
		csto.setLocalOffset(designator.getLocalOffset());
		return csto;
	    }
	    
	    ExprSTO sto = null;

	    if (designator.getType().isFloat())
	    {
		sto = new ExprSTO(designator.getName(), new FloatType());
	    }
	    else if (!designator.getType().isFloat())
	    {
		sto = new ExprSTO(designator.getName(), new IntType());
	    }

	    this.incOffset();

	    aw.writeUnary(sign, designator);
	    sto.setLocalOffset(m_localsOffset);

	    return sto;
	}
	
	STO
	DoBitOp(STO left, String oper, STO right)
	{
	    ExprSTO sto = new ExprSTO("int", new IntType());

	    this.incOffset();

	    aw.writeBitOp(left, oper, right);
	    sto.setLocalOffset(m_localsOffset);

	    return sto;
	}
	
	STO
	DoBinOp(STO left, String oper, STO right)
	{
		/*
	//System.out.println("right: " + (right.isConst() ? "isConst: " + ((ConstSTO)right).getValue() : "not const" + right));
	//System.out.println("left: " + (left.isConst() ? "isConst: " + ((ConstSTO)left).getValue() : "not const" + left));
		*/

		//if (left.isConst() || right.isConst())
		//aw.writeArithmetic(left, oper, right);
		
		double arith_result = -9696969;
		
		if (oper == "%")
			return DoModOp(left, oper, right);
		if (oper == "<" || oper == ">" || oper == "<=" || oper == ">=")
			return DoRelationOp(left, oper, right);
		if (oper == "==" || oper == "!=")
			return DoEqualityOp(left, oper, right);
		if (oper == "&&" || oper == "||")
			return DoLogicalOp(left, oper, right);
		if (oper == "!")
			return DoNotOp(oper, right);

		ExprSTO sto = null;

		//System.out.println(left.getType()+ " bitch " + right.getType());

		if (left != null && right != null && !left.isError() && !right.isError() && left.getType() != null && right.getType() != null) {
			if (left.getType().isNumeric()) {
				if (right.getType().isNumeric()) {

					if (right.isConst() && left.isConst()) {
						ConstSTO leftconst = (ConstSTO)left;
						ConstSTO rightconst = (ConstSTO)right;

						//System.out.println("both are ConstSTO, tl=" + left.getType() + " tr=" + right.getType());

						double leftval = leftconst.getValue();
						double rightval = rightconst.getValue();


						if (oper == "*") {
							arith_result = leftval * rightval;
						} else if (oper == "/") {
							arith_result = leftval / rightval;
						} else if (oper == "+") {
							arith_result = leftval + rightval;
						} else if (oper == "-") {
							arith_result = leftval - rightval;
						} else {
							//System.out.println("not a recognized operator!!!");
						}

						//System.out.println("constant folding result = " + arith_result);

						if (arith_result == Double.NaN 
								|| arith_result == Double.NEGATIVE_INFINITY 
								|| arith_result == Double.POSITIVE_INFINITY)
						{
							m_nNumErrors++;
							m_errors.print(ErrorMsg.error8_Arithmetic);
							return (new ErrorSTO ("ERROR in DoBinOp BITCH"));
						}

						if (left.getType().isFloat() || right.getType().isFloat()) {
							return new ConstSTO(arith_result+"", new FloatType(), arith_result);
						} else {
							return new ConstSTO(arith_result+"", new IntType(), arith_result);
						}
					}

					//System.out.println(left.getType()+ " bitch " + right.getType());

					if (left.getType().isFloat() || right.getType().isFloat()) {
						sto = new ExprSTO("float", new FloatType());
					} else {
						sto = new ExprSTO("int", new IntType());
					}
					
					this.incOffset();

					if (oper.equals("+") || oper.equals("-"))
					{
					    aw.writeAddSubOp(left, oper, right);
					}
					else if (oper.equals("*") || oper.equals("/"))
					{
					    aw.writeMulDivOp(left, oper, right);
					}

					sto.setLocalOffset(m_localsOffset);

					return sto;
				}
				if (!(right.getType().isFuncPtr()) && !(left.getType().isFuncPtr())){
				m_nNumErrors++;
		 		m_errors.print (Formatter.toString(ErrorMsg.error1n_Expr, right.getType(), oper));
				return (new ErrorSTO ("ERROR in DoAddOp BITCH"));}
				if ((left.getType().isFuncPtr() && ((FuncPtrType)left.getType()).isFloat()) || (right.getType().isFuncPtr() && ((FuncPtrType)right.getType()).isFloat()) || right.getType().isFloat() || left.getType().isFloat()) {
					this.incOffset();
				    sto = new ExprSTO("wtfnotaglobal1", new FloatType());
				    if (oper.equals("+") || oper.equals("-"))
					{
					    aw.writeAddSubOp(left, oper, right);
					}
					else if (oper.equals("*") || oper.equals("/"))
					{
					    aw.writeMulDivOp(left, oper, right);
					}
					sto.setLocalOffset(m_localsOffset);
				} else {
				    this.incOffset();
				    sto = new ExprSTO("wtfnotaglobal2", new IntType());
				    if (oper.equals("+") || oper.equals("-"))
					{
					    aw.writeAddSubOp(left, oper, right);
					}
					else if (oper.equals("*") || oper.equals("/"))
					{
					    aw.writeMulDivOp(left, oper, right);
					}
					sto.setLocalOffset(m_localsOffset);
				}
				return sto;
			}
			if (!(right.getType().isFuncPtr()) && !(left.getType().isFuncPtr())){
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error1n_Expr, left.getType(), oper));
			return (new ErrorSTO ("ERROR in DoBinOp BITCH"));}
    			if ((left.getType().isFuncPtr() && ((FuncPtrType)left.getType()).isFloat()) || (right.getType().isFuncPtr() && ((FuncPtrType)right.getType()).isFloat()) || left.getType().isFloat() || right.getType().isFloat()) {
    			this.incOffset();
			    sto = new ExprSTO("wtfnotaglobal3", new FloatType());
			    if (oper.equals("+") || oper.equals("-"))
				{
				    aw.writeAddSubOp(left, oper, right);
				}
				else if (oper.equals("*") || oper.equals("/"))
				{
				    aw.writeMulDivOp(left, oper, right);
				}
				sto.setLocalOffset(m_localsOffset);
    			} else {
    			this.incOffset();
			    sto = new ExprSTO("wtfnotaglobal4", new IntType());
			    if (oper.equals("+") || oper.equals("-"))
				{
				    aw.writeAddSubOp(left, oper, right);
				}
				else if (oper.equals("*") || oper.equals("/"))
				{
				    aw.writeMulDivOp(left, oper, right);
				}
				sto.setLocalOffset(m_localsOffset);
    			}
    			return sto;
		}
		/*
		m_nNumErrors++;
		if (left.getType() == null) {
			m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, left.getName()));
		}
		else 
		{
			m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, right.getName()));
		}
		return (new ErrorSTO ("ERROR in DoBinOp BITCH"));
		*/
		return sto;
	}

	STO
	DoModOp(STO left, String oper, STO right)
	{
		ExprSTO sto = null;
		//System.out.println(left.getType()+ " bitch " + right.getType());

		if (left != null && right != null) {
			if (left.getType().isInt()) {
				if (right.getType().isInt()) {

					if (right.isConst() && left.isConst()) {
						double leftval = ((ConstSTO)left).getValue();
						double rightval = ((ConstSTO)right).getValue();

						double arith_result = leftval % rightval;

						//System.out.println("both are consts, result = " + arith_result);

						if (rightval == 0) {
							m_nNumErrors++;
							m_errors.print(ErrorMsg.error8_Arithmetic);
							return (new ErrorSTO ("ERROR in DoBinOp BITCH"));
						}
						
						ConstSTO sto1 = new ConstSTO(arith_result+"", new IntType(), arith_result);
						
						this.incOffset();

						aw.writeModOp(left, oper, right);
						sto1.setLocalOffset(m_localsOffset);
						
						return sto1;
					}

					sto = new ExprSTO("int", new IntType());
					
					this.incOffset();

					aw.writeModOp(left, oper, right);
					sto.setLocalOffset(m_localsOffset);
					
					return sto;
				}
				m_nNumErrors++;
		 		m_errors.print (Formatter.toString(ErrorMsg.error1w_Expr, right.getType(), oper, left.getType()));
				return (new ErrorSTO ("ERROR in DoModOp BITCH"));
			}
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error1w_Expr, left.getType(), oper, right.getType()));
			return (new ErrorSTO ("ERROR in DoModOp BITCH"));
		}
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, "cunt"));
		return (new ErrorSTO ("ERROR in DoAddOp BITCH"));
	}

	STO
	DoRelationOp(STO left, String oper, STO right)
	{
		ExprSTO sto = null;
		//System.out.println(left.getType()+ " bitch " + right.getType());
		if (left != null && right != null) {
			if (left.getType().isNumeric()) {
				if (right.getType().isNumeric()) {
					sto = new ExprSTO("bool", new BoolType());
					
					this.incOffset();

					aw.writeRelationOp(left, oper, right);
					sto.setLocalOffset(m_localsOffset);
					
					return sto;
				}
				if (!(right.getType().isFuncPtr()) && !(left.getType().isFuncPtr())){
				m_nNumErrors++;
		 		m_errors.print (Formatter.toString(ErrorMsg.error1n_Expr, right.getType(), oper));
				return (new ErrorSTO ("ERROR in DoModOp BITCH"));}
			}
			if (!(right.getType().isFuncPtr()) && !(left.getType().isFuncPtr())){
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error1n_Expr, left.getType(), oper));
			return (new ErrorSTO ("ERROR in DoModOp BITCH"));}
		}
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, "cunt"));
		return (new ErrorSTO ("ERROR in DoAddOp BITCH"));
	}

	STO
	DoEqualityOp(STO left, String oper, STO right)
	{
		STO sto = null;
		
		//System.out.println(left.getType()+ " bitch " + right.getType());
		
		if(left != null && right != null){
			if (left.getType() == null && right.getType() == null)
			{
				sto = new ConstSTO("bool", new BoolType());

				this.incOffset();

				aw.writeRelationOp(left, oper, right);
				sto.setLocalOffset(m_localsOffset);

				return sto;
			}
			
			else if ((left.getType() == null && right.getType() != null && right.getType().isPtrGrp()) || 
					((right.getType() == null && left.getType() != null && left.getType().isPtrGrp())))
			{
				sto = new ConstSTO("bool", new BoolType());
				
				this.incOffset();

				aw.writeRelationOp(left, oper, right);
				sto.setLocalOffset(m_localsOffset);
				
				return sto;
			}
			
			else if (left.getType().isPtrGrp() && right.getType().isPtrGrp())
			{
				sto = new ConstSTO("bool", new BoolType());
				
				this.incOffset();

				aw.writeRelationOp(left, oper, right);
				sto.setLocalOffset(m_localsOffset);
				
				return sto;
			}
			
			else if (left.getType() == null || left.getType().isPtrGrp() || right.getType() == null || right.getType().isPtrGrp())
			{
				//System.out.println("left is a " + left.getType() + " and right is a " + right.getType() + " and that's that!");
				m_nNumErrors++;
				m_errors.print (Formatter.toString(ErrorMsg.error17_Expr, oper, left.getType(), right.getType()));
				return (new ErrorSTO ("ERROR in DoEqOp BITCH"));
			}
		}
		
		if (left != null && right != null) {
			if (left.getType().isNumeric()) {
				if (right.getType().isNumeric()) {
					sto = new ExprSTO("bool", new BoolType());
					
					this.incOffset();

					aw.writeRelationOp(left, oper, right);
					sto.setLocalOffset(m_localsOffset);
					
					return sto;
				}
				m_nNumErrors++;
		 		m_errors.print (Formatter.toString(ErrorMsg.error1b_Expr, left.getType(), oper, right.getType()));
				return (new ErrorSTO ("ERROR in DoModOp BITCH"));
			} else if (left.getType().isBool()) {
				if (right.getType().isBool()) {
					sto = new ExprSTO("bool", new BoolType());
					
					this.incOffset();

					aw.writeRelationOp(left, oper, right);
					sto.setLocalOffset(m_localsOffset);
					
					return sto;
				}
				m_nNumErrors++;
		 		m_errors.print (Formatter.toString(ErrorMsg.error1b_Expr, left.getType(), oper, right.getType()));
				return (new ErrorSTO ("ERROR in DoModOp BITCH"));
			}
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error1b_Expr, left.getType(), oper, right.getType()));
			return (new ErrorSTO ("ERROR in DoModOp BITCH"));
		}
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, "cunt"));
		return (new ErrorSTO ("ERROR in DoAddOp BITCH"));
	}


	STO
	DoLogicalOp(STO left, String oper, STO right)
	{
		ExprSTO sto = null;

		//System.out.println("left=" + left + " right=" + right);

		if (left != null && right != null) {
			if (left.getType().isBool() || ((FuncSTO)left).getReturnType().isBool()) {
				if (right.getType().isBool() || ((FuncSTO)right).getReturnType().isBool()) {
					sto = new ExprSTO("int", new BoolType());
					
					this.incOffset();

					aw.writeLogicalOp(left, oper, right);
					sto.setLocalOffset(m_localsOffset);

					return sto;
				}
				m_nNumErrors++;
		 		m_errors.print (Formatter.toString(ErrorMsg.error1w_Expr, right.getType(), oper, left.getType()));
				return (new ErrorSTO ("ERROR in DoModOp BITCH"));
			}
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error1w_Expr, left.getType(), oper, right.getType()));
			return (new ErrorSTO ("ERROR in DoModOp BITCH"));
		}
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, "cunt"));
		return (new ErrorSTO ("ERROR in DoAddOp BITCH"));
	}

	STO
	DoNotOp(String oper, STO right)
	{
		ExprSTO sto = null;

		//System.out.println("DoNotOp: right=" + right);

		if (right != null && right != null) {
			if (right.getType().isBool() || ((FuncSTO)right).getReturnType().isBool()) {
				sto = new ExprSTO("bool", new BoolType());

				this.incOffset();

				aw.writeLogicalOp(oper, right);
				sto.setLocalOffset(m_localsOffset);

				return sto;
			}
			m_nNumErrors++;
		 	m_errors.print (Formatter.toString(ErrorMsg.error1u_Expr, right.getType(), oper, "bool"));
			return (new ErrorSTO ("ERROR in DoModOp BITCH"));
		}

		System.out.println("nullness problem in DoNotOp");

		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.undeclared_id, "cunt"));
		return (new ErrorSTO ("ERROR in DoAddOp BITCH"));
	}
	
	STO
	DoPreIncDecOp(String oper, STO modLval)
	{
		VarSTO sto = new VarSTO(modLval.getName(), modLval.getType());
		
		this.incOffset();
		
		aw.writePreIncDecOp(oper, modLval);
		sto.setLocalOffset(m_localsOffset);
		
		return sto;
	}
	
	STO
	DoPostIncDecOp(String oper, STO modLval)
	{
		ExprSTO sto = new ExprSTO(modLval.getName(), modLval.getType());

		this.incOffset();

		aw.writePostIncDecOp(oper, modLval);
		sto.setLocalOffset(m_localsOffset);
		
		return sto;
		
		//System.out.println("fack " + modLval.getType());
/*
		if (modLval.isModLValue()) {
			if (modLval.getType().isNumeric() || modLval.getType().isPointer()) {
				return new VarSTO(modLval.getType().toString(), modLval.getType());
			}
		}


		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.error2_Lval, oper));
		return (new ErrorSTO ("ERROR in DoIncDecOp BITCH"));
		*/
	}
	
	STO
	DoArrayDef(STO arrayExpr)
	{	
		ConstSTO sto;
		
		//System.out.println("cock length = " + arrayExpr + " anus width = " + arrayExpr.getType());
		//System.out.println("IS IT IN YET?");

		if (arrayExpr.getType() == null || !arrayExpr.getType().isInt()) {

			//System.out.println("ITS IN!");

			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error10i_Array, arrayExpr.getType()));

			return (new ErrorSTO ("ERROR in DoArrayDef ASSHOLE"));
		}

		if (!arrayExpr.isConst() || ((ConstSTO)arrayExpr).getValue() == null) {
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error10c_Array);

			return (new ErrorSTO ("ERROR in DoArrayDef ASSHOLE"));
		}

		if (!(((ConstSTO)arrayExpr).getValue() > 0)) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error10z_Array, ((ConstSTO)arrayExpr).getIntValue()));

			return (new ErrorSTO ("ERROR in DoArrayDef ASSHOLE"));
		}

		sto = new ConstSTO (arrayExpr.getName(), new ArrayType(), ((ConstSTO)arrayExpr).getValue());
		sto.getType().setDimensionSize(((ConstSTO)arrayExpr).getIntValue());

		//this.incOffset();

		//sto.setLocalOffset(this.getOffset());
		//System.out.println("sto array: " + sto.getType());

		return sto;
	}

	void
	DoBCStmt(int t)
	{
	    if (t == 1)
	    {
		aw.writeBreak();	    
	    }
	    else if (t == 2)
	    {
		aw.writeContinue();	
	    }
	    
	    //System.out.println(m_whileCount);
		if (m_whileCount <= 0)
		{
			if (t == 1)
			{
				//System.out.println("KNOCK IT OFF");
				m_nNumErrors++;
				m_errors.print (ErrorMsg.error12_Break);
				//return (new ErrorSTO ("ERROR in DoBCStmt ASSHOLE"));
			}
			else if (t == 2)
			{
				//System.out.println("KNOCK IT OfdsafsaFF");
				m_nNumErrors++;
				m_errors.print (ErrorMsg.error12_Continue);
				//return (new ErrorSTO ("ERROR in DoBCStmt ASSHOLE"));
			}
		}
	}
	
	void
	into(String s)
	{
		//System.out.println("shitstain @ " + s);
		
		if (s == "if")
		{
			m_ifCount++;
		}
		if (s == "while")
		{
			//System.out.println(m_whileCount);
			m_whileCount++;
			//System.out.println("post " + m_whileCount);
		}
		if (s == "else")
		{
			m_elseCount++;
		}
		if (s == "typedef")
		{
			m_typedefCount++;
		}
	}
	
	void
	outof(String s)
	{
		//System.out.println("tahla @ " + s);
		if (s == "if")
		{
			m_ifCount--;
		}
		if (s == "while")
		{
			//System.out.println("bang sup song duu");
			m_whileCount--;
		}
		if (s == "else")
		{
			m_elseCount--;
		}
		if (s == "typedef")
		{
			m_typedefCount--;
		}
	}
	
	void
	DoDeleteStmt(STO sto)
	{
		//System.out.println(sto.getName() + " babycakes");
		
	    	aw.writeDelete(sto);
	    
		if (!sto.isModLValue())
		{
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error16_Delete_var);
		}
	
		if (sto.getType() != null && !sto.getType().isPointer())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error16_Delete, sto.getType()));
		}
	}
	
	void
	DoNewStmt(STO sto)
	{
		//System.out.println(sto.getName() + " babycakes");
		
	    	aw.writeNew(sto);
	    
		if (!sto.isModLValue())
		{
			m_nNumErrors++;
			m_errors.print (ErrorMsg.error16_New_var);
		}
		
		if (sto.getType() != null && !sto.getType().isPointer())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error16_New, sto.getType()));
		}
	}
	
	STO
	DoSizeOfVar(STO sto)
	{
		ConstSTO sto2 = null;
		
		if (sto != null && sto.getType() != null && sto.getIsAddressable())
		{
			
			if (sto.getType().isArray())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)9999);
			}
			if (sto.getType().isBool())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)4);
			}
			if (sto.getType().isInt())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)4);
			}
			if (sto.getType().isVoid())
			{
				//t = new VoidType();
			}
			if (sto.getType().isFloat())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)4);
			}
			if (sto.getType().isFuncPtr())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)4);
			}
			if (sto.getType().isPointer())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)4);
			}
			if (sto.getType().isStruct())
			{
				sto2 = new ConstSTO(sto.getName(), new IntType(), (double)8888);
			}
			
			//sto = new ConstSTO(sto.getName(), new IntType(), (double)4);
			//System.out.println("SizeOf(" + sto2.getName() + ") returns a ConstSTO of type " + sto2.getType() + 
					//" and of value " + ((ConstSTO)sto2).getValue() + " (has orig type " + sto.getType() + ")");
			return sto2;
		}
		m_nNumErrors++;
		m_errors.print (ErrorMsg.error19_Sizeof);
		return (new ErrorSTO ("ERROR in DoSizeOfVar FUCKIHAVEPIGFLU"));
	}
	
	STO
	DoSizeOfType(Type t)
	{
		ConstSTO t2 = null;
		
		if (t != null)
		{
			
			if (t.isArray())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)9999);
			}
			if (t.isBool())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)4);
			}
			if (t.isInt())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)4);
			}
			if (t.isVoid())
			{
				//t = new VoidType();
			}
			if (t.isFloat())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)4);
			}
			if (t.isFuncPtr())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)4);
			}
			if (t.isPointer())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)4);
			}
			if (t.isStruct())
			{
				t2 = new ConstSTO(t.getName(), new IntType(), (double)8888);
			}
			
			//sto = new ConstSTO(sto.getName(), new IntType(), (double)4);
			//System.out.println("SizeOf(" + t2.getName() + ") returns a ConstSTO of type " + t2.getType() + 
				//	" and of value " + ((ConstSTO)t2).getValue() + " (has orig type " + t + ")");
			return t2;
		}
		m_nNumErrors++;
		m_errors.print (ErrorMsg.error19_Sizeof);
		return (new ErrorSTO ("ERROR in DoSizeOfType STUPIDPIGFLU"));
	}
	
	STO
	DoAddrOf(STO sto)
	{
		ExprSTO slave = null;
		
		if (!sto.getIsAddressable())
		{
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error21_AddressOf, sto.getType()));
			return (new ErrorSTO ("ERROR in DoAddrOf STUPIDPIGFLU"));
		}
		
		slave = new ExprSTO("addressof:" + sto.getName(), sto.getType());
		
		slave.setIsModifiable(false);
		slave.setIsAddressable(false);
		
		//System.out.println("dereffing " + sto.getName() + " of type " + sto.getType() + " with array loc " + sto.getArrayLoc());
		
		if (sto.getName().matches(".*_base_offsetby_.*"))
		{
		    slave.setArrayLoc(sto.getArrayLoc());
		}
		
		if (sto == null || sto.getName().matches(".*_base_offsetby_.*") && sto.getArrayLoc() == null)
		{
		    //System.out.println("INSERTING NULL STO INTO SYMBOL TABLE IN DOADDROF");
		}
		m_symtab.insert(sto);
		
		this.incOffset();
		aw.writeAddressOf(sto);
		slave.setLocalOffset(m_localsOffset);
		
		return slave;
	}

	STO
	DoDereference(STO sto)
	{
	//System.out.println("DoDerefernce(" + sto + ") called.");

		//System.out.println("fix this so that ***ptr and shit works");

		if (sto.getType() != null && !sto.getType().isPointer()) {
			m_nNumErrors++;
			m_errors.print (Formatter.toString(ErrorMsg.error15_Receiver, sto.getType()));
			return new ErrorSTO ("ERROR in DoDerefernceOf worstclassever");
		}

		STO sto2 = null;

		//System.out.println("dereferencing: " + sto.getName());
		
		//System.out.println("pointer level is: " + ((PointerType)sto.getType()).getPointerLevel());
		
		if (((PointerType)sto.getType()).getPointerLevel() > 1 && sto != null && sto.getType() != null && sto.getType().isPointer()) {

			Type pointedtype = ((PointerType)sto.getType()).getPointedType();
			
			Type newpointer = new PointerType(pointedtype);
			int newlevel = ((PointerType)sto.getType()).getPointerLevel();
			newlevel--;
			((PointerType)newpointer).setPointerLevel(newlevel);
			
			sto2 = new VarSTO(sto.getName(), newpointer);
			
			return sto2;
		}

		
		if (sto != null && sto.getType() != null && sto.getType().isPointer()) {

			Type pointedtype = ((PointerType)sto.getType()).getPointedType();
			
			if (pointedtype.isArray()) {
				sto2 = new VarSTO(sto.getName(), new ArrayType());
			}

			if (pointedtype.isBool()) {
				sto2 = new VarSTO(sto.getName(), new BoolType());
			}

			if (pointedtype.isInt()) {
				sto2 = new VarSTO(sto.getName(), new IntType());
			}

			if (pointedtype.isVoid()) {
				//t = new VoidType();
			}

			if (pointedtype.isFloat()) {
				sto2 = new VarSTO(sto.getName(), new FloatType());
			}

			if (pointedtype.isFuncPtr()) {
				sto2 = new FuncSTO(sto.getName(), new FuncPtrType());
			}

			if (pointedtype.isPointer()) {
				sto2 = new VarSTO(sto.getName(), new PointerType());
			}

			if (pointedtype.isStruct()) {
			//System.out.println ("happened ............. " + sto.getName());
				sto2 = new TypedefSTO(sto.getName(), m_symtab.access(sto.getName() + "*").getType());
			//System.out.println("fields derefed: " + ((StructType)sto2.getType()).getFields());
			}
			
			//sto = new ConstSTO(sto.getName(), new IntType(), (double)4);

			//System.out.println("SizeOf(" + sto2.getName() + ") returns a ConstSTO of type " + sto2.getType() + 
					//" and of value " + ((ConstSTO)sto2).getValue() + " (has orig type " + sto.getType() + ")");
		}

		this.incOffset();
		sto2.setLocalOffset(this.getOffset());
		aw.writeDereference(sto);
		
		return sto2;
	}

	STO
	DoTypeCast(Type t, STO sto)
	{
		STO sto2 = null;
		
		if(sto != null && sto.getType() != null && (sto.getType().isBasic() || sto.getType().isPointer()))	//check alias type too...
		{
			if (sto.getType().isFloat())
			{
				if (t.isBool())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new BoolType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new BoolType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isInt())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new IntType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new IntType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isPointer())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new PointerType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new PointerType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
			}
			
			if (sto.getType().isInt())
			{
				if (t.isBool())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new BoolType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new BoolType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isFloat())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new FloatType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new FloatType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isPointer())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new PointerType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new PointerType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
			}
			
			if (sto.getType().isBool())
			{
				if (t.isInt())
				{
					if (sto.isConst())
					{
						if(((ConstSTO)sto).getBoolValue())
						{
							sto2 = new ConstSTO (sto.getName(), new IntType(), (double)1);
							this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
						}
						else
						{
							sto2 = new ConstSTO (sto.getName(), new IntType(), (double)0);
							this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
						}
					}
					
					sto2 = new VarSTO(sto.getName(), new IntType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isFloat())
				{
					if (sto.isConst())
					{
						if(((ConstSTO)sto).getBoolValue())
						{
							sto2 = new ConstSTO (sto.getName(), new FloatType(), (double)1);
							this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
						}
						else
						{
							sto2 = new ConstSTO (sto.getName(), new FloatType(), (double)0);
							this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
						}
					}
					
					sto2 = new VarSTO(sto.getName(), new FloatType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isPointer())
				{
					if (sto.isConst())
					{
						if(((ConstSTO)sto).getBoolValue())
						{
							sto2 = new ConstSTO (sto.getName(), new PointerType(), (double)1);
							this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
						}
						else
						{
							sto2 = new ConstSTO (sto.getName(), new PointerType(), (double)0);
							this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
						}
					}
					
					sto2 = new VarSTO(sto.getName(), new PointerType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
			}
			
			if (sto.getType().isPointer())
			{
				if (t.isBool())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new BoolType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new BoolType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isInt())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new IntType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new IntType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
				if (t.isFloat())
				{
					if (sto.isConst())
					{
						sto2 = new ConstSTO (sto.getName(), new FloatType(), ((ConstSTO)sto).getValue());
						this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
					}
					
					sto2 = new VarSTO(sto.getName(), new FloatType());
					this.incOffset(); aw.writeTypecast(sto, t); sto2.setLocalOffset(this.getOffset()); return sto2;
				}
			}
		}
		
		m_nNumErrors++;
		m_errors.print (Formatter.toString(ErrorMsg.error20_Cast, sto.getType(), t));
		return (new ErrorSTO (sto.getName ()));
	}

	public void
	DoReadStmt(STO designator)
	{
	    aw.writeReadStmt(designator);
	}
	
	FuncPtrType
	DoFuncPtr(Type returntype, Vector<STO> paramlist)
	{
	   //System.out.println("DoFuncPtr(" + returntype + ", " + paramlist + ")");

	    FuncPtrType funcptrtype = new FuncPtrType(returntype, paramlist);

	   //System.out.println("DoFuncPtr: KTHXBYE.");

	    return funcptrtype;
	}



	void
	DoWriteStmt(Vector<STO> writepairs)
	{
		for (int i = 0; i < writepairs.size(); ++i) {
			boolean isvar = false;
			boolean isconst = false;
			STO sto = (STO)writepairs.elementAt(i);
			
			//System.out.println("DoWriteStmt: sto=" + sto);
			//System.out.println("DoWriteStmt: sto.getName()=" + sto.getName());
			//System.out.println("DoWriteStmt: sto.getType()=" + sto.getType());
			
			Type t = sto.getType();

			if (sto == null)
			{
			    //System.out.println("NULL STO ENCOUNTERED IN DOWRITESTMT");
			}
			
			//System.out.println("DoWriteStmt: sto=" + sto);
			//System.out.println("DoWriteStmt: sto.getName()=" + sto.getName());
			//System.out.println("DoWriteStmt: t=" + t);

			if (sto.isVar() || sto.isExpr()) {
				isvar = true;
				isconst = false;
			} else if (sto.isConst()) {
				isvar = false;
				isconst = true;
			}

			if (t == null) {
				// strings
				aw.writePrintf(sto.getName());
			} else if (t.isBool()) {
				if (isconst) {
					boolean bval = ((ConstSTO)sto).getBoolValue();
					aw.writePrintf(bval, sto.getLocalOffset());
				} else if (isvar) {
				    	aw.writeBoolPrintf(sto);
				    //aw.writePrintf((BoolType)t, sto.getName(), sto.getLocalOffset());
				}
			}
			else if (t.isFuncPtr())
			{
			    aw.writePrintReturn(sto);
			}
			else
			{
			    aw.writePrintf(sto);
			}
		}
	}

	public static void
	incOffset()
	{
	    //System.out.println("incrementing m_localsOffset");

	    m_localsOffset += 4;
	}
	

	public static void
	incOffset(int i)
	{
	    //System.out.println("incrementing m_localsOffset");

	    m_localsOffset += 4*i;
	}

	private static void
	resetLocalsOffset()
	{
	    //System.out.println("resetting m_localsOffset");

	    m_localsOffset = 0;
	}

	public static int
	getOffset()
	{
	    return m_localsOffset;
	}
	
	public static boolean
	getInFunc()
	{
	    boolean bool;
	    if (m_symtab.getFunc() != null)
	    {
		bool = true; 
	    }
	    else
	    {
		bool = false;
	    }
	    return bool;
	}

//----------------------------------------------------------------
//	Instance variables
//----------------------------------------------------------------

	private Lexer			m_lexer;
	private ErrorPrinter		m_errors;
	public static int		m_nNumErrors;
	private String			m_strLastLexeme;
	private boolean			m_bSyntaxError = true;
	private int			m_nSavedLineNum;

	private boolean firstGlobal = true;

	private static SymbolTable		m_symtab;
	private int m_ifCount = 0;
	private int m_whileCount = 0;
	private int m_elseCount = 0;
	private int m_typedefCount = 0;
	private Vector<Type> m_icStructIds;

	public static AssemblyCodeGenerator aw;
	private String OUTPUT_ASSEMBLY_FILE = "rc.s";
	
	private static int m_localsOffset = 0;
}












