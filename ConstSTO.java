//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class ConstSTO extends STO
{
	public
	ConstSTO(String strName)
	{
		this(strName, null, null);
	}

	public
	ConstSTO(String strName, Type t)
	{
		this(strName, t, null);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	ConstSTO (String strName, Double val)
	{
		this(strName, null, val);

		//System.out.println("ConstSTO(" + strName + ", " + val + ")");

		/*
		super (strName);
		this.setValue(val);
		this.setIsAddressable(true);
		*/
	}

	public 
	ConstSTO (String strName, Type typ, Double val)
	{
		super (strName, typ);

		//System.out.println("ConstSTO(" + strName + ", " + typ + ", " + val + ")");

		if (val != null) {
			this.setValue(val);
		}

		this.setIsAddressable(true);
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	isConst () 
	{
		return true;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	private void
	setValue (double val) 
	{
		m_value = new Double(val);
	}

	public Double
	getValue () 
	{
		return m_value;
	}

	public int
	getIntValue () 
	{
		//System.out.println("returning " + this.getName() + ", " + m_value.intValue() + ", " + m_value);

		return m_value.intValue();
	}

	public float
	getFloatValue () 
	{
		return m_value.floatValue();
	}

	public boolean
	getBoolValue () 
	{
		//System.out.println("NOHHHHHHOHOHO " + m_value);

		// FIXME: this is a problem!
		if (m_value == null) return false;

		return m_value.intValue() != 0;
	}


//----------------------------------------------------------------
//	Constants have a value, so you should store them here.
//	Note: We suggest using Java's Double class, which can hold
//	floats and ints. You can then do .floatValue() or 
//	.intValue() to get the corresponding value based on the
//	type. Booleans/Ptrs can easily be handled by ints.
//	Feel free to change this if you don't like it!
//----------------------------------------------------------------
        private Double		m_value = 0.0;
}
