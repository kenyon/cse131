//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java.util.Vector;


class Scope
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	Scope ()
	{
		m_lstLocals = new Vector ();
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO
	access (String strName)
	{
	    //System.out.println("accessing: " + strName);
		return	accessLocal (strName);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO
	accessLocal (String strName)
	{
		STO		sto = null;
		 //System.out.println("accessing locally: " + strName);
		for (int i = 0; i < m_lstLocals.size (); i++)
		{
			sto = (STO) m_lstLocals.elementAt (i);

			if (sto.getName().equals (strName))
			{
			    //System.out.println("returning " + sto.getName() + ":" + sto + " for " + strName);
				return (sto);
			}
		}

		return (null);
	}

	/**
	 * Marks a struct TypedefSTO as complete.
	 */
	public void markStructComplete(String id)
	{
		for (int i = 0; i < m_lstLocals.size(); i++)
		{
			//sto = (TypedefSTO)m_lstLocals.elementAt(i);

			if (((STO)m_lstLocals.elementAt(i)).getName().equals(id))
				((TypedefSTO)m_lstLocals.elementAt(i)).setIsCompleteDef(true);
		}
	}

	public void insertStructFields(String id, Vector<STO> fields)
	{
	    //System.out.println("Scope: insertStructFields: fields=" + fields);

		for (int i = 0; i < m_lstLocals.size(); i++)
		{
			if (((STO)m_lstLocals.elementAt(i)).getName().equals(id)) {
				STO theoneinquestion = (STO)m_lstLocals.elementAt(i);

				StructType theonetype = (StructType)theoneinquestion.getType();

				theonetype.setFields(fields);
			}
		}
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	InsertLocal (STO sto)
	{
	    //System.out.println("inserting locally: " + sto.getName());
		m_lstLocals.addElement (sto);
	}


//----------------------------------------------------------------
//	Instance variables.
//----------------------------------------------------------------
	private Vector		m_lstLocals;

/*
	public void setPointedFunc(String id, STO func) {
	    for (int i = 0; i < m_lstLocals.size(); i++)
		{
			if (((STO)m_lstLocals.elementAt(i)).getName().equals(id)) {
				STO theoneinquestion = (STO)m_lstLocals.elementAt(i);

				theoneinquestion.setPointedFunc(func);
			}
		}
	    
	}*/
}
