class PointerType extends PtrGrpType
{
	public PointerType()
	{
		super("pointer");
	}

	public PointerType(Type pointedToType)
	{
		super("pointer");
		this.setPointedType(pointedToType);
	}

	public boolean	isPointer ()	{ return true; }

	public String toString () 	{ return "pointer";}

	private void setPointedType(Type t)
	{
		this.m_pointedType = t;
	}

	public Type getPointedType()
	{
		return m_pointedType;
	}

	private Type m_pointedType;

	// FIXME: this is probably wrong?
	public boolean isAssignable(Type t)
	{
		return true;//this.getPointedType().isAssignable(t);
	}

	// FIXME: add isAddressable

	public void setPointerLevel(int i)
	{
		this.pointerLevel = i;
	}

	public int getPointerLevel()
	{
		return pointerLevel;
	}
	
	public void incrementPointerLevel()
	{
		pointerLevel++;
	}
	
	public void decrementPointerLevel()
	{
		pointerLevel--;
	}
	
	private int pointerLevel; 
}
