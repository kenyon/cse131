#include <iostream>

using namespace std;

int x = 2;
int y = 4;
int z = 6;
int w = 8;

float fx = 22.21;
float fy = 44.32;
float fz = 66.43;
float fw = 88.54;

int main()
{
	int lx = 10;
	int ly = 12;
	int lz = 14;
	int lw = 16;

	float flx = 110.12;
	float fly = 112.34;
	float flz = 114.56;
	float flw = 116.78;

	// global ints
	cout << "1: " << x++ << endl;
	cout << "2: " << x << endl;

	cout << "3: " << y-- << endl;
	cout << "4: " << y << endl;

	cout << "5: " << ++z << endl;
	cout << "6: " << z << endl;

	cout << "7: " << --w << endl;
	cout << "8: " << w << endl;

	// global floats
	cout.precision(2);
	cout << fixed << "1f: " << fx++ << endl;
	cout << "2f: " << fx << endl;

	cout << "3f: " << fy-- << endl;
	cout << "4f: " << fy << endl;

	cout << "5f: " << ++fz << endl;
	cout << "6f: " << fz << endl;

	cout << "7f: " << --fw << endl;
	cout << "8f: " << fw << endl;

	// local ints
	cout << "9: " << lx++ << endl;
	cout << "10: " << lx << endl;

	cout << "11: " << ly-- << endl;
	cout << "12: " << ly << endl;

	cout << "13: " << ++lz << endl;
	cout << "14: " << lz << endl;

	cout << "15: " << --lw << endl;
	cout << "16: " << lw << endl;

	// local floats
	cout << "9f: " << flx++ << endl;
	cout << "10f: " << flx << endl;

	cout << "11f: " << fly-- << endl;
	cout << "12f: " << fly << endl;

	cout << "13f: " << ++flz << endl;
	cout << "14f: " << flz << endl;

	cout << "15f: " << --flw << endl;
	cout << "16f: " << flw << endl;

	return 0;
}
