#include <iostream>
using namespace std;

int x = 0;
int y = 0;

void foo()
{
	if (10 > x)
	{
		x++;

		cout << x << endl;

		foo();
	}
}

int foo1()
{
	if (20 > y) {
		y++;

		cout << y << endl;

		foo1();
	}

	if (y > 19) {
		return y;
	}
}

int main()
{
	foo();

	x = foo1();

	cout << x << endl;

	return 0;
}
