#include <iostream>

using namespace std;

/* function fib is:
 * input: integer n such that n >= 0
 * 
 *     1. if n is 0, return 0
 *     2. if n is 1, return 1
 *     3. otherwise, return [ fib(n-1) + fib(n-2) ]
 * 
 * end fib
 */

int fib(int n)
{
    if (n == 0) {
	return 0;
    }

    if (n == 1) {
	return 1;
    }

    return fib(n - 1) + fib(n - 2);
}

int main(void)
{
    cout << fib(15) << endl;

    return 0;
}
