#include <iostream>
using namespace std;

float a;
float b = ++a;
int f ()
{
   cout << "<f1>";
   if (a > b) { return 0; }
   cout << "<f2>";
   return 0;
}

float g ()
{
   cout << "<g1>";
   if (a > b) { return a; }
   cout << "<g2>";
   return b;
}

bool h ()
{
   cout << "<h1>";
   if (a > b--) { return a > b; }
   cout << "<h2>";
   return false;
}

int main() {
 cout << "begin";
 if (!h()) {
   if (g() + g() + g() > g() + g()) {
     cout << "ok";
   }
   cout << "ok";
   cout << "end\n";
 }
 return 0;
}
