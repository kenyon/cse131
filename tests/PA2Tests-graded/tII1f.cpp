#include <iostream>
using namespace std;

float r,s;
int i,j;
int fi() { return -i; }
float fr() { return (r + (r + (r + (r + (r + (r + (r))))))); }

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    r = 2.5; s = 1.75; i = 2; j = 3;
    cout << r + i + j << " " << i + j * r << " " << j / i / r << "." << endl;
    cout << fr() / 7 + -fi() + j << " " << -fi() + j * fr() / 7 << " " << -j / fi() / fr() * 7 << "." << endl;

    return 0;
}
