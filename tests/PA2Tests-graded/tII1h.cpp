#include <iostream>
using namespace std;

float a,b;
int f ()
{
    cout << "<f1>";
    if (a > b) { return 0; }
    cout << "<f2>";
    return 0;
}

float g ()
{
    cout << "<g1>";
    if (a++ > b) { return a; }
    cout << "<g2>";
    return b;
}

bool h ()
{
    cout << "<h1>";
    if (a > b) { return a > b; }
    cout << "<h2>";
    return false;
}

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    cout << "begin";
    if (h()) { cout << "bad\n"; }
    cout << g();
    if (h()) { cout << "ok"; }
    cout << g();
    cout << "end\n";

    return 0;
}
