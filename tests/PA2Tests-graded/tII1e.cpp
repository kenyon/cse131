#include <iostream>
using namespace std;

int i = 2;
int j = 3;

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    float r = 2.5;
    float s = (2 - 0.25);
    cout << r + i + j << " " << i + j * r << " " << j / i / r << endl;
    return 0;
}
