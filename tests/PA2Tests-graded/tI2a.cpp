#include <iostream>
using namespace std;

const int c = 2 + 3;
const int e = c + c;
const float cf = 2.0 + 3.25;
const float ef = cf + cf;
const float cif = c + cf;
const bool db = false;
const bool eb = db || true;
const bool fb = eb && true;
const bool gb = (eb && false) || eb;

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    cout << c << e << endl;
    cout << cf << endl << ef << endl;
    cout << cif << endl;
    cout << db << eb << fb << gb << endl;
    const int c = 42 * 3;
    const bool d = (c / 10 < 13) && (c / 10 == 12);
    const int e = c % 10;
    const bool f = (e == 0) || (e + 1 <= 0) || (e - 7 >= 0) || !(e == 6);
    cout << -c << " " << d << " " << e << " " << f << "\n";

    return 0;
}
