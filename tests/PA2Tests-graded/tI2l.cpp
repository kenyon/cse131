#include <iostream>
using namespace std;

int i = 25;
int j = (i + i + i + i + i + i + i) / i;

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    cout << i / j << "\n";

    return 0;
}
