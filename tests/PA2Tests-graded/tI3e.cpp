#include <iostream>
using namespace std;

int x = 45;
int y = x++;
int z = ++x;

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    cout << x << " " << y << " " << z << endl;
    return 0;
}
