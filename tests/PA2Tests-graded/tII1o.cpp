#include <iostream>
using namespace std;

bool tok() { cout << "ok\n"; return true; }
bool fok() { cout << "ok\n"; return false; }
bool tnok() { cout << "nok\n"; return true; }
bool fnok() { cout << "nok\n"; return false; }

bool t = tok() && tok();
bool f = t;
int i = 1;

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    f = i == 0; t = i != 0;
    bool result = tok() && tok();
    cout << result << " " << t << "\n";
    return 0;
}
