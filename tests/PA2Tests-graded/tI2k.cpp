#include <iostream>
using namespace std;

int i = 26-1;
int j = i - 18;

int main() {
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    cout << i % j << "\n";
    return 0;
}
