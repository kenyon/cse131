#include <iostream>

using namespace std;

int x = 5;
float f = 5.0;
bool b = false;

int f1(int a)
{
    return a;
}

int f2(int a, int b)
{
    return a + b;
}

float f3(float a)
{
    return a;
}

float f4(float a, float b)
{
    return a + b;
}

float f5(float a, int b)
{
    return a + b;
}

bool f6(bool b)
{
    return !b;
}

float maxargs(int a, int b, float c, bool d, float e, bool f)
{
    cout << "maxargs sees " << a << b << c << d << e << f << endl;
    a = a * b;
    b = b * a;
    c = a * b + e + c;
    d = !f;
    e = e - c;
    f = !d;
    cout << "maxargs made " << a << b << c << d << e << f << endl;
    return 2.78;
}

int main()
{
    int y = 10;
    float g = 10.0;
    bool c = true;

    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    cout << f1(y) << endl;
    cout << f1(x) << endl;
    cout << f2(y, y) << endl;
    cout << f2(x, x) << endl;
    cout << f2(x, y) << endl;
    cout << f2(y, x) << endl;
    cout << f3(g) << endl;
    cout << f3(f) << endl;
    cout << f4(g, g) << endl;
    cout << f4(f, f) << endl;
    cout << f4(f, g) << endl;
    cout << f4(g, f) << endl;
    cout << f5(g, x) << endl;
    cout << f5(f, x) << endl;
    cout << f5(g, y) << endl;
    cout << f5(f, y) << endl;
    cout << f6(b) << endl;
    cout << f6(c) << endl;

    cout << maxargs(x, y, g, c, f, b) << endl;

    return 0;
}
