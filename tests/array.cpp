#include <iostream>
using namespace std;

int a[5];
int x = 22;
int xx = 0;

float f[3];
float w = 33.01;

bool lolz[10];
int ilolz[10];
float flolz[10];

int main()
{
    int i = 0;

    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    int b[4];
    int y = 477;
    int yy = 1;

    float g[6];
    float z = 55.11;

    int lolints[10];
    float lolfloats[10];
    bool lolbools[10];

    a[2] = 52;
    a[3] = 44;

    f[0] = 55.03;
    f[2] = -11.32;

    b[1] = 324;
    b[3] = 112;

    g[2] = 122.44;
    g[5] = -222.11;

    cout << a[2] << endl;
    cout << a[3] << endl;

    cout << f[0] << endl;
    cout << f[2] << endl;

    cout << b[1] << endl;
    cout << b[3] << endl;

    cout << g[2] << endl;
    cout << g[5] << endl;

    a[2] = x;
    a[1] = a[2];
    a[2] = 0;
    a[2] = a[1];
    a[3] = y;

    f[0] = w;
    f[1] = f[0];
    f[0] = 0;
    f[0] = f[1];
    f[2] = z;

    b[1] = x;
    b[3] = y;

    g[2] = w;
    g[5] = z;

    cout << a[2] << endl;
    cout << a[3] << endl;

    cout << f[0] << endl;
    cout << f[2] << endl;

    cout << b[1] << endl;
    cout << b[3] << endl;

    cout << g[2] << endl;
    cout << g[5] << endl;

    a[xx] = x;
    a[yy] = y;

    f[xx] = w;
    f[yy] = z;

    b[xx] = x;
    b[yy] = y;

    g[xx] = w;
    g[yy] = z;

    cout << a[xx] << endl;
    cout << a[yy] << endl;

    cout << f[xx] << endl;
    cout << f[yy] << endl;

    cout << b[xx] << endl;
    cout << b[yy] << endl;

    cout << g[xx] << endl;
    cout << g[yy] << endl;

    // lolz the global bool array
    while (i < 10) {
	if (i % 2 == 0) {
	    lolz[i] = true;
	} else {
	    lolz[i] = false;
	}
	++i;
    }
    i = 0;
    while (i < 10) {
	cout << "lolz[" << i << "]=" << lolz[i] << endl;
	++i;
    }

    i = 0;
    // ilolz the global int array
    while (i < 10) {
	if (i % 2 == 0) {
	    ilolz[i] = i * 10;
	} else {
	    ilolz[i] = i - 10;
	}
	++i;
    }
    i = 0;
    while (i < 10) {
	cout << "ilolz[" << i << "]=" << ilolz[i] << endl;
	++i;
    }

    i = 0;
    // flolz the global float array
    while (i < 10) {
	if (i % 2 == 0) {
	    flolz[i] = i * 3.1415;
	} else {
	    flolz[i] = (-i * 2.78) / 4.2;
	}
	++i;
    }
    i = 0;
    while (i < 10) {
	cout << "flolz[" << i << "]=" << flolz[i] << endl;
	++i;
    }

    i = 0;
    // lolints the local int array
    while (i < 10) {
	if (i % 2 == 0) {
	    lolints[i] = i + 100;
	} else {
	    lolints[i] = i - 100;
	}
	++i;
    }
    i = 0;
    while (i < 10) {
	cout << "lolints[" << i << "]=" << lolints[i] << endl;
	++i;
    }

    i = 0;
    // lolfloats the local float array
    while (i < 10) {
	if (i % 2 == 0) {
	    lolfloats[i] = -(i * 100) / 3.14;
	} else {
	    lolfloats[i] = (i * 500) / 9.5;
	}
	++i;
    }
    i = 0;
    while (i < 10) {
	cout << "lolfloats[" << i << "]=" << lolfloats[i] << endl;
	++i;
    }

    i = 0;
    // lolbools the local bool array
    while (i < 10) {
	if (i % 2 == 0) {
	    lolbools[i] = false;
	} else {
	    lolbools[i] = true;
	}
	++i;
    }
    i = 0;
    while (i < 10) {
	cout << "lolbools[" << i << "]=" << lolbools[i] << endl;
	++i;
    }

    return 0;
}
