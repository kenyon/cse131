#include <iostream>

int x = 12;
int y = x;

int main()
{
	std::cout << "1: " << x << std::endl;
	std::cout << "2: " << y << std::endl;

	return 0;
}
