#include <iostream>

using namespace std;

float gn = -1.5;
float gp = 10.5;

int main()
{
    float ln = -1.5;
    float lp = 10.5;

    cout.precision(2);
    cout << fixed;

    cout << "1: " << gn * 7.7 << endl;
    cout << "2: " << ln * 7.7 << endl;

    cout << "3: " << gp * 7.7 << endl;
    cout << "4: " << lp * 7.7 << endl;

    cout << "5: " << gn / 7.7 << endl;
    cout << "6: " << ln / 7.7 << endl;

    cout << "7: " << gp / 7.7 << endl;
    cout << "8: " << lp / 7.7 << endl;

    cout << "9: " << gp * gn << endl;
    cout << "10: " << gp / gn << endl;

    cout << "11: " << lp * ln << endl;
    cout << "12: " << lp / ln << endl;

    cout << "13: " << lp * gn << endl;
    cout << "14: " << gp * ln << endl;

    cout << "15: " << gp * lp << endl;
    cout << "16: " << gn * ln << endl;

    cout << "17: " << lp / gn << endl;
    cout << "18: " << gp / ln << endl;

    cout << "19: " << gp / lp << endl;
    cout << "20: " << gn / ln << endl;

    return 0;
}
