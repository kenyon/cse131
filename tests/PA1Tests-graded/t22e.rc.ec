function : int foo(int a, float b)
{
  return a;
}

function : float foo(float a, int b)
{
  return a + b + a + b;
}

function : void main()
{
  foo(1+4.20+1, 4.20);
  foo(1+4.20+1, 4.20, 9);
  foo(1+4.20+1, 9 == 9);
}
