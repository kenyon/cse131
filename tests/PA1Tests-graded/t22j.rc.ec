typedef int MI;
typedef MI MII;
typedef MII MIII;
typedef MIII MIV;
typedef MIV MV;

structdef MS {
  MI x;
  MIV y;
  function : float foo(MS* &b) { b->x = 5; return b->y;}
  function : float foo(MS** &b) { (**b).y = (MV) (float) ((**b).x); return (*b)->x; }
};

function : void foo(MII a) { }
function : void foo(float a) { }
function : void baz(MIV a) { }
function : void baz(float a) { }
function : void baz(MS &a) { }

function : bool main()
{
  MS r;
  MS* s;
  MS** p;
  foo((int)(5==5));
  foo((float)(5));
  baz(r);
  baz((float)(bool*)(int)(false));
  r.x = (int) r.foo(s) + (MII) r.foo(p) + r.foo(s) + (int) r.foo(p);
  return (bool) (r.foo(s) + r.foo(p) + r.foo(s) + r.foo(p));
  baz(r, r);
}
