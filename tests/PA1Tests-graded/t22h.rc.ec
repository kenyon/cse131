function : void foo(int a) {}

structdef MS {
  int x;
  int y;
  function : void foo(int a) {}
  function : int foo(int &a) { return a + this.x; }
  function : float foo(int &a) { return a + this.x; }
};

function : void main()
{
}
