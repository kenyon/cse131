function : void foo(float a) {}

structdef MS {
  int x;
  int y;
  function : void foo(float a) {}
  function : void foo(int a, int b) {}
  function : void foo(int a, float b) {}
};

function : void foo(int a, int b) {}

MS r;

function : void main()
{
  r.foo(1);
  r.foo(1, 3+4+5+6+7+8+9+0);
  r.foo(3, 4.5);
  foo(4);
  foo(3+4+5+3*2/2, 4);
  r.foo(4.3);
}
