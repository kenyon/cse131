function : int foo(int a, float b)
{
  return 0;
}

function : int foo(float a, int b, int c)
{
  return 0;
}

function : void main()
{
  foo(5);
  foo();
  foo((float)3, 4, 5);
}
