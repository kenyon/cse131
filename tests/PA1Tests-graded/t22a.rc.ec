function : int foo(int a)
{
  return a;
}

function : int foo(int a, int b)
{
  return a + b;
}

function : int main()
{
  return foo(1, 2);
}
