#include <iostream>
using namespace std;

int x = 5;
int x2 = 0;

float f = 3.22;
float f2 = 0.00;

bool b = true;

int main()
{
	cout << boolalpha;
	cout.precision(2);
	cout << fixed;

	int lx = 555;
	int lx2 = 0;

	float lf = 322.22;
	float lf2 = 0.00;

	bool lb = false;

	cout << (float)x << endl;

	cout << (float)b << endl;

	cout << (float)lx << endl;

	cout << (float)lb << endl;

	cout << (int)f << endl;

	cout << (int)b << endl;

	cout << (int)lf << endl;

	cout << (int)lb << endl;

	cout << (bool)x << endl;

	cout << (bool)f << endl;

	cout << (bool)lx << endl;

	cout << (bool)lf << endl;

	cout << (bool)x2 << endl;

	cout << (bool)f2 << endl;

	cout << (bool)lx2 << endl;

	cout << (bool)lf2 << endl;

	return 0;
}
