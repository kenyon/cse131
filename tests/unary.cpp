#include <iostream>

int x;

int foo()
{
	return -1;	// Need to fix unary minus/plus to avoid error
}

int i;

int main()
{
	i = 5;
	x = foo();

	std::cout << "i = " << i << std::endl;
	std::cout << "x = " << x << std::endl;

	return 0;
}
