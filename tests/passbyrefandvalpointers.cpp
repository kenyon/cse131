#include <iostream>
using namespace std;

int * x;
int xval = 572;
int xval2 = 129;

float * f;
float fval = 3.6;

bool * b;
bool bval = true;

int f1(int* a)
{
	a = &xval2;
	return *a;
}

int f2(int* &b)
{
	b = &xval2;
	return *b;
}

int main()
{
	int * lx;
	int lxval = 55;

	float * lf;
	float lfval = 36.36;

	bool * lb;
	bool lbval = true;

	x = &xval;

	int* px = x;

	cout << f1(x) << endl;

	cout << *x << endl;

	cout << f2(x) << endl;

	cout << *x << endl;

	return 0;
}
