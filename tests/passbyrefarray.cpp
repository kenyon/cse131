#include <iostream>
using namespace std;

int a[10];

void printArrayElement(int *x)
{
	cout << *x << endl;
	cout << x[0] << endl;
	cout << x[1] << endl;
	cout << x[7] << endl;
}

int main()
{
	a[0] = 5;
	a[1] = 50;
	a[7] = 500;

	printArrayElement(a);

	return 0;
}
