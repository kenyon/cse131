#include <iostream>
using namespace std;

int * x;
int xval = 572;

float * f;
float fval = 3.6;

bool * b;
bool bval = true;

int main()
{
    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    int * lx;
    int lxval = 55;

    float * lf;
    float lfval = 36.36;

    bool * lb;
    bool lbval = true;

    x = &xval;

    cout << *x << endl;

    f = &fval;

    cout << *f << endl;

    b = &bval;

    cout << *b << endl;

    lx = &lxval;

    cout << *lx << endl;

    lf = &lfval;

    cout << *lf << endl;

    lb = &lbval;

    cout << *lb << endl;

    return 0;
}
