#include <iostream>

using namespace std;

int global = 88;
int global2;

float gf1;
float gf2;

int retintexpr1()
{
    int x = 66 + 99;

    return x;
}

int retintexpr2()
{
    int x = 987456;

    return 77 + 99;
}

int retintexpr3()
{
    return global;
}

float retfloatexpr1()
{
    float f = 3.14;
    float g = 99.87;

    return f + g;
}

float retfloatexpr2()
{
    float f = 3.14;
    float g = 99.87;
    float h = f * -g;

    return h; // -313.59
}

float retfloatexpr3()
{
    return 12.34;
}

float retfloatexpr4()
{
    return gf1;
}

bool retbool1()
{
    bool lol = true;

    return lol;
}

bool retbool2()
{
    // return a literal bool
    return false;
}

bool retbool3()
{
    bool gt = 99 > 66;

    return gt;
}

void voidfunc1()
{
    int x = 93785;

    if (true) {
	float lol = 45.54;

	x = 1928;

	cout << "19: " << "In voidfunc1!" << x << endl;

	return;
    }

    cout << "If you are seeing this, it is wrong!" << endl;
}

void voidfunc2()
{
    // empty functions should be ok!
}

void voidfunc3()
{
    float x = 92.14;

    cout << "20: " << "In voidfunc3!" << x << endl;

    return;

    cout << "If you are seeing this, it is wrong!" << endl;
}

int main()
{
    int local1;
    float local2;

    cout.precision(2);
    cout << fixed;

    cout << "1: " << retintexpr1() << endl;
    cout << "2: " << retintexpr2() << endl;
    cout << "3: " << retintexpr3() << endl;

    global2 = retintexpr1();
    cout << "4: " << global2 + 10 << endl;

    local1 = retintexpr2();
    cout << "5: " << local1 - 5.3 << endl;

    gf1 = retintexpr1();
    cout << "6: " << gf1 << endl;

    gf2 = retintexpr2() - 1.5;
    cout << "7: " << gf2 << endl;

    local2 = retintexpr1() * 20;
    cout << "8: " << local2 << endl;

    cout << "9: " << retfloatexpr1() << endl;
    cout << "10: " << retfloatexpr2() << endl;
    cout << "11: " << retfloatexpr3() << endl;
    cout << "12: " << retfloatexpr4() << endl;

    gf1 = retfloatexpr1();
    cout << "13: " << gf1 << endl;

    gf2 = retfloatexpr2();
    cout << "14: " << gf2 << endl;

    local2 = retfloatexpr3();
    cout << "15: " << local2 << endl;

    cout << "16: " << retbool1() << endl;
    cout << "17: " << retbool2() << endl;
    cout << "18: " << retbool3() << endl;

    voidfunc1();
    voidfunc2();
    voidfunc3();

    return 0;
}
