#include <iostream>

using namespace std;

int gx = 5;
const int cgx = 7;

float gf = 3.3;
const float cgf = 4.4;

int main()
{
    int x = 3;
    const int y = 2;

    float f = 3.2;
    const float cf = 2.6;

    if (true) {
	    cout << "1: " << "true" << endl;
    }

    if (false) {
	    cout << "2: " << "false" << endl;
    }

    if (x > y) {
	    cout << "3: " << "true" << endl;
    }

    if (y > x) {
	    cout << "4: " << "false" << endl;
    }

    if (1 > 2) {
	    cout << "5: " << "false" << endl;
    }

    if (2.1 > 1.1) {
	    cout << "6: " << "true" << endl;
    }

    if (gf > cgf) {
	    cout << "7: " << "false" << endl;
    }

    if (gf > f) {
	    cout << "8: " << "true" << endl;
    }

    if (gf > x) {
	    cout << "9: " << "true" << endl;
    }

    if (y > cgf) {
	    cout << "10: " << "false" << endl;
    }

    if (2.2 > 5) {
	    cout << "11: " << "false" << endl;
    }

    if (6 > 10.5) {
	    cout << "12: " << "false" << endl;
    }

    if (5.6 > 2) {
	    cout << "13: " << "true" << endl;
    }

    if (6 > 2.5) {
	    cout << "14: " << "true" << endl;
    }

    if (5555 > 55.55) {
	    cout << "15: " << "true" << endl;
    }

    return 0;
}
