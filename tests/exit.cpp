#include <iostream>

using namespace std;

void voidfunc1()
{
    int x = 93785;

    if (true) {
	float lol = 45.54;

	x = 192;

	cout << "1: " << "In voidfunc1! Exiting " << x << endl;

	exit(x);
    }

    cout << "If you are seeing this, it is wrong!" << endl;
}


int main()
{
    int local1;
    float local2;
    bool local3;

    voidfunc1();

    return 0;
}
