#include <stdio.h>

int x = 5;
const int w = 6;

int main()
{
	int y = 9;
	const int z = 9;

	printf("%d\n", x | y & z ^ w);

	return 0;
}
