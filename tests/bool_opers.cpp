#include <iostream>

using namespace std;

bool gt = true;
bool gf = false;

// Testing operators that return bool: Phase II.1.4

int main()
{
    bool t = true;
    bool f = false;

    cout << boolalpha;

    cout << "1: " << (1 > -2) << endl;
    cout << "2: " << (1 > 1) << endl;
    cout << "3: " << (2 < 5) << endl;
    cout << "4: " << (6 < 6) << endl;
    cout << "5: " << (9 >= 9) << endl;
    cout << "5.5: " << (9 >= 10) << endl;
    cout << "6: " << (10 >= 9) << endl;
    cout << "7: " << (10 <= 20) << endl;
    cout << "8: " << (10 <= 10) << endl;
    cout << "8.5: " << (90 <= 10) << endl;
    cout << "9: " << (10 == 10) << endl;
    cout << "10: " << (9 == 10) << endl;
    cout << "11: " << (10 != 10) << endl;
    cout << "12: " << (9 != 10) << endl;

    cout << endl;

    cout << "1f: " << (1.5 > -2.5) << endl;
    cout << "2f: " << (1.5 > 1.5) << endl;
    cout << "3f: " << (2.5 < 5.5) << endl;
    cout << "4f: " << (6.5 < 6.5) << endl;
    cout << "5f: " << (9.5 >= 9.5) << endl;
    cout << "5.5f: " << (9.5 >= 10.5) << endl;
    cout << "6f: " << (10.5 >= 9.5) << endl;
    cout << "7f: " << (10.5 <= 20.5) << endl;
    cout << "8f: " << (10.5 <= 10.5) << endl;
    cout << "8.5f: " << (90.5 <= 10.5) << endl;
    cout << "9f: " << (10.5 == 10.5) << endl;
    cout << "10f: " << (9.5 == 10.5) << endl;
    cout << "11f: " << (10.5 != 10.5) << endl;
    cout << "12f: " << (9.5 != 10.5) << endl;

    cout << endl;

    cout << "13: " << ((1 == 2) && (1 == 1)) << endl;
    cout << "14: " << ((1 == 2) || (1 == 1)) << endl;
    cout << "15: " << (!((1 == 2) && (1 == 1))) << endl;
    cout << "16: " << (!((1 == 2) || (1 == 1))) << endl;

    cout << endl;

    cout << "17: " << (true && false) << endl;
    cout << "18: " << (t && f) << endl;
    cout << "19: " << (f && t) << endl;
    cout << "20: " << (false && true) << endl;
    cout << "21: " << (!(false && true)) << endl;
    cout << "22: " << (true || false) << endl;
    cout << "23: " << (t || f) << endl;
    cout << "24: " << (!(t || f)) << endl;
    cout << "25: " << (!(true || false)) << endl;

    cout << endl;

    cout << "26: " << (!true) << endl;
    cout << "27: " << (!false) << endl;

    cout << "28: " << (!t) << endl;
    cout << "29: " << (!f) << endl;

    cout << "30: " << (!gt) << endl;
    cout << "31: " << (!gf) << endl;

    return 0;
}
