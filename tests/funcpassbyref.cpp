#include <iostream>

using namespace std;

int x = 5;
float f = 5.0;
bool b = true;

int f1(int &a)
{
    a++;
    return a;
}

float floater(float &a)
{
    a++;
    return a;
}

bool booler(bool &b)
{
    b = !b;
    return b;
}

void voidsingleref(int &a)
{
    a = a * 10;
}

void multirefs(int &a, float &b, bool &c)
{
    a = a * a;
    b = b * b;
    c = !c;
}

void voidtworefs(int &a, float &b)
{
    a = a * 12;
    b = b * 12;
}

void mixed(int a, float &b, bool c)
{
    a = a * 5;
    b = b / 5;
    c = !c;
    return;
}

float maxargs(float a, float &b, int c, int &d, bool e, bool &f)
{
    cout << "1maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;
    a = a * a;

    cout << "2maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;
    b = b / a;

    cout << "3maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;
    c = c % 7;

    cout << "4maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;
    d = d % 7;

    cout << "5maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;
    e = !e;

    cout << "6maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;
    f = !f;

    cout << "7maxargs: " << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << endl;

    return a;
}
int main()
{
    int y = 10;
    float g = 10.0;
    bool c = false;

    cout << boolalpha;
    cout.precision(2);
    cout << fixed;

    // local int
    cout << "1: ";
    cout << y << endl;
    cout << "2: ";
    cout << f1(y) << endl;
    cout << "3: ";
    cout << y << endl;

    // global int
    cout << "4: ";
    cout << x << endl;
    cout << "5: ";
    cout << f1(x) << endl;
    cout << "6: ";
    cout << x << endl;

    // local float
    cout << "7: ";
    cout << g << endl;
    cout << "8: ";
    cout << floater(g) << endl;
    cout << "9: ";
    cout << g << endl;

    // global float
    cout << "10: ";
    cout << f << endl;
    cout << "11: ";
    cout << floater(f) << endl;
    cout << "12: ";
    cout << f << endl;

    // local bool
    cout << "13: ";
    cout << c << endl;
    cout << "14: ";
    cout << booler(c) << endl;
    cout << "15: ";
    cout << c << endl;

    // global bool
    cout << "16: ";
    cout << b << endl;
    cout << "17: ";
    cout << booler(b) << endl;
    cout << "18: ";
    cout << b << endl;

    multirefs(x, f, b);
    cout << "19: ";
    cout << "x=" << x << " f=" << f << " b=" << b << endl;

    multirefs(y, g, c);
    cout << "20: ";
    cout << "y=" << y << " g=" << g << " c=" << c << endl;

    cout << "20.5: ";
    cout << "y=" << y << endl;
    voidsingleref(y);
    cout << "21: ";
    cout << "y=" << y << endl;

    voidsingleref(x);
    cout << "22: ";
    cout << "x=" << x << endl;

    cout << "23: ";
    cout << "y=" << y << " g=" << g << endl;
    voidtworefs(y, g);
    cout << "24: ";
    cout << "y=" << y << " g=" << g << endl;

    cout << "25: ";
    cout << "y=" << y << " g=" << g << " c=" << c << endl;
    mixed(y, g, c);
    cout << "26: ";
    cout << "y=" << y << " g=" << g << " c=" << c << endl;

    cout << "27: ";
    cout << "x=" << x << " f=" << f << " b=" << b << endl;
    mixed(x, f, b);
    cout << "28: ";
    cout << "x=" << x << " f=" << f << " b=" << b << endl;

    cout << "29: ";
    cout << "x=" << x << " f=" << f << " c=" << c << endl;
    maxargs(3.14, f, 52, x, true, c);
    cout << "x=" << x << " f=" << f << " c=" << c << endl;

    return 0;
}
