#include <iostream>
using namespace std;

int main()
{
	int *x = new int;
	int y = 1002;

	x = &y;
	cout << "*x=" << *x << endl;

	*x = 1001;
	cout << "*x=" << *x << endl;

	delete x;

	return 0;
}
