//---------------------------------------------------------------------
// For typedefs like: typedef int myInteger1, myInteger2;
// Also can hold the structdefs
//---------------------------------------------------------------------

class TypedefSTO extends STO
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	TypedefSTO (String strName)
	{
		super (strName);
	}

	public 
	TypedefSTO (String strName, Type typ)
	{
		super (strName, typ);
	}

	public 
	TypedefSTO (String strName, Type typ, boolean isCompleteDef)
	{
		super (strName, typ);
		this.setIsCompleteDef(isCompleteDef);
	}

	/*
	public
	TypedefSTO(String strName, Type t, String aliastype)
	{
		super(strName, t);
	}
	*/

	public boolean isStruct()
	{
		return !this.isTypedef();
	}

	public String toString()
	{
		return this.getClass().getName() + "[" + this.getName() + "," + this.getType() + ",c=" + this.isCompleteDef + "]";
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean
	isTypedef ()
	{
		return !this.getType().isStruct();
	}

	public boolean isCompleteDef()
	{
		return this.isCompleteDef;
	}

	public void setIsCompleteDef(boolean isCompleteDef)
	{
		//System.out.println("Setting isCompleteDef to " + isCompleteDef);

		this.isCompleteDef = isCompleteDef;
	}

	// Whether this STO refers to a completed definition, in the case of
	// structs. This is needed when a struct has a field which is a pointer
	// to a struct type of itself.
	private boolean isCompleteDef;
}
