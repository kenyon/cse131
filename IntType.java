class IntType extends NumericType
{
	public IntType()
	{
		super("int");
	}

	public boolean	isInt ()	{ return true; }

	public String toString () 	{ return "int";}

	public boolean isAssignable(Type t) {
		/*
		if (t.isPointer()) {
			if (((PointerType)t).getPointedType() instanceof NumericType) {
				return true;
			}
		}
		*/

		return t instanceof NumericType;
	}
}
