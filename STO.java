//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

abstract class STO implements Cloneable
{
	private static final String DATASIZE = "4";
	private static final String SEPARATOR = "\t";

	private static final String SET_OP = "set";

	private static final String THREE_PARAM = "%s" + SEPARATOR + "%s, %s, %s";
	private static final String TWO_PARAM = "%s" + SEPARATOR + "%s, %s";
	private static final String ONE_PARAM = "%s" + SEPARATOR + "%s";

	private static final String SAVE_ENDOFFUNC = "SAVE.%s = -(92 + %s) & -8";
	private static final String GLOBAL = ".global %s";
	private static final String ALIGN = ".align %s";
	private static final String SECTION = ".section \".%s\"";
	private static final String LABEL = "%s:";

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	STO (String strName)
	{
		this(strName, null);
	}

	public 
	STO (String strName, Type typ)
	{
		setName(strName);
		setType(typ);
		setIsAddressable(false);
		setIsModifiable(false);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String
	getName ()
	{
		return m_strName;
	}

	public void
	setName (String str)
	{
		m_strName = str;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Type
	getType ()
	{
		return	m_type;
	}

	public void
	setType (Type type)
	{
		m_type = type;
	}


	//----------------------------------------------------------------
	// Addressable refers to if the object has an address. Variables
	// and declared constants have an address, whereas results from 
	// expression like (x + y) and literal constants like 77 do not 
	// have an address.
	//----------------------------------------------------------------
	public boolean
	getIsAddressable ()
	{
		return	m_isAddressable;
	}

	protected void
	setIsAddressable (boolean addressable)
	{
		m_isAddressable = addressable;
	}

	//----------------------------------------------------------------
	// You shouldn't need to use these two routines directly
	//----------------------------------------------------------------
	private boolean
	getIsModifiable ()
	{
		return	m_isModifiable;
	}

	protected void
	setIsModifiable (boolean modifiable)
	{
		m_isModifiable = modifiable;
	}


	//----------------------------------------------------------------
	// A modifiable L-value is an object that is both addressable and
	// modifiable. Objects like constants are not modifiable, so they 
	// are not modifiable L-values.
	//----------------------------------------------------------------
	public boolean
	isModLValue ()
	{
		return	getIsModifiable() && getIsAddressable();
	}

	private void
	setIsModLValue (boolean m)
	{
		setIsModifiable(m);
		setIsAddressable(m);
	}

	public boolean equals(STO sto)
	{
		//System.out.println("STO.equals called;");
		return this.getName().equals(sto.getName());
	}


	public String toString()
	{
		return this.getClass().getName() + "[" + this.getName() + "," + this.getType() + ", addressable: " + this.getIsAddressable() + ", modifiable: " + this.getIsModifiable() + ", ref: " + this.getIsRef() + "]";
	}

	public int
	getLocalOffset ()
	{
		return	m_localOffset;
	}

	public void
	setLocalOffset (int offset)
	{
		m_localOffset = offset;
	}
	
	public boolean
	getIsRef ()
	{
		return	m_isRef;
	}

	public void
	setIsRef (boolean isRef)
	{
		m_isRef = isRef;
	}
	
	public STO
	getArrayLoc ()
	{
		return	m_arrayLoc;
	}

	public void
	setArrayLoc (STO arrayLoc)
	{
		m_arrayLoc = arrayLoc;
	}
	
	public FuncSTO
	getPointedFunc ()
	{
		return	m_pointedFunc;
	}

	public void
	setPointedFunc (FuncSTO func)
	{
		m_pointedFunc = func;
	}
		
	public String
	getIntoRegAssembly(String reg, int indent_level)
	{
	    String tabs = "";
	    for (int i = 1; i <= indent_level; i++) {
		tabs += "\t";
	    }

	    //location for each type(static, const, local, global, float, non float)
	    if (this.getName().matches(".*_base_offsetby_.*") && !this.getName().matches(".*addressof:.*")){
		String nameOfBase = "";
		int endOfBase = this.getName().indexOf("_base_offsetby_");

		nameOfBase = this.getName().substring(0, endOfBase);
		//nameOfBase += "_0_";
		
		String temp = "";
		
		if (this.getArrayLoc() == null)
		{
		    //System.out.println("arrayloc is null of " + this.getName());
		}
		
		if (this.getArrayLoc().getLocalOffset() != 0) //offset is local
		{
		    temp = "ld\t[%%fp-" + this.getArrayLoc().getLocalOffset() + "], %%l3\n";
		}
		else //offset is global
		{
		    if (this.getArrayLoc().isConst()) //offsset is const
		    {
			temp = "set\t" + ((ConstSTO) this.getArrayLoc()).getIntValue() + ", %%l3\n";
		    }
		    else //offset is global var
		    {
			temp = "set\t" + this.getArrayLoc().getName() + ", %%l3\n" +
		    		tabs + "ld\t[%%l3], %%l3\n";
		    }
		}
		
		String temp1 = "";
		if (this.getLocalOffset() == 0) //global array
		{
			temp1 = tabs + "set\t"+ nameOfBase + ", %%l2\n";
		}
		else //local array
		{
			temp1 = tabs + "mov\t%%fp, %%l4\n" + 
    				tabs + "set\t"+ this.m_localOffset + ", %%l2\n" +
    				tabs + "sub\t%%l4, %%l2, %%l2\n";
		}
		
		return temp + //offset value into l3
			tabs + "smul\t%%l3, 4, %%l3\n" +
			temp1 + //address of base into l2
    			tabs + "sub\t%%l2, %%l3, %%l2\n" +
    			tabs + "ld\t[%%l2], " + reg;
	    }
	    else if (this.getType().isFuncPtr()) {
		if (((FuncPtrType)this.getType()).getReturnType().isFloat()) {
		    //return "fmovs\t%%f5, " + reg;
		    return "ld\t[%%fp-" + this.getLocalOffset() + "], " + reg;
		} else if (!((FuncPtrType)this.getType()).getReturnType().isFloat()) { // return type is not float
		    if (reg.charAt(2) == 'f') {
			    return "ld\t[%%fp-" + this.getLocalOffset() + "], " + reg + "\n"
				+ tabs + "fitos\t" + reg + ", " + reg;
			} else {
			    return "ld\t[%%fp-" + this.getLocalOffset() + "], " + reg
				+ "\t! not putting into float reg, in getIntoRegAssembly";
			}
		} else {
		    return "Error: Unhandled condition in getIntoRegAssembly!";
		}
	    } else if (this.getType().isFloat()) {
            	    if (this.isConst()) {
            		return ".section \".data\"\n" +
            			tabs + ".align 4\n" +
            			"float" + AssemblyCodeGenerator.floatnum + ":\t.single\t0r" + ((ConstSTO)this).getFloatValue() + "\n" +
            			tabs + ".section \".text\"\n" +
            			tabs + ".align 4\n" + 
            			tabs + "set\tfloat" + AssemblyCodeGenerator.floatnum++ + ", %%l0\n" +
                        	tabs + "ld\t[%%l0], " + reg;
            	    } else if (this.m_localOffset == 0) {
            		return "set\t" + this.getName() + ", %%l0\n" +
            			tabs + "ld\t[%%l0], " + reg;
            	    } else if (this.m_localOffset != 0) {
            		return "ld\t" + "[%%fp-" + this.m_localOffset + "], "+ reg;
            	    }
	    } else { // not a float
		String retstr = new String();

		if (this.isConst()) {
		    if (reg.charAt(2) == 'f') {
			MyParser.incOffset();

			return "set\t" + ((ConstSTO)this).getIntValue() + ", %%l0\n" +
				tabs + "st\t%%l0, [%%fp-" + MyParser.getOffset() + "]\n" +
				tabs + "ld\t[%%fp-" + MyParser.getOffset() + "], " + reg + "\n" +
				tabs + "fitos\t" + reg + ", " + reg;
		    } else {
			retstr = "set\t" + ((ConstSTO)this).getIntValue() + ", " + reg;
			return retstr;
		    }
		} else if (this.m_localOffset == 0) {
		    retstr = "set\t" + this.getName() + ", %%l0\n"
			    + tabs + "ld\t[%%l0], " + reg;

		    if (reg.charAt(2) == 'f') { // we are promoting this to a float
			retstr += "\n" + tabs + "fitos\t" + reg + ", " + reg;
		    }

		    return retstr;
		} else if (this.m_localOffset != 0) {
		    retstr = "ld\t" + "[%%fp-" + this.m_localOffset + "], " + reg;

		    if (reg.charAt(2) == 'f') { // we are promoting this to a float
			retstr += "\n" + tabs + "fitos\t" + reg + ", " + reg;
		    }

		    return retstr;
		} else {
		    return "Error: Unhandled condition in STO.java: getIntoRegAssembly()";
		}
	    }
    	    //else if (static)
    	    //{
    	    //}

	    return "WowErrorInGetIntoRegAssembly";
	}
	

	public String
	getLocIntoRegAssembly(String reg, int indent_level)
	{
	    String tabs = "";
	    for (int i = 1; i <= indent_level; i++)
	    {
		tabs += "\t";
	    }

	    //location for each type(static, const, local, global, float, non float)
	    if (this.getName().matches(".*_base_offsetby_.*")){
		String nameOfBase = "";
		int endOfBase = this.getName().indexOf("_base_offsetby_");

		nameOfBase = this.getName().substring(0, endOfBase);
		//nameOfBase += "_0_";

		String temp = "";
		if (this.getArrayLoc().getLocalOffset() != 0) //the offset expr result is a local
		{
		    temp = "ld\t[%%fp-" + this.getArrayLoc().getLocalOffset() + "], %%l3\t\t!b1\n";
		}
		else //the offset expr result is a global
		{
		    if (!this.getArrayLoc().isConst()) //the offset is a const
		    {
			temp = "set\t" + this.getArrayLoc().getName() + ", %%l3\t\t!b2\n" + 
				tabs + "ld\t[%%l3], %%l3\n";
		    }
		    else //the offset is a global var
		    {
			temp = "set\t" + ((ConstSTO) this.getArrayLoc()).getIntValue() + ", %%l3\t\t!b2\n";
		    }
		}

		String temp2 = "";
		if (this.getLocalOffset() == 0) //the array is global
		{
		    temp2 = tabs + "set\t"+ nameOfBase + ", %%l2\n";
		}
		else //the array is local
		{
		    temp2 = tabs + "mov\t%%fp, %%l4\n" +
		    	tabs + "set\t"+ this.m_localOffset + ", %%l2\n" +
		    	tabs + "sub\t%%l4, %%l2, %%l2\n";
		}

		return temp +
			tabs + "smul\t%%l3, 4, %%l3\n" + //value of the offset*4 is now in l3
			temp2 + //address of base is in l2
			tabs + "sub\t%%l2, %%l3, " + reg;
	    }
	    else
	    {
	    	    if (this.m_localOffset == 0)
	    	    {
	    		return "set\t" + this.getName() + ", " + reg;
	    	    }
	    	    else if (this.m_localOffset != 0)
	    	    {
	    		return "mov\t%%fp, %%l4\n" +
    				tabs + "set\t"+ this.m_localOffset + ", %%l3\n" +
    				tabs + "sub\t%%l4, %%l3, "+ reg;
	    	    }
	    }
    	    //else if (static)
    	    //{
    	    //}
	    return "wowerroringetlocintoregassembly";
	}
	
	//----------------------------------------------------------------
	//	It will be helpful to ask a STO what specific STO it is.
	//	The Java operator instanceof will do this, but these methods 
	//	will allow more flexibility (ErrorSTO is an example of the
	//	flexibility needed).
	//----------------------------------------------------------------
	public boolean	isVar () 	{ return false; }
	public boolean	isConst ()	{ return false; }
	public boolean	isExpr ()	{ return false; }
	public boolean	isFunc () 	{ return false; }
	public boolean	isTypedef () 	{ return false; }
	public boolean	isError () 	{ return false; }
	public boolean	isStruct () 	{ return false; }

	public Object clone()
	{
	    try {
		return super.clone();
	    } catch (CloneNotSupportedException e) {
		return null;
	    }
	}

	//----------------------------------------------------------------
	// 
	//----------------------------------------------------------------
	private String  	m_strName;
	private Type		m_type;
	private boolean		m_isAddressable;
	private boolean		m_isModifiable;
	private int		m_localOffset;
	private boolean		m_isRef;
	private STO		m_arrayLoc = null;
	private FuncSTO		m_pointedFunc;
}
