import java.util.Vector;

class FuncPtrType extends PtrGrpType implements Cloneable
{
	public FuncPtrType()
	{
		super("function");
	}

	public FuncPtrType(Type returntype)
	{
		super("function");
		//System.out.println("FuncPtrType constructor: creating FuncPtrType with returntype=" + returntype);
		this.returntype = returntype;
	}

	public FuncPtrType(Type returntype, Vector<STO> paramlist)
	{
		super("function");
		this.returntype = returntype;
		this.paramlist = paramlist;
	}

	public boolean	isFuncPtr ()	{ return true; }

	public String toString ()
	{
		//return "function";
		return "funcptr : " + this.returntype + " (" + this.paramPrinter(this.paramlist) + ")";
	}

	/// NOT DONE HERE. need to add stars for pointers.
	private String paramPrinter(Vector<STO> v)
	{
		if (v == null) return "";

		String result = new String();

		for (int i = 0; i < v.size(); ++i) {
			result += (Type)v.elementAt(i).getType() + " " + (String)v.elementAt(i).getName() + (i == v.size() - 1 ? "" : ", ");
		}

		return result;
	}
	/// NOT DONE HERE.

	public Vector<STO> getParamList()
	{
		return this.paramlist;
	}

	public Type getReturnType()
	{
		return this.returntype;
	}
	
	public void setReturnType(Type t)
	{
		returntype = t;
	}

	public Object clone()
	{
	    FuncPtrType copy = (FuncPtrType)super.clone();

	    if (returntype != null) {
		copy.returntype = (Type)returntype.clone();
	    }

	    if (paramlist != null) {
		copy.paramlist = (Vector<STO>)paramlist.clone();
	    }

	    return copy;
	}

	private Type returntype;
	private Vector<STO> paramlist;
}
