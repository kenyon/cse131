//---------------------------------------------------------------------
// This is the top of the Type hierarchy. You most likely will need to
// create sub-classes (since this one is abstract) that handle specific
// types, such as IntType, FloatType, ArrayType, etc.
//---------------------------------------------------------------------


class Type implements Cloneable
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	Type (String strName, int size)
	{
		//System.out.println("Type (String strName, int size) called.");

		setName(strName);
		setSize(size);
	}

	public
	Type (String strName, String aliastype)
	{
		this(strName, 4);
		this.setAliasType(aliastype);

		//System.out.println("Type (String strName, String aliastype) called.");
	}

	public void setOptModifierList(String mods)
	{
		this.m_opt_modifier_list = mods;
	}

	public String getOptModifierList()
	{
		return this.m_opt_modifier_list;
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public String
	getName ()
	{
		return m_typeName;
	}

	public void
	setName (String str)
	{
		m_typeName = str;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public int
	getSize ()
	{
		return m_size;
	}

	private void
	setSize (int size)
	{
		m_size = size;
	}

	public boolean isEquivalent(Type t) {
		return this.getClass() == t.getClass();
	}

	public boolean isAssignable(Type t) {
	    return false;
	}

	public String
	getAliasType()
	{
		return m_aliastype;
	}

	public void
	setAliasType(String s)
	{
		this.m_aliastype = s;
	}

	public String
	getParentAliasType()
	{
		return m_parentaliastype;
	}

	public void
	setParentAliasType(String s)
	{
		this.m_parentaliastype = s;
	}

	public String
	toString()
	{
		return "[[aliastype: " + this.getAliasType() + "]]";
	}

	public void setDimensionSize(int x)
	{
	}

	//----------------------------------------------------------------
	//	It will be helpful to ask a Type what specific Type it is.
	//----------------------------------------------------------------
	public boolean	isInt ()	{ return false; }
	public boolean	isBasic()	{ return false; }
	public boolean	isVoid()	{ return false; }
	public boolean	isComposite()	{ return false; }
	public boolean	isNumeric()	{ return false; }
	public boolean	isBool()	{ return false; }
	public boolean	isChar()	{ return false; }
	public boolean	isArray()	{ return false; }
	public boolean	isStruct()	{ return false; }
	public boolean	isPtrGrp()	{ return false; }
	public boolean	isFloat()	{ return false; }
	public boolean	isFuncPtr()	{ return false; }
	public boolean	isPointer()	{ return false; }


	public Object clone()
	{
	    try {
		return super.clone();
	    } catch (CloneNotSupportedException e) {
		return null;
	    }
	}


	//----------------------------------------------------------------
	//	Name of the Type (e.g., int, bool, or some typedef
	//----------------------------------------------------------------
	private String  	m_typeName;
	private int		m_size;

	private String m_aliastype = null;
	private String m_parentaliastype;

	// Just a string of optional modifiers, like * for pointers.
	private String m_opt_modifier_list;
}
