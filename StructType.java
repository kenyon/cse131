import java.util.Vector;

class StructType extends CompositeType
{
	public StructType()
	{
		super("struct");
	}

	public StructType(Vector<STO> fields)
	{
		super("struct");
		this.fields = fields;
	}

	public boolean	isStruct ()	{ return true; }

	public String toString () 	{ return "struct" + this.getFields(); }

	public Vector<STO> getFields()
	{
		return this.fields;
	}

	/**
	 * Returns the number of fields this struct has.
	 */
	public int numFields()
	{
	    return this.getFields().size();
	}

	public void setFields(Vector<STO> fields)
	{
		this.fields = fields;
	}

	/*
	public Vector<String> getFieldNames()
	{
		return field_names;
	}

	private void setFieldNames(Vector v)
	{
		this.field_names = v;
	}
	*/

	//private Vector<String> field_names;

	private Vector<STO> fields;
}
