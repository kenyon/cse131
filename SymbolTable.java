
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
import java.util.*;


class SymbolTable
{
	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public 
	SymbolTable ()
	{
		m_nLevel = 0;
		m_stkScopes = new Stack ();
		m_scopeGlobal = null;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	insert (STO sto)
	{
		Scope		scope = (Scope) m_stkScopes.peek ();
		//System.out.println("SymbolTable just took this in the anus: " + sto.getName());
		scope.InsertLocal (sto);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO
	accessGlobal (String strName)
	{
	    //System.out.println("symtab access global: " + strName);
		return (m_scopeGlobal.access (strName));
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO
	accessLocal (String strName)
	{
		Scope		scope = (Scope) m_stkScopes.peek ();
		//System.out.println("symtab access local: " + strName);
		return (scope.accessLocal (strName));
	}


	/**
	 * Marks a struct TypedefSTO as complete in the local scope.
	 */
	public void markStructComplete(String id)
	{
		Scope scope = (Scope)m_stkScopes.pop();

		//System.out.println("scope: " + scope);

		scope.markStructComplete(id);

		m_stkScopes.push(scope);
	}

	public void insertStructFields(String id, Vector<STO> fields)
	{
		Scope scope = (Scope)m_stkScopes.pop();
		scope.insertStructFields(id, fields);
		m_stkScopes.push(scope);
	}
	
	/*public void setPointedFunc(String id, STO func)
	{
		Scope scope = (Scope)m_stkScopes.pop();
		scope.setPointedFunc(id, func);
		m_stkScopes.push(scope);
	}*/


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public STO
	access (String strName)
	{
	    //System.out.println("symtab access: " + strName);
		Stack cstk = (Stack)m_stkScopes.clone();

		STO		stoReturn = null;	

		while (!cstk.isEmpty()) {
			Scope sc = (Scope)cstk.pop();
			if ((stoReturn = sc.access(strName)) != null)
			{
				return stoReturn;
			}
		}

		return (null);
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	openScope ()
	{
		Scope		scope = new Scope();

		//	The first scope created will be the global scope.
		if (m_scopeGlobal == null)
			m_scopeGlobal = scope;

		m_stkScopes.push (scope);
		m_nLevel++;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public void
	closeScope ()
	{
		m_stkScopes.pop ();
		m_nLevel--;
	}


	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public int
	getLevel ()
	{
		return m_nLevel;
	}


	//----------------------------------------------------------------
	//	This is the function currently being parsed.
	//----------------------------------------------------------------
	public FuncSTO		getFunc () { return m_func; }
	public void		setReturn(FuncPtrType t) {m_func.setReturnType(t);}
	public void		setFunc (FuncSTO sto) { m_func = sto; }


//----------------------------------------------------------------
//	Instance variables.
//----------------------------------------------------------------
	private Stack  		m_stkScopes;
	private int		m_nLevel;
	private Scope		m_scopeGlobal;
	private FuncSTO	        m_func = null;
}
