abstract class CompositeType extends Type implements Cloneable
{
	public CompositeType(String name)
	{
		super(name, 4);
	}

	public boolean	isComposite ()	{ return true; }

	public String toString () 	{ return "composite";}

	public Object clone()
	{
	    return super.clone();
	}
}
